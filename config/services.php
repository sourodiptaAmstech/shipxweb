<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    // 'google' => [
    //     'client_id' => env('GOOGLE_CLIENT_ID'),
    //     'client_secret' => env('GOOGLE_CLIENT_SECRET'),
    //     'redirect' => env('GOOGLE_REDIRECT'),
    // ],

    'google' => [
        'client_id' => '403612630923-usnmb9tq3mjaudkn7qh05pesnpuoll0n.apps.googleusercontent.com',
        'client_secret' => 'Svla6ThNQdt9P4yrxKyOj-HK',
        // 'redirect' => 'http://127.0.0.1:8000/login/google/callback',
        'redirect' => 'https://demos.mydevfactory.com/debarati/shipxweb/public/login/google/callback',
    ],

    'facebook' => [
        'client_id' => '174876670389034',
        'client_secret' => '8fe919db0e60924831ac2fa41d8c7bfd',
        'redirect' => 'https://demos.mydevfactory.com/debarati/shipxweb/public/login/facebook/callback',
    ],

];
