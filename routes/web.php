<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/test',function(){
//     return view('pages.test4',['name'=>'test']);
// });
// Route::get('/ride', function () {
//     return view('pages.auth.ride', ['name' => 'ride']);
// });

// Route::get('/driver-ride', function () {
//     return view('pages.auth.driver-ride', ['name' => 'driver-ride']);
// });

// Route::get('/passenger-ride', function () {
//     return view('pages.auth.passenger-ride', ['name' => 'passenger-ride']);
// });

Route::get('/', function () {
    return view('pages.auth.home');
});

Route::get('/home', function () {
    return view('pages.auth.home');
})->name('home');



//contact us
Route::get('contact/us', 'Website\Contact\ContactController@contactUs')->name('contact.us');
Route::post('contact/to/us', 'Website\Contact\ContactController@ContactToUs')->name('contact.to.us');
Route::post('news/letter', 'Website\Contact\ContactController@subscribeNewsLetter')->name('news.letter');

Route::get('privacy/policy', 'Website\Contact\ContactController@privacyPolicy')->name('privacy.policy');
Route::get('terms/conditions', 'Website\Contact\ContactController@termsConditions')->name('terms.conditions');

Route::get('about/us', 'Website\Contact\ContactController@aboutUs')->name('about.us');

//GOOGLE LOGIN
Route::get('login/google', 'Website\Auth\LoginController@redirectToProvider')->name('login.google');
Route::get('login/google/callback', 'Website\Auth\LoginController@handleProviderCallback')->name('login.google.callback');

//FACEBOOK LOGIN
Route::get('login/facebook', 'Website\Auth\LoginController@redirectToFacebook')->name('login.facebook');

Route::get('login/facebook/callback', 'Website\Auth\LoginController@handleFacebookCallback')->name('login.facebook.callback');


Route::get('signin', 'Website\Auth\LoginController@index')->name('signin');
Route::post('loggedin', 'Website\Auth\LoginController@login')->name('loggedin');

Route::get('social-signup', 'Website\Auth\SignupController@passengerSocialSignup')->name("social-signup");

Route::post('passenger/social/signup/otp', 'Website\Auth\SignupController@sendOTPFirstSocialPassenger')->name("passenger.social.signup.otp");

Route::post('social/signin', 'Website\Auth\LoginController@socialLogin')->name("social.signin");

Route::post('passenger/social/signup', 'Website\Auth\SignupController@passengerSocialRegister')->name("passenger.social.signup");


Route::get('forgotPassword', 'Website\Auth\ForgetPasswordController@forgetPasswordIndex')->name('forgotPassword');
Route::post('forget/password', 'Website\Auth\ForgetPasswordController@forgetPassword')->name('forget.password');

Route::get('resetPassword', 'Website\Auth\ForgetPasswordController@resetPasswordIndex')->name('resetPassword');
Route::post('reset/password', 'Website\Auth\ForgetPasswordController@resetPassword')->name('reset.password');

Route::get('signup','Website\Auth\SignupController@signup')->name('signup');

Route::post('passenger/signup/otp','Website\Auth\SignupController@sendOTPFirstPassenger')->name('passenger.signup.otp');

Route::post('driver/signup/otp','Website\Auth\SignupController@sendOTPFirstDriver')->name('driver.signup.otp');

Route::post('verifyotp','Website\Auth\SignupController@verifyOTP')->name('verifyotp');

Route::post('passenger/signup', 'Website\Auth\SignupController@passengerRegister')->name("passenger.signup");

Route::post('driver/signup', 'Website\Auth\SignupController@driverRegister')->name("driver.signup");


Route::get('service/list', 'Website\Auth\SignupController@masterServiceList')->name("service.list");

//terms conditions
Route::get('passenger/terms-conditions', 'Website\Auth\SignupController@userTermsConditions')->name("passenger.terms-conditions");

Route::get('driver/terms-conditions', 'Website\Auth\SignupController@driverTermsConditions')->name("driver.terms-conditions");


// Route::get('model/list', 'Website\Auth\SignupController@modelList')->name("model.list");

// Route::get('service/leasing/list', 'Website\Passenger\LeasingController@serviceLeasingList')->name("service.leasing.list");

// Route::get('home', 'Website\Auth\LoginController@homeIndex')->name('home');

// Route::post('payment/verify/transaction', 'Website\Payment\PayStackController@verifyTransaction')->name("payment.verify.transaction");

// Route::get('email/verification/{param}', 'Website\Payment\PaymentController@verifyEmail')->name("email.verification");

// //vehicle service
// Route::get('service/make', 'Website\Vehicle\VehicleController@getMake')->name("service.make");
// Route::post('service/model', 'Website\Vehicle\VehicleController@getModel')->name("service.model");
// Route::post('service/year', 'Website\Vehicle\VehicleController@getYear')->name("service.year");



Route::group(['middleware' => ['CustomAuthMiddleware']], function () {

//     //PASSENGER ROUTE.............
    Route::get('passenger/home','Website\Passenger\ProfileController@homeIndex')->name('passenger.home');

    Route::get('passenger/account', 'Website\Passenger\ProfileController@accountIndex')->name('passenger.account');

//     //message
//     Route::post('passenger/ride/message', 'Website\Message\MessageController@sendMessage')->name("passenger.ride.message");
//     Route::post('passenger/get/message', 'Website\Message\MessageController@getMessage')->name("passenger.get.message");

    //payment
    Route::get('passenger/payment', 'Website\Payment\PaymentController@paymentIndex')->name('passenger.payment');

    Route::post('passenger/transaction/email','Website\Payment\PaymentController@UpdateTranscationEmail')->name("passenger.updateTranscationEmail");

    Route::get('passenger/card-list', 'Website\Payment\PaymentController@CheckVerifyEmail')->name("passenger.ListCard");

    Route::post('passenger/card', 'Website\Payment\PaymentController@AddTransactionCard')->name("passenger.addCard");
    Route::put('passenger/default/card', 'Website\Payment\PaymentController@setDefaultTransactionCard')->name("passenger.default.card");
    Route::delete('passenger/delete/card', 'Website\Payment\PaymentController@deleteTransactionCard')->name("passenger.delete.card");
        //..............
    Route::get('passenger/addpayment', 'Website\Payment\PaymentController@addPayment')->name("passenger.addpayment");

//     //leasing
//     Route::get('passenger/lease','Website\Passenger\LeasingController@passengerLeasing')->name('passenger.lease');

//     Route::post('passenger/leasing/hourly', 'Website\Passenger\LeasingController@hourlyLeaseRequest')->name("passenger.leasing.hourly");

//     Route::post('passenger/leasing/daily', 'Website\Passenger\LeasingController@dailyLeaseRequest')->name("passenger.leasing.daily");

//     Route::post('passenger/leasing/longtime', 'Website\Passenger\LeasingController@longLeaseRequest')->name("passenger.leasing.longtime");

    Route::get('passenger/logout', 'Website\Auth\LoginController@passengerLogout')->name("passenger.logout");

//     // Route::get('passenger/emergencyContact', 'Website\Auth\SignupController@emergencyContactIndex')->name("passenger.emergencyContact");

//     // Route::post('passenger/passEmergencyContact', 'Website\Auth\SignupController@passengerEmergencyContact')->name("passenger.passEmergencyContact");

    //change password
    Route::get('passenger/change/password/index', 'Website\Passenger\ChangePasswordController@changePasswordIndex')->name("passenger.change.password.index");

    Route::post('passenger/change/password', 'Website\Passenger\ChangePasswordController@changePassword')->name("passenger.change.password");
    //profile image
    Route::post('passenger/profile/image', 'Website\Passenger\ProfileController@profileImageUpdate')->name("passenger.profile.image");

    Route::get('passenger/profile/list', 'Website\Passenger\ProfileController@getProfile')->name("passenger.profile.list");

    Route::post('passenger/profile/update', 'Website\Passenger\ProfileController@updatePassengerProfile')->name("passenger.profile.update");
    Route::post('passenger/background/data', 'Website\Passenger\BackgroundController@passengerBackgroundData')->name("passenger.background.data");

    Route::post('passenger/get/estimated/fare','Website\Passenger\RequestController@estimated')->name("passenger.estimatedFare");

    Route::post('passenger/request/ride','Website\Passenger\RequestController@confirmRequest')->name("passenger.request");

     Route::post('passenger/request/ratingComment','Website\Passenger\RequestController@ratingComment')->name("passenger.ratingComment");

//     Route::post('passenger/request/schedule/ride','Website\Passenger\RequestController@scheduleRide')->name("passenger.scheduleRide");

//       // trip history
    Route::get('passenger/trip-history', 'Website\Passenger\RequestController@tripHistoryIndex')->name("passenger.trip-history");

    Route::get('passenger/trip/history', 'Website\Passenger\RequestController@passengerTripHistory')->name("passenger.trip.history");

    Route::get('passenger/trip-details', 'Website\Passenger\RequestController@tripHistoryDetails')->name("passenger.trip-details");

    Route::post('passenger/trip/details', 'Website\Passenger\RequestController@passengerTripDetails')->name("passenger.trip.details");

    Route::get('passenger/upcoming-trip', 'Website\Passenger\RequestController@scheduleTripIndex')->name("passenger.upcoming-trip");

    Route::get('passenger/upcoming/schedule', 'Website\Passenger\RequestController@upcommingScheduleTrip')->name("passenger.upcoming.scheduleTrip");

    Route::get('passenger/upcoming-trip/details', 'Website\Passenger\RequestController@scheduleTripDetailsIndex')->name("passenger.upcoming-trip.details");

    Route::post('passenger/get/upcoming-trip/details', 'Website\Passenger\RequestController@passengerScheduleTripDetails')->name("passenger.get.upcoming-trip.details");


    Route::post('passenger/cancel/schedule', 'Website\Passenger\RequestController@cancelScheduleTrip')->name("passenger.cancel.schedule");
    Route::post('passenger/cancel/ride', 'Website\Passenger\RequestController@cancelRideTrip')->name("passenger.cancel.ride");
    Route::post('passenger/pay/ride', 'Website\Passenger\RequestController@pay')->name("passenger.pay.ride");
//       //admin chat support
//     Route::get('passenger/chat-support', 'Website\Message\MessageController@sendPassengerMessageAdminIndex')->name("passenger.chat-support");
//     Route::post('passenger/chat/support', 'Website\Message\MessageController@sendPassengerMessageAdmin')->name("passenger.chat.support");
//     Route::post('passenger/chat/support/get', 'Website\Message\MessageController@getPassengerMessageAdmin')->name("passenger.chat.support.get");
    //promocode
    Route::post('passenger/add/promocode', 'Website\Promocode\PromoCodeController@addPromoCode')->name("passenger.add.promocode");
    Route::get('passenger/get/promocode', 'Website\Promocode\PromoCodeController@getPromoCode')->name("passenger.get.promocode");
    Route::get('passenger/promocode', 'Website\Promocode\PromoCodeController@promoCodeIndex')->name("passenger.promocode");
    //booking
    Route::get('passenger/request/booking', 'Website\Passenger\RequestController@requestBooking')->name("passenger.request.booking");
    //help
    Route::get('passenger/help', 'Website\Passenger\ProfileController@helpIndex')->name("passenger.help");
    Route::get('passenger/get/help', 'Website\Passenger\ProfileController@passengerHelp')->name("passenger.get.help");

    //report issue
    Route::get('passenger/report-issue', 'Website\Passenger\ProfileController@reportIssueIndex')->name("passenger.report-issue");
    Route::post('passenger/report', 'Website\Passenger\ProfileController@reportIssue')->name("passenger.report");

    Route::get('passenger/contact/us', 'Website\Contact\ContactController@passengerContactUs')->name('passenger.contact.us');
    Route::get('passenger/about/us', 'Website\Contact\ContactController@passengerAboutUs')->name('passenger.about.us');

    Route::get('passenger/privacy/policy', 'Website\Contact\ContactController@passengerPrivacyPolicy')->name('passenger.privacy.policy');
    Route::get('passenger/terms/conditions', 'Website\Contact\ContactController@passengerTermsConditions')->name('passenger.terms.conditions');



//     //END PASSENGER ROUTE.............


//     //DRIVER ROUTE....................

Route::get('driver/privacy/policy', 'Website\Contact\ContactController@driverPrivacyPolicy')->name('driver.privacy.policy');
Route::get('driver/terms/conditions', 'Website\Contact\ContactController@driverTermsConditions')->name('driver.terms.conditions');

//help
Route::get('driver/help', 'Website\Driver\ProfileController@helpIndex')->name("driver.help");
Route::get('driver/get/help', 'Website\Driver\ProfileController@driverHelp')->name("driver.get.help");

//       //admin chat support
//     Route::get('driver/chat-support', 'Website\Message\MessageController@sendDriverMessageAdminIndex')->name("driver.chat-support");
//     Route::post('driver/chat/support', 'Website\Message\MessageController@sendDriverMessageAdmin')->name("driver.chat.support");
//     Route::post('driver/chat/support/get', 'Website\Message\MessageController@getDriverMessageAdmin')->name("driver.chat.support.get");

    // trip history
    Route::get('driver/trip-history', 'Website\Driver\TripController@tripHistoryIndex')->name("driver.trip-history");
    Route::get('driver/trip/history', 'Website\Driver\TripController@driverTripHistory')->name("driver.trip.history");
    Route::get('driver/trip-details', 'Website\Driver\TripController@tripHistoryDetails')->name("driver.trip-details");
    Route::post('driver/trip/details', 'Website\Driver\TripController@driverTripDetails')->name("driver.trip.details");

     Route::get('driver/upcoming-trip', 'Website\Driver\TripController@scheduleTripIndex')->name("driver.upcoming-trip");

    Route::get('driver/upcoming/schedule', 'Website\Driver\TripController@scheduleDriverTrips')->name("driver.upcoming.scheduleTrip");

    Route::get('driver/upcoming-trip/details', 'Website\Driver\TripController@scheduleTripDetailsIndex')->name("driver.upcoming-trip.details");

    Route::post('driver/get/upcoming-trip/details', 'Website\Driver\TripController@scheduleDriverTripDetails')->name("driver.get.upcoming-trip.details");

//      //message
//     Route::post('driver/ride/message', 'Website\Message\MessageController@sendMessageDriver')->name("driver.ride.message");
//     Route::post('driver/get/message', 'Website\Message\MessageController@getMessageDriver')->name("driver.get.message");


    Route::get('driver/home','Website\Driver\ProfileController@homeIndex')->name('driver.home');

//     // Route::get('driver/email', 'Website\Auth\SignupController@driverEmailIndex')->name("driver.email");

//     // Route::post('driver/email/update', 'Website\Auth\SignupController@updateEmail')->name("driver.email.update");

    Route::get('driver/account', 'Website\Driver\ProfileController@accountIndex')->name('driver.account');

//     Route::get('driver/map', 'Website\Driver\ProfileController@mapLocation')->name('driver.map');

    Route::get('driver/logout', 'Website\Auth\LoginController@driverLogout')->name("driver.logout");
    //change password
    Route::get('driver/change/password/index', 'Website\Driver\ChangePasswordController@changePasswordIndex')->name("driver.change.password.index");

    Route::post('driver/change/password', 'Website\Driver\ChangePasswordController@changePassword')->name("driver.change.password");
    //profile image
    Route::post('driver/profile/image', 'Website\Driver\ProfileController@profileImageUpdate')->name("driver.profile.image");

    Route::get('driver/profile/list', 'Website\Driver\ProfileController@getProfile')->name("driver.profile.list");

    Route::post('driver/profile/update', 'Website\Driver\ProfileController@updateDriverProfile')->name("driver.profile.update");

        //document
    Route::get('driver/document', 'Website\Driver\DocumentController@document')->name("driver.document");

    Route::get('driver/document/list', 'Website\Driver\DocumentController@documentList')->name("driver.document.list");

    Route::post('driver/document/upload', 'Website\Driver\DocumentController@uploadDocument')->name("driver.document.upload");
    Route::post('driver/social/update', 'Website\Driver\ProfileController@updateDriverSocialProfile')->name("driver.social.update");
     Route::post('driver/background/data', 'Website\Driver\BackgroundController@driversBackgroundData')->name("driver.background.data");
     Route::post('driver/request/reject', 'Website\Driver\TripController@declineRequest')->name("driver.request.reject");
     Route::post('driver/cancel/ride', 'Website\Driver\TripController@cancelRideTrip')->name("driver.cancel.ride");

     Route::post('driver/trip/control', 'Website\Driver\TripController@tripControl')->name("driver.trip.control");

//     Route::post('driver/payment', 'Website\Driver\TripController@Payment')->name("driver.payment");

    Route::post('driver/request/ratingComment', 'Website\Driver\TripController@ratingComment')->name("driver.ratingComment");

//     Route::put('driver/on/off/update', 'Website\Driver\ProfileController@driverOnOffStatus')->name("driver.onOff");
    Route::get('driver/trip/request', 'Website\Driver\TripController@bookingRequest')->name("driver.trip-request");

    Route::get('driver/wallet', 'Website\Driver\TripController@wallet')->name("driver.wallet");
    Route::get('driver/payment/earning', 'Website\Driver\TripController@paymentWallet')->name("driver.payment.earning");

    Route::get('driver/contact/us', 'Website\Contact\ContactController@driverContactUs')->name('driver.contact.us');
    Route::get('driver/about/us', 'Website\Contact\ContactController@driverAboutUs')->name('driver.about.us');
//     //END DRIVER ROUTE................

});
