var oldLocation={lat:"",log:""};
var mapElement=document.querySelector("#map");
var directionsService = new google.maps.DirectionsService();
var directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true});
var googleMap="";
var clearMap=0;
var overlay=0;
var isPaid=0;//ss

var currentLocation={};
var bookingLocation={pickupLocations:{},destination:[]};
var options = {
    enableHighAccuracy: true,
    timeout: 4000,
    maximumAge: 0
};
var getCurrentLatLong=()=>{
    navigator.geolocation.getCurrentPosition(success, error);
};

var success=(pos)=> {
    currentLocation = pos.coords;
    if(googleMap=="" ){
        if($("#map").length>0){
          //  getCurrentLocation("no");
            googleMap=new google.maps.Map(mapElement);
            drawMap(new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude));
        }
    }

    driverBackgroundAPI(currentLocation);
};
function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
};

var driverBackgroundAPI=(currentLocation)=>{
    if(overlay==0){
        $(".overlay").show();
        overlay=1;
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/driver/background/data",
        method:"POST",
        data:{ device_latitude:currentLocation.latitude,
        device_longitude:currentLocation.longitude, timeZone: "timeZone" },
        success: function(response){
            console.log(response.data.requests[0]);
            if(response.data.requests.length>0){
                $("#map").show();
                if(response.data.requests[0].request.status=="STARTED"){
                    if($("#requestStarted").length==0){
                         $("#requestContainer").html(requestStarted(response.data.requests[0]));
                         $("#requestContainer").show();
                        }
                        if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=response.data.requests[0].request.s_latitude || oldLocation.Dlog!=response.data.requests[0].request.s_longitude){
                            googleDrawRoute({
                            origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                            destination: new google.maps.LatLng(response.data.requests[0].request.s_latitude, response.data.requests[0].request.s_longitude)
                        });
                        oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                        oldLocation.Dlat=response.data.requests[0].request.d_latitude; oldLocation.Dlog=response.data.requests[0].request.d_longitude;
                        }

                }
                else if(response.data.requests[0].request.status=="ARRIVED"){
                    isPaid=response.data.requests[0].request.paid;
                    if($("#requestARRIVED").length==0){
                         $("#requestContainer").html(requestARRIVED(response.data.requests[0]));
                         $("#requestContainer").show();

                    }
                    if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=response.data.requests[0].request.s_latitude || oldLocation.Dlog!=response.data.requests[0].request.s_longitude){
                        googleDrawRoute({
                        origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                        destination: new google.maps.LatLng(response.data.requests[0].request.s_latitude, response.data.requests[0].request.s_longitude)
                    });
                    oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                    oldLocation.Dlat=response.data.requests[0].request.d_latitude; oldLocation.Dlog=response.data.requests[0].request.d_longitude;
                    }

                }
                else if(response.data.requests[0].request.status=="PICKEDUP"){//alert(87);
                    if($("#onPickedUP").length==0){
                        $("#requestContainer").html(requestPICKEDUP(response.data.requests[0]));
                        $("#requestContainer").show();
                    }
                    if(response.data.requests[0].request.multiple_waypoints=="MULTIPLE_WAYPOINTS"){//alert(92);
                        if( Object.keys(response.data.requests[0].request.MultipleWayPoints).length>0){//alert(93);
                            if(response.data.requests[0].request.MultipleWayPoints.status==1){//alert(94);
                                $("#tapWhenDel").text("START NEXT");
                                $("#tapWhenDel").attr("id","START_NEXT");

                            }
                            else{
                                if($("#START_NEXT").length>0){//alert(100)
                                    $("#onPickedUP").attr("id","onPickedUPss");
                                    $(".overlay").show();
                                    overlay=1;
                                }
                            }
                            if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=response.data.requests[0].request.MultipleWayPoints.w_latitude || oldLocation.Dlog!=response.data.requests[0].request.MultipleWayPoints.w_longitude){
                                googleDrawRoute({
                                origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                                destination: new google.maps.LatLng(response.data.requests[0].request.MultipleWayPoints.w_latitude, response.data.requests[0].request.MultipleWayPoints.w_longitude)
                            });
                            oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                            oldLocation.Dlat=response.data.requests[0].request.MultipleWayPoints.w_latitude; oldLocation.Dlog=response.data.requests[0].request.MultipleWayPoints.w_longitude;
                            }
                        }
                        else{
                            if($("#START_NEXT").length>0){//alert(100)
                                $("#onPickedUP").attr("id","onPickedUPss");
                                $(".overlay").show();
                                overlay=1;
                            }

                    if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=response.data.requests[0].request.s_latitude || oldLocation.Dlog!=response.data.requests[0].request.s_longitude){
                        googleDrawRoute({
                        origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                        destination: new google.maps.LatLng(response.data.requests[0].request.d_latitude, response.data.requests[0].request.d_longitude)
                    });
                    oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                    oldLocation.Dlat=response.data.requests[0].request.d_latitude; oldLocation.Dlog=response.data.requests[0].request.d_longitude;
                    }
                        }
                    }
                    else{

                    if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=response.data.requests[0].request.s_latitude || oldLocation.Dlog!=response.data.requests[0].request.s_longitude){
                        googleDrawRoute({
                        origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                        destination: new google.maps.LatLng(response.data.requests[0].request.d_latitude, response.data.requests[0].request.d_longitude)
                    });
                    oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                    oldLocation.Dlat=response.data.requests[0].request.d_latitude; oldLocation.Dlog=response.data.requests[0].request.d_longitude;
                    }
                    }
                }
                else if(response.data.requests[0].request.status=="SEARCHING"){
                    $("#requestContainer").html(requestSEARCHING(response.data.requests[0]));
                    $("#requestContainer").show();
                    if(oldLocation.lat!=currentLocation.latitude || oldLocation.log!=currentLocation.longitude || oldLocation.Dlat!=response.data.requests[0].request.s_latitude || oldLocation.Dlog!=response.data.requests[0].request.s_longitude){
                        googleDrawRoute({
                            origin:new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude),
                            destination: new google.maps.LatLng(response.data.requests[0].request.d_latitude, response.data.requests[0].request.d_longitude)
                        });
                        oldLocation.lat=currentLocation.latitude; oldLocation.log=currentLocation.longitude;
                        oldLocation.Dlat=response.data.requests[0].request.d_latitude; oldLocation.Dlog=response.data.requests[0].request.d_longitude;
                    }
                }
            }
            else{
                //if in booking page
                if(isRecive!=undefined){
                    if(isRecive==true){
                        // I am in request booking section
                        if($("#requestContainer").css("display")=="block"|| $("#signatureBlock").css("display")=="block" ){
                            $("#requestContainer").hide();
                            $("#requestContainer").html('');
                            $("#signatureBlock").hide();//alert();
                            $("#map").html('');
                            googleMap=new google.maps.Map(mapElement);
                            drawMap(new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude));
                        }

                    }
                }

            }
            getCurrentLatLong();
            if(overlay==1){
                $(".overlay").hide();
            }

        },
        error: function(response){
            getCurrentLatLong();
        }
    });



};



var requestStarted=(data)=>{
    return ' <div class="row mt-5" id="requestStarted">'+
    '<div class="col-12">'+
    '<div class="card">'+
    '<div class="card-header" style="width: 100%" >'+
    '<div class="row">'+
    '<div class="col-12">'+
    '<div class="blockquote-footer text-center">Pick up Location</div>'+
    '<p class="mb-0 text-center">'+data.request.s_address+'</p>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="col-12">'+
    '<div class="card" style="padding: 0;margin: 0;border: ;border: 0;">'+
    '<div class="card-body">'+
    '<img style="width:120px;padding: 10px;" class="img-fluid" src="https://app.shipxnow.com/public/storage/'+data.request.user.picture+'" alt="">'+
    '<label class="mr-2">'+data.request.user.first_name+' '+data.request.user.last_name+'</label>'+
    '<i class="fa fa-phone-square" aria-hidden="true" style="font-size: 34px;padding: 0;margin: 0;text-align: center;"></i>'+
    '</div>'+
    '<div class="card-footer" style="width: 100%;padding: 0;margin: 0;background-color: #fff;padding-top: 35px;"> <div class="row"> <div class="col-lg-6 d-flex justify-content-lg-end justify-content-center">'+
    '<button id="tabActionOnCancel" reqId="'+data.request_id+'" class="btn btn-muted-theme btn-lrg text-white ml-md-4 mb-3" style="height: 40px;font-size: unset;padding: 0;">Cancel Ship</button> </div>'+
    '<div class="col-lg-6 d-flex justify-content-lg-start justify-content-center"><button id="tabActionOnArrived" reqId="'+data.request_id+'" class="btn btn-success-theme btn-lrg text-white ml-md-4 mb-5" style="height: 40px;font-size: unset;padding: 0;">TAP WHEN ARRIVED</button> </div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>';
}
var requestARRIVED=(data)=>{
    dAdds="";item_description="";recipient_name="";parcelDetails_isSignature="";
    if(data.request.multiple_waypoints=="MULTIPLE_WAYPOINTS"){
        if(   Object.keys(data.request.MultipleWayPoints).length>0){
            dAdds=data.request.MultipleWayPoints.w_address;
            recipient_name=data.request.MultipleWayPoints.parcelDetails_recipient_name;
            item_description =data.request.MultipleWayPoints.parcelDetails_item_description;
            parcelDetails_isSignature=data.request.MultipleWayPoints.parcelDetails_isSignature;
        }
        else{
            dAdds=data.request.d_address;
            recipient_name=data.request.DestinationPoints.parcelDetails_recipient_name;
            item_description =data.request.DestinationPoints.parcelDetails_item_description;
            parcelDetails_isSignature=data.request.DestinationPoints.parcelDetails_isSignature;
        }
    }
    else{
        dAdds=data.request.d_address;
        recipient_name=data.request.DestinationPoints.parcelDetails_recipient_name;
        item_description =data.request.DestinationPoints.parcelDetails_item_description;
        parcelDetails_isSignature=data.request.DestinationPoints.parcelDetails_isSignature;
    }




    return ' <div class="row mt-5" id="requestARRIVED">'+
    '<div class="col-12">'+
    '<div class="card">'+
    '<div class="card-header" style="width: 100%" >'+
    '<div class="row">'+
    '<div class="col-12">'+
    '<div class="blockquote-footer text-center">Drop Location</div>'+
    '<p class="mb-0 text-center">'+dAdds+'</p>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="col-12">'+
    '<div class="card" style="padding: 0;margin: 0;border: ;border: 0;">'+
    '<div class="card-body">'+
    '<img style="width:120px;padding: 10px;" class="img-fluid" src="https://app.shipxnow.com/public/storage/'+data.request.user.picture+'" alt="">'+
    '<label class="mr-2">'+data.request.user.first_name+' '+data.request.user.last_name+'</label>'+
    '<i class="fa fa-phone-square" aria-hidden="true" style="font-size: 34px;padding: 0;margin: 0;text-align: center;"></i>'+
    '</div>'+
    '<div class="card-footer" style="width: 100%;padding: 0;margin: 0;background-color: #fff;padding-top: 35px;"> <div class="row"> <div class="col-lg-6 d-flex justify-content-lg-end justify-content-center">'+
    '<button class="btn btn-muted-theme btn-lrg text-white ml-md-4 mb-3" style="height: 40px;font-size: unset;padding: 0;">Cancel Ship</button> </div>'+
    '<div class="col-lg-6 d-flex justify-content-lg-start justify-content-center"><button id="tabActionOnPICKEDUP" reqId="'+data.request_id+'" class="btn btn-success-theme btn-lrg text-white ml-md-4 mb-5" style="height: 40px;font-size: unset;padding: 0;">TAP WHEN PICKEDUP</button>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>';
}

var requestPICKEDUP=(data)=>{
    dAdds="";item_description="";recipient_name="";parcelDetails_isSignature="";
if(data.request.multiple_waypoints=="MULTIPLE_WAYPOINTS"){ //alert(240);
    if(   Object.keys(data.request.MultipleWayPoints).length>0){//alert(241);

        dAdds=data.request.MultipleWayPoints.w_address;
        recipient_name=data.request.MultipleWayPoints.parcelDetails_recipient_name;
        item_description =data.request.MultipleWayPoints.parcelDetails_item_description;
        parcelDetails_isSignature=data.request.MultipleWayPoints.parcelDetails_isSignature;
    }
    else{//alert(247);
        dAdds=data.request.d_address;
        recipient_name=data.request.DestinationPoints.parcelDetails_recipient_name;
        item_description =data.request.DestinationPoints.parcelDetails_item_description;
        parcelDetails_isSignature=data.request.DestinationPoints.parcelDetails_isSignature;
    }
}
else{//alert(254);
    dAdds=data.request.d_address;
    recipient_name=data.request.DestinationPoints.parcelDetails_recipient_name;
    item_description =data.request.DestinationPoints.parcelDetails_item_description;
    parcelDetails_isSignature=data.request.DestinationPoints.parcelDetails_isSignature;
}





    return ' <div class="row mt-5" id="onPickedUP">'+
    '<div class="col-12 mx-0">'+
    '<div class="card">'+
    '<div class="card-header" style="width: 100%" >'+
    '<div class="row">'+
    '<div class="col-12">'+
    '<div class="blockquote-footer text-center">Drop Location</div>'+
    '<p class="mb-0 text-center" id="dAdds">'+dAdds+'</p>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="col-12">'+
    '<div class="card" style="padding: 0;margin: 0;border: ;border: 0;">'+
    '<div class="card-body">'+
    '<i class="fa fa-user-circle-o" aria-hidden="true" style="font-size: 54px;"></i>'+
    '<label style="margin:10px;"><blockquote class="blockquote">'+
    '<p class="mb-0">'+recipient_name+'</p>'+
    '<footer class="blockquote-footer">'+item_description+'</footer>'+'</blockquote></label>'+
    '<i class="fa fa-phone-square" aria-hidden="true" style="font-size: 54px;padding: 0;margin: 0;text-align: center;"></i>'+
    '</div>'+
    '<div class="card-footer" style="width: 100%;padding: 0;margin: 0;background-color: #fff;padding-top: 35px;"> <div class="row"> '+
    '<div class="col-lg-6 col-md-12 d-flex justify-content-center"><button id="tapWhenDel" reqId="'+data.request_id+'" d="'+data.request.distance+'" s="'+parcelDetails_isSignature+'"   class="btn btn-success-theme btn-lrg text-white mb-5" style="height: 40px;font-size: unset;padding: 0;">TAP WHEN DELIVERED</button>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+'<div class="row" id="onSignaturePad"></div>';
}

var rideCOMPLETED=(data)=>{
    return '<div class="row">'+
                '<div class="card col-6" style="padding:0">'+
                    '<h5 class="card-header col-12">Rate your experience with '+data.provider.first_name+' '+data.provider.last_name+'</h5>'+
                    '<div class="row">'+
                        '<div class="col-12">'+
                            '<div class="card col-12" style="padding:0">'+
                                '<img style="width:100px" src="https://app.shipxnow.com/public/assets/img/profile-user.png" >'+
                                 '<div class="card-body">'+
                                    '<p class="card-text"><input type="hidden" id="reviewRating" value="1">'+
                                        '<i class="fa fa-star" aria-hidden="true"></i>'+
                                        '<i class="fa fa-star-half-o" aria-hidden="true"></i>'+
                                        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
                                        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
                                        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
                                    '</p>'+
                                '</div>'+
                                '<div class="card-body">'+
                                '<p class="card-text">'+
                                    '<div class="form-group">'+
                                        '<input style="border: none;border-bottom: 1px #ccc solid;border-radius: 0;" type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">'+
                                    '</div>'+
                                '</p>'+
                            '</div>'+
                                '<div class="card-body">'+
                                    '<p class="card-text">'+
                                        '<button type="button" style="height: 40px;width: 160px;font-size: unset;" class="btn btn-success-theme btn-lrg text-white" id="callDriver">Call</button>'+
                                    '</p>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+

                    '</div>'+

                '</div>'+
            '</div>';
}
var requestSEARCHING=(data)=>{
    return ' <div class="row mt-5">'+
    '<div class="col-12">'+
    '<div class="card">'+
    '<div class="card-header" style="width: 100%" >'+
    '<div class="row">'+
    '<div class="col-12">'+
    '<div class="blockquote-footer text-center">Pick up Location</div>'+
    '<p class="mb-0 text-center">'+data.request.s_address+'</p>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="col-12">'+
    '<div class="card" style="display: flex;padding: 0;margin: 0;border: ;border: 0;">'+
    '<div class="card-body">'+
    '<img style="width:120px;padding: 10px;" class="img-fluid" src="https://app.shipxnow.com/public/storage/'+data.request.user.picture+'" alt="">'+
    '<label style="margin:10px;"><blockquote class="blockquote">'+
    '<p class="mb-0">'+data.request.user.first_name+' '+data.request.user.last_name+'</p>'+
    '<footer class="blockquote-footer">'+data.request.DestinationPoints.parcelDetails_item_description+'</footer>'+'</blockquote></label>'+
    '<i class="fa fa-phone-square" aria-hidden="true" style="font-size: 54px;padding: 0;margin: 0;text-align: center;"></i>'+
    '</div>'+
    '<div class="card-footer" style="width: 100%;padding: 0;margin: 0;background-color: #fff;padding-top: 35px;"> <div class="col-12 row"> <div class="col-6 d-flex justify-content-end">'+
    '<button id="rejectRideRequest" requestAttr="'+data.request_id+'" class="btn btn-muted-theme btn-lrg text-white mb-5" style="width: 125px;height: 40px;font-size: unset;padding: 0;">Reject</button> </div>'+
    '<div class="col-6"><button id="accpectRideRequest" requestAttr="'+data.request_id+'" class="btn btn-success-theme btn-lrg text-white mb-md-5" style="width: 125px;height: 40px;font-size: unset;padding: 0;">Accept</button><div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>';
}


$(document).on("change",".noPercel", function(e){
 var id=(e.target.id).split("_")[1];
 bookingLocation.destination["destination_"+id].noPercel=e.target.value;
 //{pickupPointAddres:{},dropPointAddress:{},noPercel:0,description:"",recipients_name:"",signature:""};

});
$(document).on("change",".description", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].description=e.target.value;
});
$(document).on("change",".recipientsName", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].recipients_name=e.target.value;
});
$(document).on("change",".signature", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].signature=e.target.value;
});
$(document).on("change",".deliveryMethods", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].deliveryMethods=e.target.value;
});
$(document).on("change",".deliveryItemCategory", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].deliveryItemCategory=e.target.value;
});
$(document).on("change",".deliveryOptions", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].deliveryOptions=e.target.value;
});


$(document).on("click","#rejectRideRequest",function(){

    // check for the client side validation
    overlay=2;
    $(".overlay").show();

    var data={request_id:$("#rejectRideRequest").attr("requestAttr")};



    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/driver/request/reject",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){
            overlay=1;
        },
        error: function(response){
            overlay=1;

        }
    });






});


$(document).on("click","#accpectRideRequest",function(){

    // check for the client side validation

    overlay=2;
    $(".overlay").show();

    var data={request_id:$("#accpectRideRequest").attr("requestAttr"),tripStatus:"accpect"};



    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/driver/trip/control",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){
            overlay=1;
        },
        error: function(response){
            overlay=1;
        }
    });






});

$(document).on("click","#tabActionOnCancel",function(){
    var modelHtml='<div class="card">'+
    '<div class="card-body">'+
      '<h4 class="card-title">Cancel Request</h4>'+
      '<h6 class="card-subtitle mb-2 text-muted"></h6>'+
      '<p class="card-text">'+
      'Are you sure want to cancel the ship?'+
      '</p>'+
      '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class="card-link btn btn-success-theme btn-lrg text-white" id="cancelNoPass" >NO</a>'+
      '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class=" card-link btn btn-success-theme btn-lrg text-white" id="cancelYesPass" >YES</a>'+
    '</div>'+
  '</div>';
 $("#modalContent").html(modelHtml);
 var modal = document.getElementById("myModal");
 modal.style.display = "block";


 });
 $(document).on("click","#cancelNoPass",function(){
     var modal = document.getElementById("myModal");
     modal.style.display = "none";


  });
  $(document).on("click","#cancelYesPass",function(){
     var modelHtml='<div class="card">'+
     '<div class="card-body">'+
       '<h4 class="card-title">Reason to cancel </h4>'+
       '<h6 class="card-subtitle mb-2 text-muted"></h6>'+
       '<p class="card-text">'+
       '<div class="form-group">'+
       '<input style="border: none;border-bottom: 1px #ccc solid;border-radius: 0;" type="text" class="form-control" id="cancleRidePassCommet" placeholder="Write your comment">'+
       '</div>'+
       '</p>'+

       '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class=" card-link btn btn-success-theme btn-lrg text-white" id="cancelYesPassSubmitd" >SUBMIT</a>'+
     '</div>'+
   '</div>';
  $("#modalContent").html(modelHtml);
  var modal = document.getElementById("myModal");
  modal.style.display = "block";


  });
  $(document).on("click","#cancelYesPassSubmitd",function(){
    overlay=2;
    $(".overlay").show();

      $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });

     $.ajax({
         url: APP_URL+"/driver/cancel/ride",
         method:"POST",
         data:{
         comment:$("#cancleRidePassCommet").val(),request_id:$("#tabActionOnCancel").attr("reqId") },
         success: function(response){
            var modal = document.getElementById("myModal");
             modal.style.display = "none";
             overlay=1;
            },
         error: function(response){
          //   getCurrentLatLong();
         }
     });
 });


 $(document).on("click","#tabActionOnArrived",function(){ // check for the client side validation
    overlay=2;
    $(".overlay").show();


    var data={request_id:$("#tabActionOnArrived").attr("reqId"),tripStatus:"arrived"};



    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/driver/trip/control",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){
            overlay=1;
            //$(".overlay").show();


        },
        error: function(response){
            overlay=1;
        }
    });
});


$(document).on("click","#tabActionOnPICKEDUP",function(){ // check for the client side validation


    if(isPaid==0){
        var modelHtml='<div class="card">'+
        '<div class="card-body">'+
          '<h4 class="card-title">Not Paid.. </h4>'+
          '<h6 class="card-subtitle mb-2 text-muted"></h6>'+
          '<p class="card-text">'+
          'Please wait for customer payment complete.'+
          '</p>'+
          '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class="card-link btn btn-success-theme btn-lrg text-white" id="cancelNoPass" >Cancel</a>'+
        '</div>'+
      '</div>';
      $("#modalContent").html(modelHtml);
      var modal = document.getElementById("myModal");
      modal.style.display = "block";
      return false;
    }
    overlay=2;
    $(".overlay").show();
    var data={request_id:$("#tabActionOnPICKEDUP").attr("reqId"),tripStatus:"PICKEDUP"};



    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/driver/trip/control",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){

            overlay=1;
        },
        error: function(response){
            overlay=1;
        }
    });
});

$(document).on("click","#tapWhenDel",function(){ // check for the client side validation


    var data={request_id:$("#tapWhenDel").attr("reqId"),tripStatus:"DROP",latitude:currentLocation.latitude,longitude:currentLocation.longitude,distance:$("#tapWhenDel").attr("d"),address:$("#dAdds").text()};
    if($("#tapWhenDel").attr("s")==1){
        $("#signatureBlock").show();
        $("#requestContainer").hide();
        return false;
    }

    overlay=2;
    $(".overlay").show();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/driver/trip/control",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){

            $.ajax({
                url: APP_URL+"/driver/request/ratingComment",
                method:"POST",
                data:data,//JSON.stringify(bookingLocation),
                success: function(response){
                    overlay=1;
                },
                error: function(response){
                    overlay=1;
                }
            });
        },
        error: function(response){
            $.ajax({
                url: APP_URL+"/driver/request/ratingComment",
                method:"POST",
                data:data,//JSON.stringify(bookingLocation),
                success: function(response){
                    overlay=1;
                },
                error: function(response){
                    overlay=1;
                }
            });
        }
    });
});
$(document).on("click","#cancelSignature",function(){
    $("#clearSignature").click();
    $("#signatureBlock").hide();
    $("#requestContainer").show();

});

$(document).on("click","#saveSignature",function(){
  //  $("#clearSignature").click();
   // $("#signatureBlock").hide();
   // $("#requestContainer").show();
    var data={request_id:$("#tapWhenDel").attr("reqId"),tripStatus:"DROP",latitude:currentLocation.latitude,longitude:currentLocation.longitude,distance:$("#tapWhenDel").attr("d"),address:$("#dAdds").text()};
    overlay=2;
    $(".overlay").show();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/driver/trip/control",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: APP_URL+"/driver/request/ratingComment",
                method:"POST",
                data:data,//JSON.stringify(bookingLocation),
                success: function(response){
                    overlay=1;
                    location.reload();
                },
                error: function(response){
                    overlay=1;
                    location.reload();
                }
            });
        },
        error: function(response){
            $.ajax({
                url: APP_URL+"/driver/request/ratingComment",
                method:"POST",
                data:data,//JSON.stringify(bookingLocation),
                success: function(response){
                    overlay=1;
                    location.reload();
                },
                error: function(response){
                    overlay=1;
                    location.reload();
                }
            });
        }
    });
});


var googleDrawRoute=(locationData)=>{
    var request = {
        origin: locationData.origin,
        destination: locationData.destination,
        travelMode: google.maps.TravelMode.DRIVING
    };

    var icon = {
        url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/storage/assets/img/location-1@3x.png",
        scaledSize: new google.maps.Size(50, 50),
    };

    new google.maps.Marker({
        position: request.destination,
        map: googleMap,
        icon: icon
    });

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsRenderer.setDirections(response);
            directionsRenderer.setMap(googleMap);
        } else {
            alert(status+"Off here");
        }
    });
}
var iconCar = {
    url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/storage/assets/img/Car@3x.png",
    scaledSize: new google.maps.Size(50, 50),
};
var styleMap=[
    { elementType: "geometry", stylers: [{ color: "#ebe3cd" }] },
    { elementType: "labels.text.fill", stylers: [{ color: "#523735" }] },
    { elementType: "labels.text.stroke", stylers: [{ color: "#f5f1e6" }] },
    {
      featureType: "administrative",
      elementType: "geometry.stroke",
      stylers: [{ color: "#c9b2a6" }],
    },
    {
      featureType: "administrative.land_parcel",
      elementType: "geometry.stroke",
      stylers: [{ color: "#dcd2be" }],
    },
    {
      featureType: "administrative.land_parcel",
      elementType: "labels.text.fill",
      stylers: [{ color: "#ae9e90" }],
    },
    {
      featureType: "landscape.natural",
      elementType: "geometry",
      stylers: [{ color: "#dfd2ae" }],
    },
    {
      featureType: "poi",
      elementType: "geometry",
      stylers: [{ color: "#dfd2ae" }],
    },
    {
      featureType: "poi",
      elementType: "labels.text.fill",
      stylers: [{ color: "#93817c" }],
    },
    {
      featureType: "poi.park",
      elementType: "geometry.fill",
      stylers: [{ color: "#a5b076" }],
    },
    {
      featureType: "poi.park",
      elementType: "labels.text.fill",
      stylers: [{ color: "#447530" }],
    },
    {
      featureType: "road",
      elementType: "geometry",
      stylers: [{ color: "#f5f1e6" }],
    },
    {
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [{ color: "#fdfcf8" }],
    },
    {
      featureType: "road.highway",
      elementType: "geometry",
      stylers: [{ color: "#f8c967" }],
    },
    {
      featureType: "road.highway",
      elementType: "geometry.stroke",
      stylers: [{ color: "#e9bc62" }],
    },
    {
      featureType: "road.highway.controlled_access",
      elementType: "geometry",
      stylers: [{ color: "#e98d58" }],
    },
    {
      featureType: "road.highway.controlled_access",
      elementType: "geometry.stroke",
      stylers: [{ color: "#db8555" }],
    },
    {
      featureType: "road.local",
      elementType: "labels.text.fill",
      stylers: [{ color: "#806b63" }],
    },
    {
      featureType: "transit.line",
      elementType: "geometry",
      stylers: [{ color: "#dfd2ae" }],
    },
    {
      featureType: "transit.line",
      elementType: "labels.text.fill",
      stylers: [{ color: "#8f7d77" }],
    },
    {
      featureType: "transit.line",
      elementType: "labels.text.stroke",
      stylers: [{ color: "#ebe3cd" }],
    },
    {
      featureType: "transit.station",
      elementType: "geometry",
      stylers: [{ color: "#dfd2ae" }],
    },
    {
      featureType: "water",
      elementType: "geometry.fill",
      stylers: [{ color: "#b9d3c2" }],
    },
    {
      featureType: "water",
      elementType: "labels.text.fill",
      stylers: [{ color: "#92998d" }],
    },
  ];

var drawMap=(position)=>{
    googleMap.setOptions({
        center: position,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles:styleMap

    });

    new google.maps.Marker({ position, map: googleMap, icon: iconCar });
    directionsRenderer.setMap(googleMap);
}



// action on document onload




$( document ).ready(function() {
    if(isRecive!=undefined){
        if(isRecive==true){

        }
    }
    getCurrentLatLong();

});






