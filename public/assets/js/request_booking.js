//const { overArgs } = require("lodash");

var oldLocation={lat:"",log:""};
var source_latitude="";
var source_longitude="";
var sourceAdd="";
var destinationAdd="";
var destination_latitude=""; var  destination_longitude="";
var currentLocation={};
var bookingLocation={pickupLocations:{},destination:[]};
var options = {
    enableHighAccuracy: true,
    timeout: 4000,
    maximumAge: 0
};``
var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    var map;
 var mapInit=0;

var iconCar = {
    url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/storage/assets/img/Car@3x.png",
    scaledSize: new google.maps.Size(50, 50),
};
var iconS = {
    url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/storage/assets/img/location-2@3x.png",
    scaledSize: new google.maps.Size(50, 50),
};
var iconD = {
    url: "https://demos.mydevfactory.com/debarati/entrowebapp/public/storage/assets/img/location-1@3x.png",
    scaledSize: new google.maps.Size(50, 50),
};

var getCurrentLatLong=()=>{
    navigator.geolocation.getCurrentPosition(success, error);
};
var success=(pos)=> {
    currentLocation = pos.coords;
    customerBackgroundAPI(currentLocation);

};

function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
};

var customerBackgroundAPI=(currentLocation)=>{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/passenger/background/data",
        method:"POST",
        data:{ device_latitude:currentLocation.latitude,
        device_longitude:currentLocation.longitude, timeZone: "timeZone" },
        success: function(response){
            if(response.data.data.length>0){
                $("#bookingRequestForm").hide();
                $("#estimatedFareBlock").hide();
                $("#map").show();
                if(response.data.data[0].status=="STARTED"){
                    if($("#startedRequested").length==0){
                        $("#onRideContainer").html(rideStarted(response.data.data[0]));
                        $("#onRideContainer").show();
                        destination_latitude=response.data.data[0].s_latitude;
                        destination_longitude=response.data.data[0].s_longitude;
                        destinationAdd=response.data.data[0].s_address;

                        sourceAdd="N.A";
                        source_latitude=response.data.data[0].provider.latitude;
                        source_longitude=response.data.data[0].provider.longitude;
                        initialize();
                    }
                    else{
                        if(oldLocation.lat!=response.data.data[0].provider.latitude && oldLocation.log!=response.data.data[0].provider.longitude){
                            destination_latitude=response.data.data[0].s_latitude;
                            destination_longitude=response.data.data[0].s_longitude;
                            destinationAdd=response.data.data[0].s_address;
                            sourceAdd="N.A";
                            source_latitude=response.data.data[0].provider.latitude;
                            source_longitude=response.data.data[0].provider.longitude;
                            oldLocation.lat=source_latitude;
                            oldLocation.log=source_longitude;
                            calcRoute();
                        }
                    }
                }
                else if(response.data.data[0].status=="ARRIVED"){
                   // alert(88)
                    if($("#rideARRIVED").length==0 &&  response.data.data[0].paid==0){
                        $("#onRideContainer").html(rideARRIVED(response.data.data[0]));
                        $("#onRideContainer").show();
                        destination_latitude=response.data.data[0].s_latitude;
                        destination_longitude=response.data.data[0].s_longitude;
                        destinationAdd=response.data.data[0].s_address;

                        sourceAdd="N.A";
                        source_latitude=response.data.data[0].provider.latitude;
                        source_longitude=response.data.data[0].provider.longitude;
                        initialize();
                    }

                        if(response.data.data[0].paid==1) {
                          //  alert(103)
                            if(mapInit==1){
                                if(oldLocation.lat!=response.data.data[0].provider.latitude && oldLocation.log!=response.data.data[0].provider.longitude){
                                if(response.data.data[0].multiple_waypoints=="SINGLE"){
                                    sourceAdd="N.A";
                                    source_latitude=response.data.data[0].provider.latitude;
                                    source_longitude=response.data.data[0].provider.longitude;
                                    destinationAdd=response.data.data[0].d_address;;
                                    destination_latitude=response.data.data[0].d_latitude;
                                    destination_longitude=response.data.data[0].d_longitude;
                               }
                               else{
                                sourceAdd="N.A";
                                source_latitude=response.data.data[0].provider.latitude;
                                source_longitude=response.data.data[0].provider.longitude;
                                   destinationAdd=response.data.data[0].d_address;;
                                   destination_latitude=response.data.data[0].d_latitude;
                                   destination_longitude=response.data.data[0].d_longitude;
                              }
                              oldLocation.lat=source_latitude;
                              oldLocation.log=source_longitude;
                              calcRoute();
                            }


                            }
                            else{
                                mapInit=1;
                                if(response.data.data[0].multiple_waypoints=="SINGLE"){
                                    sourceAdd="N.A";
                        source_latitude=response.data.data[0].provider.latitude;
                        source_longitude=response.data.data[0].provider.longitude;
                                    destinationAdd=response.data.data[0].d_address;;
                                    destination_latitude=response.data.data[0].d_latitude;
                                    destination_longitude=response.data.data[0].d_longitude;
                               }
                               else{
                                sourceAdd="N.A";
                                source_latitude=response.data.data[0].provider.latitude;
                                source_longitude=response.data.data[0].provider.longitude;
                                   destinationAdd=response.data.data[0].d_address;;
                                   destination_latitude=response.data.data[0].d_latitude;
                                   destination_longitude=response.data.data[0].d_longitude;
                              }
                              initialize();
                            }
                            $("#onRideContainer").html(ridePICKEDUP(response.data.data[0]));
                            $("#onRideContainer").show();
                        }

                }
                else if(response.data.data[0].status=="PICKEDUP"){
                     // alert(156)
                    if(mapInit==1){
                        if(oldLocation.lat!=response.data.data[0].provider.latitude && oldLocation.log!=response.data.data[0].provider.longitude){
                        if(response.data.data[0].multiple_waypoints=="SINGLE"){
                            sourceAdd="N.A";
                            source_latitude=response.data.data[0].provider.latitude;
                            source_longitude=response.data.data[0].provider.longitude;
                            destinationAdd=response.data.data[0].d_address;;
                            destination_latitude=response.data.data[0].d_latitude;
                            destination_longitude=response.data.data[0].d_longitude;
                       }
                       else{
                           sourceAdd="N.A";
                           source_latitude=response.data.data[0].provider.latitude;
                           source_longitude=response.data.data[0].provider.longitude;
                           destinationAdd=response.data.data[0].d_address;;
                           destination_latitude=response.data.data[0].d_latitude;
                           destination_longitude=response.data.data[0].d_longitude;
                      }
                      oldLocation.lat=source_latitude;
                      oldLocation.log=source_longitude;
                      calcRoute();
                    }
                }
                    else{
                        mapInit=1;
                        if(response.data.data[0].multiple_waypoints=="SINGLE"){
                            sourceAdd="N.A";
                            source_latitude=response.data.data[0].provider.latitude;
                            source_longitude=response.data.data[0].provider.longitude;
                            destinationAdd=response.data.data[0].d_address;;
                            destination_latitude=response.data.data[0].d_latitude;
                            destination_longitude=response.data.data[0].d_longitude;
                       }
                       else{
                        sourceAdd="N.A";
                        source_latitude=response.data.data[0].provider.latitude;
                        source_longitude=response.data.data[0].provider.longitude;
                           destinationAdd=response.data.data[0].d_address;;
                           destination_latitude=response.data.data[0].d_latitude;
                           destination_longitude=response.data.data[0].d_longitude;
                      }
                      initialize();
                    }
                    $("#onRideContainer").html(ridePICKEDUP(response.data.data[0]));
                    $("#onRideContainer").show();
                }
                else if(response.data.data[0].status=="COMPLETED"){
                    if($("#ratingBlock").length==0){
                        $("#onRideContainer").html(rideCOMPLETED(response.data.data[0]));
                        $("#onRideContainer").show();
                    }
                    if(mapInit==1){
                        if(oldLocation.lat!=response.data.data[0].provider.latitude && oldLocation.log!=response.data.data[0].provider.longitude){
                        if(response.data.data[0].multiple_waypoints=="SINGLE"){
                            sourceAdd="N.A";
                            source_latitude=response.data.data[0].provider.latitude;
                            source_longitude=response.data.data[0].provider.longitude;
                            destinationAdd=response.data.data[0].d_address;;
                            destination_latitude=response.data.data[0].d_latitude;
                            destination_longitude=response.data.data[0].d_longitude;
                       }
                       else{
                           sourceAdd="N.A";
                           source_latitude=response.data.data[0].provider.latitude;
                           source_longitude=response.data.data[0].provider.longitude;
                           destinationAdd=response.data.data[0].d_address;;
                           destination_latitude=response.data.data[0].d_latitude;
                           destination_longitude=response.data.data[0].d_longitude;
                      }
                      oldLocation.lat=source_latitude;
                      oldLocation.log=source_longitude;
                      calcRoute();
                    }
                }
                    else{
                        mapInit=1;
                        if(response.data.data[0].multiple_waypoints=="SINGLE"){
                            sourceAdd="N.A";
                            source_latitude=response.data.data[0].provider.latitude;
                            source_longitude=response.data.data[0].provider.longitude;
                            destinationAdd=response.data.data[0].d_address;;
                            destination_latitude=response.data.data[0].d_latitude;
                            destination_longitude=response.data.data[0].d_longitude;
                       }
                       else{
                        sourceAdd="N.A";
                        source_latitude=response.data.data[0].provider.latitude;
                        source_longitude=response.data.data[0].provider.longitude;
                           destinationAdd=response.data.data[0].d_address;;
                           destination_latitude=response.data.data[0].d_latitude;
                           destination_longitude=response.data.data[0].d_longitude;
                      }
                      initialize();
                    }

                }
                else if(response.data.data[0].status=="SEARCHING"){
                    if($("#rideSEARCHING").length==0){
                    $("#onRideContainer").html(rideSEARCHING(response.data.data[0]));
                    $("#onRideContainer").show();
                    if(response.data.data[0].multiple_waypoints=="SINGLE"){
                         source_latitude=response.data.data[0].s_latitude;
                         source_longitude=response.data.data[0].s_longitude;
                         sourceAdd=response.data.data[0].s_address;
                         destinationAdd=response.data.data[0].d_address;;
                         destination_latitude=response.data.data[0].d_latitude;
                         destination_longitude=response.data.data[0].d_longitude;
                    }
                    else{
                        source_latitude=response.data.data[0].s_latitude;
                        source_longitude=response.data.data[0].s_longitude;
                        sourceAdd=response.data.data[0].s_address;
                        destinationAdd=response.data.data[0].d_address;;
                        destination_latitude=response.data.data[0].d_latitude;
                        destination_longitude=response.data.data[0].d_longitude;
                   }
                   initialize();
                }
            }

            }
            else{
                //if in booking page
                if(isBooking!=undefined){
                    if(isBooking==true){
                        // I am in request booking section
                        generateRequestForm("onload");
                        if($("#estimatedFareBlock").css('display')=='none' && $("#rideEst").length==0 && $("#onRideContainer").css("display")=="block"){
                            $("#bookingRequestForm").show();
                            $("#onRideContainer").hide();
                            $("#onRideContainer").html('');
                            $("#map").hide();

                        }
                    }
                }
            }
            getCurrentLatLong();
            if(overlay==0)
            $(".overlay").hide();

        },
        error: function(response){
            if(overlay==0)
            $(".overlay").hide();
            getCurrentLatLong();
        }
    });



};
var generateRequestForm=(isOnload)=>{
    var formCounter=$(".bookingFormShipx").length;
    html="";
    if(formCounter<6){
        times=(new Date()).getTime();
        html='<div class="col-lg-10 offset-lg-1 px-sm-3 bookingFormShipx '+isOnload+'">'+
             '<div class="input-group mb-4">'+
             '<input type="number" class="checkInput form-control inset-input pl-4 noPercel" min="1" max="9" id="noPercel_'+times+'" placeholder="Select the number of parcels / boxes">'+
             '</div>'+
             '<div class="input-group mb-4">'+
             '<input type="text" class="checkInput form-control inset-input pl-4 recipientsName" id="recipientsName_'+times+'" placeholder="Enter the recipient name">'+
             '</div>'+
             '<div class="input-group mb-4"><div style="position: relative;top: 20px;right: 10px0;left: 6px;z-index: 99;color: #495066;width: 0;">+1</div>'+
             '<input style="border-radius: 7px;" type="text" class="checkInput form-control inset-input pl-5 recipientsPh" onkeypress="validate(event);" id="recipientsPh_'+times+'" placeholder="Enter the recipient\'s phone number">'+
             '</div>'+
             '<div class="input-group mb-4">'+
             '<input type="text" class="checkInput form-control inset-input pl-4 dropPoint"  id="dropPoint_'+times+'"  placeholder="Enter recipient\'s location">'+
             '</div>'+
             '<div class="input-group mb-4">'+
                '<select class="checkInput form-control inset-input pl-4 deliveryOptions" id="deliveryOptions_'+times+'"  >'+
                    '<option value="">Select delivery options.</option>'+
                    '<option value="OFFICE">Office</option>'+
                    '<option value="RESIDENTAILS">House</option>'+
                '</select>'+
             '</div>'+
             '<div class="input-group mb-4">'+
             '<input type="text" class="checkInput form-control inset-input pl-4 description" id="description_'+times+'" placeholder="Description of items">'+
             '</div>'+
             '<div class="input-group mb-4">'+
                '<select class="checkInput form-control inset-input pl-4 deliveryItemCategory" id="deliveryItemCategory_'+times+'"  >'+
                    '<option value="">Select delivery item categoy</option>'+
                    '<option value="Documnests">Documnests</option>'+
                    '<option value="Legal Document">Legal Document</option>'+
                    '<option value="Gift">Gift</option>'+
                    '<option value="Auto Parts">Auto Parts</option>'+
                    '<option value="Household">Household</option>'+
                    '<option value="Delivery Item">Delivery Item</option>'+
                    '<option value="Medical Supply">Medical Supply</option>'+
                    '<option value="Office Supply">Office Supply</option>'+
                    '<option value="Boxes">Boxes</option>'+
                    '<option value="Others">Others</option>'+
                '</select>'+
             '</div>'+

             '<div class="input-group mb-4">'+
             '<select class="checkInput form-control inset-input pl-4 deliveryMethods" id="deliveryMethods_'+times+'"  >'+
                 '<option value="">Select delivery on Hand or Door.</option>'+
                 '<option value="Hand delivery">Hand delivery</option>'+
                 '<option value="Door to door">Door to door</option>'+
             '</select>'+
          '</div>'+
             '<div class="form-check">'+
             '<input type="checkbox" checked readonly style="pointer-events:none;" class="checkInput inputCheckBox form-check-input signature" id="signature_'+times+'">'+
             '<label class="form-check-label text-muted"   for="exampleCheck1">Request Recipient\'s signature on delivery/Free</label>'+
             '</div>'+
             '</div>';
            }
            if(html!="" && ((isOnload=="onload" && $(".onload").length==0)|| isOnload=="added"))
            {
                $("#dropLocation").append(html);
                autoCompleteInitailisationDestinaton(times);
            }
    }
var autoCompleteInitailisation=(element)=>{
    if(document.getElementById(element)!=undefined){
        google.maps.event.addListener(new google.maps.places.Autocomplete(document.getElementById(element)), 'place_changed', function () {
            var place = this.getPlace();
            bookingLocation.pickupLocations={
                address:document.getElementById(element).value,
                latitude:place.geometry.location.lat(),
                longitude:place.geometry.location.lng()
            }
        });
    }
};
var autoCompleteInitailisationDestinaton=(element)=>{
    bookingLocation.destination["destination_"+element]={dropPointAddress:{},noPercel:0,description:"",recipients_name:"",recipients_phone:"",signature:"",deliveryOptions:"",deliveryItemCategory:"",deliveryMethods:""};
    if(document.getElementById("dropPoint_"+element)!=undefined){
        google.maps.event.addListener(new google.maps.places.Autocomplete(document.getElementById("dropPoint_"+element)), 'place_changed', function () {
            var place = this.getPlace();
            bookingLocation.destination["destination_"+element].dropPointAddress={
                address:document.getElementById("dropPoint_"+element).value,
                latitude:place.geometry.location.lat(),
                longitude:place.geometry.location.lng()
            }
        });
    }
};

var rideStarted=(data)=>{

    var star='<i class="fa fa fa-star-o" aria-hidden="true"></i>'+
    '<i class="fa fa fa-star-o" aria-hidden="true"></i>'+
    '<i class="fa fa-star-o" aria-hidden="true"></i>'+
    '<i class="fa fa-star-o" aria-hidden="true"></i>'+
    '<i class="fa fa-star-o" aria-hidden="true"></i>';

    if(data.provider.rating>0 && data.provider.rating<1){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    if(data.provider.rating>1 && data.provider.rating<2){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    if(data.provider.rating>2 && data.provider.rating<3){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    if(data.provider.rating>3 && data.provider.rating<4){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    if(data.provider.rating>4 ){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>';
    }

    return '<div class="row" id="startedRequested">'+
    '<div class="card col-12" style="padding:0">'+
        '<h5 class="card-header col-12" style="max-height: 50px;">Driver is on the way to pick up</h5>'+
        '<div class="row mx-0">'+
            '<div class="col-lg-6 px-0 h-100">'+
                '<div class="border row py-3 mx-0" style="padding:0;border-bottom:0 !important;"><div class="col-lg-4 d-flex justify-content-lg-start justify-content-center">'+
                    '<img style="width:100%;object-fit:contain;" src="https://app.shipxnow.com/public/storage/'+data.provider.avatar+'" alt="'+data.provider.first_name+' '+data.provider.last_name+'"></div>'+
                    '<div class="col-lg-4"><div class="row"><div class="card-body p-1">'+
                        '<p class="card-text text-center">'+data.provider.first_name+' '+data.provider.last_name+'</p>'+
                    '</div></div>'+
                    '<div class="row"><div class="card-body p-1">'+
                        '<p class="card-text text-center">'+star+
                        '</p>'+
                    '</div></div></div>'+
                    '<div class="col-lg-4"><div class="card-body p-1">'+
                        '<p class="card-text text-center">'+
                            '<button type="button" style="height: 40px;width: 140px;font-size: unset;" class="btn btn-success-theme btn-lrg text-white" id="callDriver">Call</button>'+
                        '</p>'+
                    '</div></div>'+
                '</div>'+
            '</div>'+
            '<div class="col-lg-6 px-0 h-100">'+
                '<div class="border row py-3 mx-0" style="padding:0;border-bottom:0 !important;"><div class="col-lg-4 d-flex justify-content-lg-start justify-content-center">'+
                    '<img src="'+data.service_type.image+'" alt="'+data.service_type.description+'" style="width:100%;object-fit:contain;"></div>'+
                    '<div class="col-lg-4"><div class="row"><div class="card-body p-1">'+
                        '<p class="card-text text-center">'+data.service_type.name+'</p>'+
                    '</div></div>'+
                    '<div class="row"><div class="card-body p-1">'+
                    '<p class="card-text text-center">'+
                    '<lable>'+data.provider_service.service_model+'</lable>'+
                    '<lable>'+data.provider_service.service_number+'</lable>'+
                '</p>'+
                    '</div></div></div>'+
                    '<div class="col-lg-4"><div class="card-body p-1">'+
                        '<p class="card-text text-center">'+
                        '<button type="button" style="height: 40px;width: 160px;font-size: unset;" class="btn btn-success-theme btn-lrg text-white" reqID="'+data.id+'" id="cancelShipByPassenger">Cancel Ship</button>'+
                        '</p>'+
                    '</div></div>'+
                '</div>'+
            '</div>'+
        '</div>'+

    '</div>'+
'</div>';



    return '<div class="row" id="startedRequested">'+
            '<div class="col-lg-6 col-md-12 h-100">'+
            '<div class="row">'+
            '<div class="col-lg-4 d-flex justify-content-lg-start justify-content-center">'+
            '<img style="width:100px;object-fit:contain;" src="https://app.shipxnow.com/public/storage/'+data.provider.avatar+'" alt="'+data.provider.first_name+' '+data.provider.last_name+'">'+
            '</div>'+
            '<div class="col-lg-4">'+
            '<div class="row">'+
            '<div class="card-body">'+
            '<p class="card-text text-center">'+data.provider.first_name+' '+data.provider.last_name+'</p>'+
            '</div>'+
            '</div>'+
            '<div class="row">'+
            '<div class="card-body">'+
            '<p class="card-text text-center">'+star+
            '</p>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="col-lg-4">'+
            '<div class="card-body">'+
                '<p class="card-text text-center">'+
                    '<button type="button" style="height: 40px;width: 160px;font-size: unset;" class="btn btn-success-theme btn-lrg text-white" id="callDriver">Call</button>'+
                '</p>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="col-lg-6 col-md-12 h-100">'+
            '<div class="row">'+
            '<div class="col-lg-4 d-flex justify-content-lg-start justify-content-center">'+
            '<img src="'+data.service_type.image+'" alt="'+data.service_type.description+'" style="width:100px;object-fit:contain;">'+
            '</div>'+
            '<div class="col-lg-4">'+
            '<div class="row">'+
            '<div class="card-body">'+
            '<p class="card-text text-center">'+data.service_type.name+'</p>'+
            '</div>'+
            '</div>'+
            '<div class="row">'+
            '<div class="card-body">'+
                '<p class="card-text text-center">'+
                    '<lable>'+data.provider_service.service_model+'</lable>'+
                    '<lable>'+data.provider_service.service_number+'</lable>'+
                '</p>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="col-lg-4">'+
            '<div class="card-body">'+
            '<p class="card-text text-center">'+
            '<button type="button" style="height: 40px;width: 160px;font-size: unset;" class="btn btn-success-theme btn-lrg text-white" reqID="'+data.id+'" id="cancelShipByPassenger">Cancel Ship</button>'+
            '</p>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>';
}
var rideARRIVED=(data)=>{

return '<div class="row" id="rideARRIVED">'+
            '<div class="col-6">'+
                '<div class="card">'+
                    '<h3 style="width: 100%;text-align: center;" class="card-header">Invoice</h3>'+
                    '<div class="card-body" style="width: 100%;">'+
                        '<div class="row">'+
                            '<div class="col-6">Booking ID</div>'+
                            '<div class="col-6">'+data.booking_id+'</div>'+
                        '</div>'+
                        '<div class="row">'+
                            '<div class="col-6">Base Fee</div>'+
                            '<div class="col-6">$'+data.payment.distance+'</div>'+
                        '</div>'+
                        '<div class="row">'+
                            '<div class="col-6">Tax</div>'+
                            '<div class="col-6">$'+data.payment.tax+'</div>'+
                        '</div>'+
                        '<div class="row" style="color: orange;font-weight: bold;">'+
                            '<div class="col-6">Promotional Discount</div>'+
                            '<div class="col-6">$'+data.payment.discount+'</div>'+
                        '</div>'+
                    '</div>'+
                    '<h4 style="width: 100%;text-align: center;background-color: #fff;border: navajowhite;padding-bottom: 0;" class="card-header">Total</h4>'+
                    '<h4 style="width: 100%;text-align: center;background-color: #fff;border: none;" class="card-header">$'+data.payment.total+'</h4>'+
                    '<div class="card-footer" style="width: 100%;text-align: center;">'+
                    '<p class="card-text">'+'<i class="fa fa-credit-card" aria-hidden="true"></i> XXXX-XXXX-XXXX-'+$("#hiddenCard").val()+'</p>'+
                    '<p class="card-text">'+
                        '<button type="button" style="height: 40px;width: 160px;font-size: unset;" reqID="'+data.id+'" class="btn btn-success-theme btn-lrg text-white" id="paynow">Pay Now</button>'+
                    '</p>'+
                    '</div>'+

            '</div>'+
        '</div>'+
    '</div>';
}

var ridePICKEDUP=(data)=>{
    var star='<i class="fa fa fa-star-o" aria-hidden="true"></i>'+
    '<i class="fa fa fa-star-o" aria-hidden="true"></i>'+
    '<i class="fa fa-star-o" aria-hidden="true"></i>'+
    '<i class="fa fa-star-o" aria-hidden="true"></i>'+
    '<i class="fa fa-star-o" aria-hidden="true"></i>';

    if(data.provider.rating>0 && data.provider.rating<1){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    if(data.provider.rating>1 && data.provider.rating<2){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    if(data.provider.rating>2 && data.provider.rating<3){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    if(data.provider.rating>3 && data.provider.rating<4){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    if(data.provider.rating>4 ){
        var star='<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>'+
        '<i class="fa fa-star" aria-hidden="true"></i>';
    }

    return '<div class="row" id="ridePICKEDUP">'+
                '<div class="card col-12" style="padding:0">'+
                    '<h5 class="card-header col-12" style="max-height: 50px;">Your shipment is on the way</h5>'+
                    '<div class="row mx-0">'+
                        '<div class="col-lg-6 px-0 h-100">'+
                            '<div class="border row py-3 mx-0" style="padding:0;border-bottom:0 !important;"><div class="col-lg-4 d-flex justify-content-lg-start justify-content-center">'+
                                '<img style="width:100%;object-fit:contain;" src="https://app.shipxnow.com/public/storage/'+data.provider.avatar+'" alt="'+data.provider.first_name+' '+data.provider.last_name+'"></div>'+
                                '<div class="col-lg-4"><div class="row"><div class="card-body p-1">'+
                                    '<p class="card-text text-center">'+data.provider.first_name+' '+data.provider.last_name+'</p>'+
                                '</div></div>'+
                                '<div class="row"><div class="card-body p-1">'+
                                    '<p class="card-text text-center">'+star+
                                    '</p>'+
                                '</div></div></div>'+
                                '<div class="col-lg-4"><div class="card-body p-1">'+
                                    '<p class="card-text text-center">'+
                                        '<button type="button" style="height: 40px;width: 140px;font-size: unset;" class="btn btn-success-theme btn-lrg text-white" id="callDriver">Call</button>'+
                                    '</p>'+
                                '</div></div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-lg-6 px-0 h-100">'+
                            '<div class="border row py-3 mx-0" style="padding:0;border-bottom:0 !important;"><div class="col-lg-4 d-flex justify-content-lg-start justify-content-center">'+
                                '<img src="'+data.service_type.image+'" alt="'+data.service_type.description+'" style="width:100%;object-fit:contain;"></div>'+
                                '<div class="col-lg-4"><div class="row"><div class="card-body p-1">'+
                                    '<p class="card-text text-center">'+data.service_type.name+'</p>'+
                                '</div></div>'+
                                '<div class="row"><div class="card-body p-1">'+
                                    '<p class="card-text text-center">'+
                                        '<lable>'+data.provider_service.service_model+'</lable>'+
                                        '<lable>'+data.provider_service.service_number+'</lable>'+
                                    '</p>'+
                                '</div></div></div>'+
                                '<div class="col-lg-4"><div class="card-body p-1">'+
                                    '<p class="card-text text-center">'+
                                        '<button type="button" style="height: 40px;width: 140px;font-size: unset;" class="btn btn-success-theme btn-lrg text-white" id="shareShipByPassenger">Share</button>'+
                                    '</p>'+
                                '</div></div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+

                '</div>'+
            '</div>';
}

var rideCOMPLETED=(data)=>{
    return '<div class="row" id="ratingBlock">'+
                '<div class="card col-lg-6 col-md-12" style="padding:0">'+
                    '<h5 class="card-header col-12" style="max-height: 50px;">Rate your experience with '+data.provider.first_name+' '+data.provider.last_name+'</h5>'+
                    '<div class="row">'+
                        '<div class="col-12">'+
                            '<div class="card col-12 pt-3" style="padding:0">'+
                                '<img style="width:100px" src="https://app.shipxnow.com/public/storage/'+data.provider.avatar+'" alt="'+data.provider.first_name+' '+data.provider.last_name+'">'+
                                 '<div class="card-body">'+
                                 '<p class="card-text"><input type="hidden" id="reviewRating" value="1">'+
                                        '<lable id="starOneLable" style="cursor: pointer;"><i id="starOne" style="font-size: 24px;color: orange;"   class="fa fa-star" aria-hidden="true"></i></lable>'+
                                        '<lable id="starTwoLable" style="cursor: pointer;"><i id="starTwo"   style="font-size: 24px;color: orange;"   class="fa fa-star-o" aria-hidden="true"></i></lable>'+
                                        '<lable id="starThreeLable" style="cursor: pointer;"><i id="starThree" style="font-size: 24px;color: orange;" class="fa fa-star-o" aria-hidden="true"></i></lable>'+
                                        '<lable id="starFourLable" style="cursor: pointer;"><i id="starFour"  style="font-size: 24px;color: orange;"  class="fa fa-star-o" aria-hidden="true"></i></lable>'+
                                        '<lable id="starFiveLable" style="cursor: pointer;"><i id="starFive"  style="font-size: 24px;color: orange;"  class="fa fa-star-o" aria-hidden="true"></i></lable>'+
                                    '</p>'+
                                '</div>'+
                                '<div class="card-body mx-0">'+
                                '<p class="card-text">'+
                                    '<div class="form-group">'+
                                        '<textarea style="border: none;border-bottom: 1px #ccc solid;border-radius: 0;" type="text" class="form-control" id="commentRivew" placeholder="Write your comment"></textarea>'+
                                    '</div>'+
                                '</p>'+
                            '</div>'+
                                '<div class="card-body">'+
                                    '<p class="card-text">'+
                                        '<button type="button" style="height: 40px;width: 160px;font-size: unset;" reqID="'+data.id+'" class="btn btn-success-theme btn-lrg text-white" id="submintRating">SUBMIT</button>'+
                                    '</p>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+

                    '</div>'+

                '</div>'+
            '</div>';
}
var rideSEARCHING=(data)=>{
    console.log(data);
    return '<div class="row" id="rideSEARCHING">'+
                '<div class="card col-lg-6 col-md-12" style="padding:0">'+
                    '<h5 class="card-header col-12" style="max-height: 50px;">Finding near by driver</h5>'+
                    '<div class="row">'+
                        '<div class="col-12">'+
                            '<div class="card col-12" style="padding:0">'+
                                '<div class="card-body">'+
                                    '<p class="card-text">'+
                                        '<button type="button" style="height: 40px;width: 160px;font-size: unset;" class="btn btn-success-theme btn-lrg text-white" id="cancelDriverSearch">Cancel Ship</button>'+
                                    '</p>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+

                    '</div>'+

                '</div>'+
            '</div>';
}

$(document).on("click",".addDestination", function(e){
    for(var x=0;x<$(".checkInput").length;x++){
        if($($(".checkInput")[x]).val()==""){
            return false;
        }
    }
    generateRequestForm("added");
});

$(document).on("change",".noPercel", function(e){
    var ASCIICode = (e.which) ? e.which : e.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
        if(parseInt($('.noPercel').val())<=0 || parseInt($('.noPercel').val())>9 ){
          //  alert();
                return false;
            }

        var id=(e.target.id).split("_")[1];
        bookingLocation.destination["destination_"+id].noPercel=e.target.value;
        //{pickupPointAddres:{},dropPointAddress:{},noPercel:0,description:"",recipients_name:"",signature:""};
    });
    $(document).on("keypress",".noPercel", function(e){
        var ASCIICode = (e.which) ? e.which : e.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
        if(parseInt($('.noPercel').val())<=0 || parseInt($('.noPercel').val())>9 ){
          //  alert();
                return false;
            }
        return true;
    });

    $(document).on("keypress",".recipientsPh", function(e){
        var ASCIICode = (e.which) ? e.which : e.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
        return true;
    });


$(document).on("change",".description", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].description=e.target.value;
});
$(document).on("change",".recipientsName", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].recipients_name=e.target.value;
});
$(document).on("change",".recipientsPh", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].recipients_phone="+1"+e.target.value;
});

$(document).on("change",".signature", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].signature=e.target.value;
});
$(document).on("change",".deliveryMethods", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].deliveryMethods=e.target.value;
});
$(document).on("change",".deliveryItemCategory", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].deliveryItemCategory=e.target.value;
});
$(document).on("change",".deliveryOptions", function(e){
    var id=(e.target.id).split("_")[1];
    bookingLocation.destination["destination_"+id].deliveryOptions=e.target.value;
});

// get estimated fare
$(document).on("click","#getEstimatedFare",function(){
    overlay=1;
    $(".overlay").show();
    for(var x=0;x<$(".inputCheckBox").length;x++){
        if($($(".inputCheckBox")[x]).prop("checked")==false){
            $(".overlay").hide();
            $("#tac").click();
            return false;

        }
    }

    // check for the client side validation

    console.log(bookingLocation);
    var destinationKey=Object.keys(bookingLocation.destination);
    var newDestinationArrOfObj=[];

    var data={pickupLocations:bookingLocation.pickupLocations,destination:newDestinationArrOfObj};
    source_latitude=bookingLocation.pickupLocations.latitude;
    source_longitude=bookingLocation.pickupLocations.longitude;
    sourceAdd=bookingLocation.pickupLocations.address;

    if(source_latitude=="" || source_longitude=="" || sourceAdd=="" || source_latitude==undefined || source_longitude==undefined || sourceAdd==undefined ){
        $("#pickupLocations").val('');
        $("#pickupLocations").focus();
        $(".overlay").hide();
        alert("Please select pickup location");
        return false;
    }
    for(var i=0;i<destinationKey.length; i++){
        newDestinationArrOfObj.push(bookingLocation.destination[destinationKey[i]]);
        destination_latitude=bookingLocation.destination[destinationKey[i]].dropPointAddress.latitude;
        destination_longitude=bookingLocation.destination[destinationKey[i]].dropPointAddress.longitude;
        destinationAdd=bookingLocation.destination[destinationKey[i]].dropPointAddress.address;
    }


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+"/passenger/get/estimated/fare",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){
            overlay=0;
            $("#bookingRequestForm").hide();
            $("#map").show();
            $("#estimatedFareBlock").show();
            initialize();
            var html=""; var paymentInfo="";
            for(var x=0;x<response.data.response.length;x++){
                console.log(response.data.response[x]);
                html=html+'<div class="carousel-item col-md-4 active serviceTypeClass"  style="box-shadow: 0 0 7px #ddd;background-color: #f5f6f7;padding: 10px;">'+
                '<div class="panel panel-default">'+
                   '<div class="panel-thumbnail">'+
                     '<div class="thumb">'+
                       '<img class="img-fluid mx-auto d-block" src="'+response.data.response[x].image+'" style="width: 150px;" alt="slide 1">'+
                     '</div>'+
                   '</div>'+
                 '<lable style="display: block;width: 100%;padding: 5px;text-align: center;font-size: 12px;font-weight: bold;padding-top: 10px;">'+response.data.response[x].description+' </lable>'+
                 '<div style="display: flex; justify-content:center"><input type="radio"  id="carType_'+response.data.response[x].id+'" fare="'+response.data.response[x].estimated_fare+'", distance="'+response.data.response[x].sourceToDestinationDistance+'" serviceType="'+response.data.response[x].id+'" name="carType" '+
                        'class="serviceSection" style="background: orange;pointer-events: none;margin-top: 10px;">'+
                 '<lable style="margin: 0;font-size: 15px;font-weight: bold;padding: 5px;color: orange;">$'+response.data.response[x].estimated_fare+' </lable></div>'+
                 '<lable  style="display: block;text-align: center;font-size: 14px;padding: 5px;">Estimated Distance '+response.data.response[x].sourceToDestinationDistance+' Miles</lable>'+

                 '</div>'+
             '</div>';
            }
            for(var x=0;x<response.getUserCard.data.length;x++){
                if(response.getUserCard.data[x].is_default==0){
                    paymentInfo='<div id="cInfo" card="'+response.getUserCard.data[x].card_id+'"><i class="fa fa-credit-card" aria-hidden="true"></i> XXXX-XXXX-XXXX-'+response.getUserCard.data[x].last_four+'</div>';
                 // alert(1);
                    $("#hiddenCard").val(response.getUserCard.data[x].last_four);
                }
            }
            if(paymentInfo==""){
                if(response.getUserCard.data.length>0){
                    paymentInfo='<div id="cInfo" card="'+response.getUserCard.data[0].card_id+'"><i class="fa fa-credit-card" aria-hidden="true"></i> XXXX-XXXX-XXXX-'+response.getUserCard.data[0].last_four+'</div>';
                //  alert(0);
                    $("#hiddenCard").val(response.getUserCard.data[0].last_four);
                }
            }
            if(paymentInfo==""){
                paymentInfo='<div>Select Payment Option</div>';
            }
            if(html!="")
            {
                $("#carouseSliderListBox").html(html);

                $("#paymentInfo").html(paymentInfo);
            }


        },
        error: function(response){
            overlay=0;
            $("#bookingRequestForm").show();
            $("#map").hide();
            $("#estimatedFareBlock").hide();
            alert(response.responseJSON.message);
        }
    });






});
$(document).on("click",".serviceTypeClass",function(e){
    $(".serviceSection").prop("checked",false);
     $($($($($(e.currentTarget).children()).children()).eq(2)).children().eq(0)).prop("checked",true);

  })
$(document).on("click","#rideNow",function(e){

    if($(".serviceSection:checked").length==1){
        // check for the client side validation
        var destinationKey=Object.keys(bookingLocation.destination);
        var newDestinationArrOfObj=[];
        for(var i=0;i<destinationKey.length; i++){
            newDestinationArrOfObj.push(bookingLocation.destination[destinationKey[i]]);
        }
        var data={pickupLocations:bookingLocation.pickupLocations,destination:newDestinationArrOfObj,ty:"est"};
        data.service_type=$(".serviceSection:checked").attr("servicetype");
        data.card_id=$("#cInfo").attr("card");
        data.distance=$(".serviceSection:checked").attr("distance");
        data.estimated_fare=$(".serviceSection:checked").attr("fare");
        data.payment_mode="CARD";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        overlay=1;
        $(".overlay").show();
        $.ajax({
            url: APP_URL+"/passenger/request/ride",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){
            overlay=0;

            var estHtml='<div class="row" id="rideEst">'+
            '<div class="col-lg-6 col-md-12">'+
                '<div class="card">'+
                '<h4 class="card-title">Estimation Fare</h4>'+

                    '<div class="card-body" style="width: 100%;">'+

                        '<div class="row">'+
                            '<div class="col-6">Estimation Fare</div>'+
                            '<div class="col-6">$'+response.data.estimated_fare+'</div>'+
                        '</div>'+
                        '<div class="row">'+
                            '<div class="col-6">EAT</div>'+
                            '<div class="col-6">'+response.data.time+'</div>'+
                        '</div>'+
                        '<div class="row" style="">'+
                            '<div class="col-6">Type</div>'+
                            '<div class="col-6">Shipx-XL</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="card-footer" style="width: 100%;text-align: center;">'+
                    '<p class="card-text">'+
                        '<button type="button" style="height: 40px;width: 160px;font-size: unset;"  class="btn btn-success-theme btn-lrg text-white" id="requestNowNow">Request Now</button>'+
                    '</p>'+
                    '</div>'+

            '</div>'+
        '</div>'+
    '</div>';
            $("#onRideContainer").html(estHtml);
            $("#onRideContainer").show();
            $("#estimatedFareBlock").hide();


        },
        error: function(response){
            overlay=0;
        //    alert(response.message);
        }
    });




    }
});
$(document).on("click","#requestNowNow",function(e){
    if($(".serviceSection:checked").length==1){
        // check for the client side validation
        var destinationKey=Object.keys(bookingLocation.destination);
        var newDestinationArrOfObj=[];
        for(var i=0;i<destinationKey.length; i++){
            newDestinationArrOfObj.push(bookingLocation.destination[destinationKey[i]]);
        }
        var data={pickupLocations:bookingLocation.pickupLocations,destination:newDestinationArrOfObj,ty:"rqst"};
        data.service_type=$(".serviceSection:checked").attr("servicetype");
        data.card_id=$("#cInfo").attr("card");
        data.distance=$(".serviceSection:checked").attr("distance");
        data.estimated_fare=$(".serviceSection:checked").attr("fare");
        data.payment_mode="CARD";
        overlay=1;
        $(".overlay").show();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: APP_URL+"/passenger/request/ride",
        method:"POST",
        data:data,//JSON.stringify(bookingLocation),
        success: function(response){
overlay=0;
        },
        error: function(response){
overlay=0;
           // alert(response.message);
        }
    });




    }
});

$(document).on("click","#starOneLable",function(e){
    $("#starTwo").removeClass("fa-star");
    $("#starTwo").addClass("fa-star-o");

    $("#starThree").removeClass("fa-star");
    $("#starThree").addClass("fa-star-o");

    $("#starFour").removeClass("fa-star");
    $("#starFour").addClass("fa-star-o");

    $("#starFive").removeClass("fa-star");
    $("#starFive").addClass("fa-star-o");
    $("#reviewRating").val(1);
});
$(document).on("click","#starTwoLable",function(e){

    $("#starThree").removeClass("fa-star");
    $("#starThree").addClass("fa-star-o");

    $("#starFour").removeClass("fa-star");
    $("#starFour").addClass("fa-star-o");

    $("#starFive").removeClass("fa-star");
    $("#starFive").addClass("fa-star-o");

    $("#starTwo").removeClass("fa-star-o");
    $("#starTwo").addClass("fa-star");
    $("#reviewRating").val(2);

});
$(document).on("click","#starThreeLable",function(e){
    $("#starFour").removeClass("fa-star");
    $("#starFour").addClass("fa-star-o");
    $("#starFive").removeClass("fa-star");
    $("#starFive").addClass("fa-star-o");

    $("#starTwo").removeClass("fa-star-o");
    $("#starTwo").addClass("fa-star");

    $("#starThree").removeClass("fa-star-o");
    $("#starThree").addClass("fa-star");
    $("#reviewRating").val(3);

});
$(document).on("click","#starFourLable",function(e){
    $("#starFive").removeClass("fa-star");
    $("#starFive").addClass("fa-star-o");
    $("#starTwo").removeClass("fa-star-o");
    $("#starTwo").addClass("fa-star");

    $("#starThree").removeClass("fa-star-o");
    $("#starThree").addClass("fa-star");
    $("#starFour").removeClass("fa-star-o");
    $("#starFour").addClass("fa-star");
    $("#reviewRating").val(4);
});
$(document).on("click","#starFiveLable",function(e){
    $("#starTwo").removeClass("fa-star-o");
    $("#starTwo").addClass("fa-star");

    $("#starThree").removeClass("fa-star-o");
    $("#starThree").addClass("fa-star");
    $("#starFour").removeClass("fa-star-o");
    $("#starFour").addClass("fa-star");
    $("#starFive").removeClass("fa-star-o");
    $("#starFive").addClass("fa-star");
    $("#reviewRating").val(5);
});

$(document).on("click","#submintRating",function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    overlay=1;
    $(".overlay").show();
    $.ajax({
        url: APP_URL+"/passenger/request/ratingComment",
        method:"POST",
        data:{ rating:$("#reviewRating").val(),
        comment:$("#commentRivew").val(),request_id:$("#submintRating").attr("reqID") },
        success: function(response){
            overlay=0;
        },
        error: function(response){
            overlay=0;
         //   getCurrentLatLong();
        }
    });


});

$(document).on("click","#cancelShipByPassenger",function(){
   var modelHtml='<div class="card">'+
   '<div class="card-body">'+
     '<h4 class="card-title">Cancel Request</h4>'+
     '<h6 class="card-subtitle mb-2 text-muted"></h6>'+
     '<p class="card-text">'+
     '  $5 will be deducted from your account as cancellation charges. Do you really want to cancel the ship?'+
     '</p>'+
     '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class="card-link btn btn-success-theme btn-lrg text-white" id="cancelNoPass" >NO</a>'+
     '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class=" card-link btn btn-success-theme btn-lrg text-white" id="cancelYesPass" >YES</a>'+
   '</div>'+
 '</div>';
$("#modalContent").html(modelHtml);
var modal = document.getElementById("myModal");
modal.style.display = "block";


});
$(document).on("click","#cancelNoPass",function(){
    var modal = document.getElementById("myModal");
    modal.style.display = "none";


 });
 $(document).on("click","#cancelYesPass",function(){
    var modelHtml='<div class="card">'+
    '<div class="card-body">'+
      '<h4 class="card-title">Reasn for cancelling </h4>'+
      '<h6 class="card-subtitle mb-2 text-muted"></h6>'+
      '<p class="card-text">'+
      '<div class="form-group">'+
      '<input style="border: none;border-bottom: 1px #ccc solid;border-radius: 0;" type="text" class="form-control" id="cancleRidePassCommet" placeholder="Write your comment">'+
      '</div>'+
      '</p>'+

      '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class=" card-link btn btn-success-theme btn-lrg text-white" id="cancelYesPassSubmitd" >SUBMIT</a>'+
    '</div>'+
  '</div>';
 $("#modalContent").html(modelHtml);
 var modal = document.getElementById("myModal");
 modal.style.display = "block";


 });
 $(document).on("click","#cancelYesPassSubmitd",function(){
    overlay=1;
    $(".overlay").show();
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: APP_URL+"/passenger/cancel/ride",
        method:"POST",
        data:{
        comment:$("#cancleRidePassCommet").val(),request_id:$("#cancelShipByPassenger").attr("reqID") },
        success: function(response){
            overlay=0;
           var modal = document.getElementById("myModal");
            modal.style.display = "block";
        },
        error: function(response){
            overlay=0;
         //   getCurrentLatLong();
        }
    });
});

$(document).on("click","#paynow",function(){
    $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
   overlay=1;
    $(".overlay").show();
   $.ajax({
       url: APP_URL+"/passenger/pay/ride",
       method:"POST",
       data:{
        tips:0,request_id:$("#paynow").attr("reqID") },
       success: function(response){
           overlay=0;
          // var modal = document.getElementById("myModal");
          // modal.style.display = "block";
       },
       error: function(response){
           overlay=0;
        //   getCurrentLatLong();
       }
   });
});

/***gmap */
function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
    var source = new google.maps.LatLng(source_latitude,source_longitude);
    var styleMap=[
        { elementType: "geometry", stylers: [{ color: "#ebe3cd" }] },
        { elementType: "labels.text.fill", stylers: [{ color: "#523735" }] },
        { elementType: "labels.text.stroke", stylers: [{ color: "#f5f1e6" }] },
        {
          featureType: "administrative",
          elementType: "geometry.stroke",
          stylers: [{ color: "#c9b2a6" }],
        },
        {
          featureType: "administrative.land_parcel",
          elementType: "geometry.stroke",
          stylers: [{ color: "#dcd2be" }],
        },
        {
          featureType: "administrative.land_parcel",
          elementType: "labels.text.fill",
          stylers: [{ color: "#ae9e90" }],
        },
        {
          featureType: "landscape.natural",
          elementType: "geometry",
          stylers: [{ color: "#dfd2ae" }],
        },
        {
          featureType: "poi",
          elementType: "geometry",
          stylers: [{ color: "#dfd2ae" }],
        },
        {
          featureType: "poi",
          elementType: "labels.text.fill",
          stylers: [{ color: "#93817c" }],
        },
        {
          featureType: "poi.park",
          elementType: "geometry.fill",
          stylers: [{ color: "#a5b076" }],
        },
        {
          featureType: "poi.park",
          elementType: "labels.text.fill",
          stylers: [{ color: "#447530" }],
        },
        {
          featureType: "road",
          elementType: "geometry",
          stylers: [{ color: "#f5f1e6" }],
        },
        {
          featureType: "road.arterial",
          elementType: "geometry",
          stylers: [{ color: "#fdfcf8" }],
        },
        {
          featureType: "road.highway",
          elementType: "geometry",
          stylers: [{ color: "#f8c967" }],
        },
        {
          featureType: "road.highway",
          elementType: "geometry.stroke",
          stylers: [{ color: "#e9bc62" }],
        },
        {
          featureType: "road.highway.controlled_access",
          elementType: "geometry",
          stylers: [{ color: "#e98d58" }],
        },
        {
          featureType: "road.highway.controlled_access",
          elementType: "geometry.stroke",
          stylers: [{ color: "#db8555" }],
        },
        {
          featureType: "road.local",
          elementType: "labels.text.fill",
          stylers: [{ color: "#806b63" }],
        },
        {
          featureType: "transit.line",
          elementType: "geometry",
          stylers: [{ color: "#dfd2ae" }],
        },
        {
          featureType: "transit.line",
          elementType: "labels.text.fill",
          stylers: [{ color: "#8f7d77" }],
        },
        {
          featureType: "transit.line",
          elementType: "labels.text.stroke",
          stylers: [{ color: "#ebe3cd" }],
        },
        {
          featureType: "transit.station",
          elementType: "geometry",
          stylers: [{ color: "#dfd2ae" }],
        },
        {
          featureType: "water",
          elementType: "geometry.fill",
          stylers: [{ color: "#b9d3c2" }],
        },
        {
          featureType: "water",
          elementType: "labels.text.fill",
          stylers: [{ color: "#92998d" }],
        },
      ];
    var mapOptions = {
        zoom: 14,
        center: source,
        style: styleMap
    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    directionsDisplay.setMap(map);
    calcRoute();
}
function calcRoute() {
    var start = new google.maps.LatLng(source_latitude,source_longitude);
    var end = new google.maps.LatLng(destination_latitude,destination_longitude);
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(start);
    bounds.extend(end);
    map.fitBounds(bounds);

    var markerS = new google.maps.Marker({
        position: start,
        map: map,
        icon: iconS
    });
    if(sourceAdd!="" && sourceAdd!="N.A"){
        var infowindowS = new google.maps.InfoWindow({
            content: sourceAdd
        });
        infowindowS.open(map,markerS);
    }

    var markerD = new google.maps.Marker({
        position: end,
        map: map,
        icon: iconD
    });
    if(destinationAdd!="" && destinationAdd!="N.A"){
    var infowindowD = new google.maps.InfoWindow({
        content: destinationAdd
        });
    infowindowD.open(map,markerD);
}

    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            directionsDisplay.setMap(map);
        } else {
            alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
        }
    });
}

/***end of gmap */


function validate(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
    // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  $(document).on("click","#tac",function(){
    var modelHtml='<div class="card">'+
    '<div class="card-body">'+
      '<h4 class="card-title">Self Declaration</h4>'+
      '<h6 class="card-subtitle mb-2 text-muted">Terms and Conditions</h6>'+
      '<p class="card-text">'+
      'I certify under penalty of perjury that this shipment does not contain any hazardous or dangerous materials, illegal substance, no weapons, no cash, no drugs, no alcohol. I understand I will be prosecuted in state or federal court for false declaration.'+
      '</p>'+
      '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class="card-link btn btn-success-theme btn-lrg text-white" id="acceptTerms" >Accept</a>'+
      '<a href="javascript:void(0);" style="height: 40px;width: 160px;font-size: unset;" class=" card-link btn btn-success-theme btn-lrg text-white" id="declineTerms" >Decline</a>'+
    '</div>'+
  '</div>';
 $("#modalContent").html(modelHtml);
 var modal = document.getElementById("myModal");
 modal.style.display = "block";
});
$(document).on("click","#acceptTerms",function(){
    $("#selfDeclaration").prop("checked",true);
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
});
$(document).on("click","#declineTerms",function(){
    $("#selfDeclaration").prop("checked",false);
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
});


// action on document onload
$( document ).ready(function() {
    if(isBooking!=undefined){
        if(isBooking==true){
            autoCompleteInitailisation("pickupLocations");
        }
    }
    getCurrentLatLong();

});






