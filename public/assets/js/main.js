$(document).ready(function() {
    $('#datepicker1').datetimepicker({
        format: 'DD-MM-YYYY',
    });
    $('#vehicle_expiration_date').datetimepicker({
        format: 'YYYY-MM-DD',
    });
    $('#timepicker1, #timepicker2').datetimepicker({
        format: 'hh:mm A',
    });
    $('#selectpicker1, #selectpicker2, #languagePicker').selectpicker();

    $(function() {
        $('.center').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            arrows: false,
            responsive: [
            {
                breakpoint: 768,
                settings: {
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1
                }
            }
            ]
        });
    });

    // Vanilla Javascript
    // var input = document.querySelector("#telephone");
    // window.intlTelInput(input,({
    // // options here
    // }));

    // // jQuery
    // $("#telephone").intlTelInput({
    // // options here
    // });
});

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

//Get the button:
mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
