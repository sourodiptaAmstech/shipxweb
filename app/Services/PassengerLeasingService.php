<?php

namespace App\Services;

use App\Services\CurlService;


class PassengerLeasingService
{
	private function hourlyLease($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/leasing/hourly';
	      $method = "POST";

	      $array = [
	      	'dateOfJourny'=>$data->dateOfJourny,
	      	'fromTime'=>$data->fromTime,
	      	'toTime'=>$data->toTime,
	      	'services_type_id'=>$data->services_type_id,
	      	'longitude'=>$data->longitude,
	      	'latitude'=>$data->latitude,
	      	'no_passenger'=>$data->no_passenger,
	      	'address'=>$data->address,
	      	'details'=>$data->details,
	      	'vehicle_equipment'=>$data->vehicle_equipment, 
	      ];

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function dailyLease($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/leasing/daily';
	      $method = "POST";

	      $array = [
	      	'dateOfJourny'=>$data->dateOfJourny,
	      	'services_type_id'=>$data->services_type_id,
	      	'longitude'=>$data->longitude,
	      	'latitude'=>$data->latitude,
	      	'no_passenger'=>$data->no_passenger,
	      	'address'=>$data->address,
	      	'details'=>$data->details,
	      	'vehicle_equipment'=>$data->vehicle_equipment, 
	      ];

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function longLease($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/leasing/longtime';
	      $method = "POST";

	      $array = [
	      	'fromdate'=>$data->fromdate,
	      	'todate'=>$data->todate,
	      	'services_type_id'=>$data->services_type_id,
	      	'longitude'=>$data->longitude,
	      	'latitude'=>$data->latitude,
	      	'address'=>$data->address,
	      	'details'=>$data->details,
	      	'vehicle_equipment'=>$data->vehicle_equipment, 
	      ];

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function serviceLeasingList($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/service/leasing/list';
	      $method = "GET";
	      $json_encode = "";
	      $timeZone = $data->timeZone;
	     
	      $curlService = new CurlService;
	      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	public function accessHourlyLease($data)
	{
    	return $this->hourlyLease($data);
  	}

  	public function accessDailyLease($data)
	{
    	return $this->dailyLease($data);
  	}

  	public function accessLongLease($data)
	{
    	return $this->longLease($data);
  	}

  	public function accessServiceLeasingList($data)
	{
    	return $this->serviceLeasingList($data);
  	}

}