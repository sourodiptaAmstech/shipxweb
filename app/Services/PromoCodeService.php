<?php

namespace App\Services;

use App\Services\CurlService;


class PromoCodeService
{
	private function addPromo($data)
	{
	    try{
	      $curl_url = env('serverURL').'user/promocode/add';
	      $method = "POST";
	      $array = ['promocode'=>$data->promo_code];

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      curl_close($curl);
	      $response=json_decode($response,true);
	      
	      if ($httpcode==422) {
	      	return ['message'=>"The selected promocode is invalid.","data"=>[],"errors"=>[],'statusCode'=>422];
	      }
	      
	      return ['message'=>"Promocode added successfully.","data"=>[],"errors"=>[],'statusCode'=>201];
	    }
	    catch(\Illuminate\Database\QueryException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getPromo($data)
	{
	    try{
	      $curl_url = env('serverURL').'user/promocodes';
	      $method = "GET";
	      $json_encode = "";
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>"Promocode List","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	public function accessAddPromo($data)
	{
    	return $this->addPromo($data);
  	}

  	public function accessGetPromo($data)
	{
    	return $this->getPromo($data);
  	}
}