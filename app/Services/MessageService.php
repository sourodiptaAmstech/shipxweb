<?php

namespace App\Services;

use App\Services\CurlService;


class MessageService
{
	private function sendMessage($data)
	{
	    try{
	    	$curl_url = env('serverURL').'passenger/on/ride/message';
	      	$method = "POST";
	      	$array = ['request_id'=>$data->request_id,
	      			'user_scope'=>$data->user_scope,
	      			'message'=>$data->message
	  				];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getMessage($data)
	{
	    try{
	    	$curl_url = env('serverURL').'passenger/on/ride/get/message';
	      	$method = "POST";
	      	$array = ['request_id'=>$data->request_id];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function sendMessageDriver($data)
	{
	    try{
	    	$curl_url = env('serverURL').'driver/on/ride/message';
	      	$method = "POST";
	      	$array = ['request_id'=>$data->request_id,
	      			'user_scope'=>$data->user_scope,
	      			'message'=>$data->message
	  				];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getMessageDriver($data)
	{
	    try{
	    	$curl_url = env('serverURL').'driver/on/ride/get/message';
	      	$method = "POST";
	      	$array = ['request_id'=>$data->request_id];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function newsLetter($data)
	{
	    try{
	    	$curl_url = env('serverURL').'user/subscribe/newsletter';
	      	$method = "POST";
	      	$array = ['email'=>$data->email];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function contactUS($data)
	{
	    try{
	    	$curl_url = env('serverURL').'user/contact/us';
	      	$method = "POST";
	      	$array = ['first_name'=>$data->first_name,'last_name'=>$data->last_name,'email'=>$data->email,'message'=>$data->message];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      	curl_close($curl);
	      	$response=json_decode($response,true);
	      	
	      	
	      	return ['message'=>$response['message'],"data"=>[],"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	public function accessContactUS($data)
	{
    	return $this->contactUS($data);
  	}


	public function accessNewsLetter($data)
	{
    	return $this->newsLetter($data);
  	}

	public function accessSendMessage($data)
	{
    	return $this->sendMessage($data);
  	}

  	public function accessGetMessage($data)
	{
    	return $this->getMessage($data);
  	}

  	public function accessSendMessageDriver($data)
	{
    	return $this->sendMessageDriver($data);
  	}

  	public function accessGetMessageDriver($data)
	{
    	return $this->getMessageDriver($data);
  	}

}