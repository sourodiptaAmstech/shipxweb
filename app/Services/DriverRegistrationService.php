<?php

namespace App\Services;

use App\Services\CurlService;


class DriverRegistrationService 
{
	private function sendOTP($data)
	{
    try{
      $curl_url = env('serverURL').'user/mobile/number/verification';
      $method = "POST";
      $array = ['mobileNo'=>$data->mobile_no ];
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['otp'],"errors"=>[],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
	
	}

  private function register($data)
  {
    try{
      $curl_url = env('serverURL').'provider/register';
      $method = "POST";
      $array = [
                'login_by'=>$data->login_by,
                'device_id'=>$data->device_id,
                'device_token'=>$data->device_token,
                'device_type'=>$data->device_type,
                'first_name'=>$data->first_name,
                'last_name'=>$data->last_name,
                'mobile'=>$data->mobile_no,
                'email'=>$data->email,
                'password'=>$data->password,
                'password_confirmation'=>$data->password_confirmation,
                'vehicle_year'=>$data->model_year,
                'service_type'=>$data->service_type,
                'service_number'=>$data->service_number,
                'vehicle_make'=>$data->make,
                'service_model'=>$data->model,
                'address'=>$data->address
              ];
      
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);
      
      if ($httpcode==422) {
        return ['message'=>$response['message'],"data"=>[],"errors"=>$response['errors'],'statusCode'=>$httpcode];
      }
      
      return ['message'=>$response['message'],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }  
  }

  private function getMasterServices($data)
  {
    try{
      $curl_url = env('serverURL').'provider/service';
      $method = "GET";
      $json_encode = "";
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>[],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

  }

  private function getModels($data)
  {
    try{
      $curl_url = env('serverURL').'get/model/list';
      $method = "GET";
      $json_encode = "";
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }

  private function updateEmail($data)
  {
    try{
      $curl_url = env('serverURL').'driver/email';
      $method = "POST";
      $array = ['email_id'=>$data->email_id];
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

      curl_close($curl);
      $response=json_decode($response,true);
      
      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
      return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

  }

	public function accessSendOTP($data){
      return $this->sendOTP($data);
  }

  public function accessRegister($data){
      return $this->register($data);
  }

  public function accessGetMasterServices($data){
      return $this->getMasterServices($data);
  }

  public function accessGetModels($data){
      return $this->getModels($data);
  }

  public function accessUpdateEmail($data){
      return $this->updateEmail($data);
  }

}