<?php

namespace App\Services;

use App\Services\CurlService;


class PaymentService
{
	private function verifyTransaction($data)
	{
	    try{
	      $curl_url = env('serverURL').'payment/verify/transaction';
	      $method = "POST";
	      $array = ['reference'=>$data->reference];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function updateTranscationEmail($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/transaction/email';
	      $method = "POST";
	      $array = ['email_id'=>$data->email_id];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function checkVerifyEmail($data)
	{
	    try{
	      $curl_url = env('serverURL').'user/card';
	      $method = "GET";
	      $json_encode = "";
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      
	      curl_close($curl);
	      $response=json_decode($response,true);
	      

	      return ['message'=>"Card List.","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function addCard($data)
	{
	    try{
	      $curl_url = env('serverURL').'user/card';
	      $method = "POST";
	      $array = ['stripe_token'=>$data->stripeToken];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      // print_r($httpcode);
	      curl_close($curl);
	      $response=json_decode($response,true);
	      // dd($response);

	      return ['message'=>"Card Added.","data"=>[],"errors"=>[],'statusCode'=>200];

	      // return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function setDefaultCard($data)
	{
	    try{
	      $curl_url = env('serverURL').'passenger/default/card';
	      $method = "PUT";
	      $array = ['user_cards_id'=>$data->user_cards_id];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}

	private function deleteCard($data)
	{
	    try{
	      $curl_url = env('serverURL').'user/card/destory';
	      $method = "DELETE";
	      $array = ['card_id'=>$data->user_cards_id];

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;
	      
	      $curlService = new CurlService;
      	  $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      // print_r($httpcode);
	      curl_close($curl);
	      $response=json_decode($response,true);
	      // dd($response);

	      return ['message'=>"Card deleted.","data"=>[],"errors"=>[],'statusCode'=>200];

	      // return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}


	private function verifyTransactionEmail($data)
	{
	    try{
	      $curl_url = env('serverURL').'transaction/emailverify';
	      $method = "PUT";
	      $array = ['ute_id'=>$data->ute_id];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	     
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}


	public function accessVerifyTransaction($data)
	{
    	return $this->verifyTransaction($data);
  	}

  	public function accessUpdateTranscationEmail($data)
	{
    	return $this->updateTranscationEmail($data);
  	}

  	public function accessVerifyTransactionEmail($data)
	{
    	return $this->verifyTransactionEmail($data);
  	}

  	public function accessCheckVerifyEmail($data)
	{
    	return $this->checkVerifyEmail($data);
  	}

  	public function accessAddCard($data)
	{
    	return $this->addCard($data);
  	}

  	public function accessSetDefaultCard($data)
	{
    	return $this->setDefaultCard($data);
  	}

  	public function accessDeleteCard($data)
	{
    	return $this->deleteCard($data);
  	}

}