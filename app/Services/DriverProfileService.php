<?php

namespace App\Services;

use App\Services\CurlService;


class DriverProfileService
{

	private function changePassword($data)
	{
		try{
			$curl_url = env('serverURL').'provider/profile/password';
			$method = "POST";
			$array = ['password_old'=>$data->old_password, 'password'=>$data->password, 'password_confirmation'=>$data->password_confirmation];
      		$json_encode = json_encode($array);
			$timeZone = $data->timeZone;
			$token = $data->token;
			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);

			if ($httpcode==422) {
				return ['message'=>"Please enter correct password","data"=>[],"errors"=>array("exception"=>["Invalid Request"]),'statusCode'=>$httpcode];
			}
			return ['message'=>"Password changed successfully!","data"=>[],"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}

	private function profileImageUpdate($data)
	{
		try{
			$curl_url = env('serverURL').'driver/profile/image';
			$postfields = ['picture'=> new \CURLFILE($data->tmp_name,$data->type,$data->name)];
			
			$curlService = new CurlService;
			$curl = $curlService->accessAuthFileCurl($curl_url,$postfields,$data);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);
			
			if ($httpcode == 422) {
				return ['message'=>$response['message'],"field"=>$response['field'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
			}
			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}

	private function getProfile($data)
	{
		try{
			$curl_url = env('serverURL').'provider/profile';
			$method = "GET";
			$json_encode = "";
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);
			return ['message'=>'Driver Profile.',"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
	}

	private function updateProfile($data)
	{
		try{
			$curl_url = env('serverURL').'provider/profile';
			
			if($data->name!=''){
				$postfields = ['avatar'=> new \CURLFILE($data->tmp_name,$data->type,$data->name), 'first_name'=>$data->first_name, 'last_name'=>$data->last_name, 'mobile'=>$data->mobile, 'service_number'=>$data->service_number, 'service_type'=>$data->service_type,'service_model'=>$data->service_model, 'vehicle_year'=>$data->vehicle_year];
			} else {
				$postfields = ['first_name'=>$data->first_name, 'last_name'=>$data->last_name, 'mobile'=>$data->mobile, 'service_number'=>$data->service_number, 'service_type'=>$data->service_type,'service_model'=>$data->service_model, 'vehicle_year'=>$data->vehicle_year];
			}

			$curlService = new CurlService;
			$curl = $curlService->accessAuthFileCurl($curl_url,$postfields,$data);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);
			
			return ['message'=>'Profile updated successfully.',"data"=>[],"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}


	private function driverOnOff($data)
	{
		try{
			$curl_url = env('serverURL').'driver/on/off/update';
			$method = "PUT";
			$array = ['status'=>$data->status];
      		$json_encode = json_encode($array);
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);
			
			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}


	private function updateSocialProfile($data)
	{
		try{
			$curl_url = env('serverURL').'provider/profile/updatesocialno';
			$method = "POST";
			$array = ['ss_number'=>$data->ss_number, 'vehicale_color'=>$data->vehicale_color, 'vehicale_number_plate_no'=>$data->vehicale_number_plate_no, 'number_plate_no_exprydate'=>$data->number_plate_no_exprydate];

      		$json_encode = json_encode($array);
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);
			
			if ($httpcode==422) {
				return ['message'=>$response['message'],"data"=>[],"errors"=>$response['errors'],'statusCode'=>$httpcode];
			}
			
			return ['message'=>"Profile Data.","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
	}


	private function providerHelp($data)
	{
		try{
			$curl_url = env('serverURL').'provider/help';
			$method = "GET";
			$json_encode = "";
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);

			return ['message'=>"Help.","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
	}


	public function accessProviderHelp($data)
	{
    	return $this->providerHelp($data);
  	}


	public function accessUpdateSocialProfile($data)
	{
    	return $this->updateSocialProfile($data);
  	}

	public function accessChangePassword($data)
	{
    	return $this->changePassword($data);
  	}

  	public function accessProfileImageUpdate($data)
	{
    	return $this->profileImageUpdate($data);
  	}

  	public function accessGetProfile($data)
	{
    	return $this->getProfile($data);
  	}

  	public function accessUpdateProfile($data)
	{
    	return $this->updateProfile($data);
  	}

  	public function accessDriverOnOff($data)
	{
    	return $this->driverOnOff($data);
  	}

}