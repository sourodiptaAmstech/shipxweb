<?php

namespace App\Services;

use App\Services\CurlService;


class DriverAuthService
{
	private function login($data)
	{
    try{
      $curl_url = env('serverURL').'provider/oauth/token';
      $method = "POST";
      $array = ['device_id'=>$data->device_id,'device_token'=>$data->device_token,
                'device_type'=>$data->device_type,'email'=>$data->username,'password'=>$data->password ];

      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

      curl_close($curl);
      $response=json_decode($response,true);

      if($httpcode==200){
        $IP = $_SERVER['REMOTE_ADDR'];
        $mac = shell_exec("ip link | awk '{print $2}'");
        preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
        $output = array_combine($matches[1], $matches[2]);
        $mac_address_values=$IP.":".env('APP_KEY');
        foreach($output as $key=>$val){
          if($val!=""){
            if($mac_address_values!=="")
            {  $mac_address_values=":".$mac_address_values.$val;}
            else
            {  $mac_address_values=$mac_address_values.$val;}
          }
        }
        $entroSignature= base64_encode(($mac_address_values));

        $cookie_name1 = "shipxSignature";
        $cookie_value1 = base64_encode(($entroSignature));
        setcookie($cookie_name1, $cookie_value1, 0, "/");

        $cookie_name = "shipxSecurity";
        $cookie_value = base64_encode(encrypt('Bearer'.' '.$response["access_token"]));
        setcookie($cookie_name, $cookie_value, 0, "/");

        return ['message'=>"Logged in.","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
      }

      return ['message'=>"The email address or password you entered is incorrect.","data"=>$json_encode,"errors"=>$curl_url,'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

	}

  private function socialLogin($data)
  {
    try{
      if ($data->login_by=='facebook') {
        $curl_url = env('serverURL').'provider/auth/facebook';
      }
      if ($data->login_by=='google') {
        $curl_url = env('serverURL').'provider/auth/google';
      }

      $method = "POST";
      $array = ['social_unique_id'=>$data->social_unique_id,'device_id'=>$data->device_id,'device_token'=>$data->device_token,'device_type'=>$data->device_type,'login_by'=>$data->login_by, 'accessToken'=>$data->accessToken, 'name'=>$data->name, 'email'=>$data->email,'avatar'=>$data->avatar, 'mobile'=>$data->mobile];

      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      if($httpcode==200){
        $IP = $_SERVER['REMOTE_ADDR'];
        $mac = shell_exec("ip link | awk '{print $2}'");
        preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
        $output = array_combine($matches[1], $matches[2]);
        $mac_address_values=$IP.":".env('APP_KEY');
        foreach($output as $key=>$val){
          if($val!=""){
            if($mac_address_values!=="")
            {  $mac_address_values=":".$mac_address_values.$val;}
            else
            {  $mac_address_values=$mac_address_values.$val;}
          }
        }
        $entroSignature= base64_encode(($mac_address_values));

        $cookie_name1 = "shipxSignature";
        $cookie_value1 = base64_encode(($entroSignature));
        setcookie($cookie_name1, $cookie_value1, 0, "/");

        $cookie_name = "shipxSecurity";
        $cookie_value = base64_encode(encrypt($response["token_type"]." ".$response["access_token"]));
        setcookie($cookie_name, $cookie_value, 0, "/");
      }
      return ['message'=>"Driver Social Login.","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }


	private function forgetPassword($data)
	{
    try{
      $curl_url = env('serverURL').'provider/forgot/password';
      $method = "POST";
      $array = ['email'=>$data->email];
      //dd($array);
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      print_r($httpcode);
      curl_close($curl);
      $response=json_decode($response,true);
      dd($response);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

	}


  private function resetPassword($data)
  {
    try{
      $curl_url = env('serverURL').'provider/reset/password';
      $method = "POST";
      $array = ['id'=>$data->id, 'email'=>$data->email, 'password'=>$data->password, 'password_confirmation'=>$data->password_confirmation];
      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;

      $curlService = new CurlService;
      $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>'Password reset successfully.',"data"=>[],"errors"=>[],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
  }


  private function logout($data)
  {
    try{
      $curl_url = env('serverURL').'provider/logout';
      $method = "POST";
      $json_encode = '';
      $timeZone = $data->timeZone;
      $token = $data->token;
      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>[],"errors"=>[],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

  }


  private function getTermCondition($data)
  {
      try{
        $curl_url = env('serverURL').'provider/terms_conditions';
        $method = "POST";
        $json_encode = "";
        $timeZone = $data->timeZone;

        $curlService = new CurlService;
        $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);

        return ['message'=>'Terms & Condition.',"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }

  }


  public function accessGetTermCondition($data){
      return $this->getTermCondition($data);
  }


  public function accessLogin($data){
      return $this->login($data);
	}

	public function accessForgetPassword($data){
      return $this->forgetPassword($data);
	}

  public function accessResetPassword($data){
      return $this->resetPassword($data);
  }

  public function accessLogout($data){
      return $this->logout($data);
  }

  public function accessSocialLogin($data){
      return $this->socialLogin($data);
  }

}
