<?php

namespace App\Services;

use App\Services\CurlService;


class PassengerRequestService
{
	private function estimatedFare($data)
	{
	    try{
           $curl_url = env('serverURL').'user/services'; //exit;
          //$curl_url = "https://demos.mydevfactory.com/debarati/shipx/public/api/".'user/services'; //exit;
	      $method = "POST";

	      $array = $data->bookingRequest;

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
          $token = $data->token;
          $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      curl_close($curl);
	      $response=json_decode($response,true);

	      if ($httpcode==401) {
	      	return ['message'=>$response['message'],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
          }
          else if($httpcode==500){
            return ['message'=>$response['message'],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
          }

	      return ['message'=>'',"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }

	}
    public function GetDefaultCard($data)
	{
	    try{
            $curl_url = env('serverURL').'user/card';// exit;
          //$curl_url = "https://demos.mydevfactory.com/debarati/shipx/public/api/".'user/card'; //exit;
          $token = $data->token;
          $curlService = new CurlService;
	      $curl = $curlService->authCurlGET($curl_url,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      curl_close($curl);
	      $response=json_decode($response,true);

	      if ($httpcode==401) {
	      	return ['message'=>$response['message'],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
          }
          else if($httpcode==500){
            return ['message'=>$response['message'],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
          }

	      return ['message'=>'',"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }

	}
	private function request($data)
	{
	    try{
            if($data->ty=="rqst")
           $curl_url = env('serverURL').'user/send/request';
           if($data->ty=="est")
           $curl_url = env('serverURL').'user/estimated/fare';
       // echo   $curl_url = 'https://demos.mydevfactory.com/debarati/shipx/public/api/user/send/request';
          $method = "POST";
          //echo $curl_url; exit;

          $array = $data->bookingRequest;
          //print_r($array);exit;

	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
          $token = $data->token;// exit;
          $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      curl_close($curl);
	      $response=json_decode($response,true);

	      if ($httpcode==401) {
	      	return ['message'=>'',"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
          }
          else if($httpcode==500){
            return ['message'=>$response['message'],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
          }

	      return ['message'=>'',"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }

	}

	private function ratingComment($data)
	{
	    try{
	      $curl_url = env('serverURL').'user/rate/provider';
	      $method = "POST";
	      $array = [
	      	'request_id'=>$data->request_id,
	      	'rating'=>$data->rating,
	      	'comment'=>$data->comment
	      ];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      curl_close($curl);
          $response=json_decode($response,true);
          print_r($response); exit;

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }

	}


	private function scheduleRide($data)
	{
	    try{
	      	$curl_url = env('serverURL').'passenger/request/schedule/ride';
	      	$method = "POST";
	      	$array = [
	      		'request_id'=>$data->request_id,
	      		'service_type_id'=>$data->service_type_id,
	      		'payment_method'=>$data->payment_method,
	      		'date'=>$data->date,
	      		'time'=>$data->time,
	      		'isFemaleFriendly'=>$data->isFemaleFriendly,
	      		'card_id'=>$data->card_id
	      	];
	      	$json_encode = json_encode($array);

	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }

	}


	private function tripHistory($data)
	{
	    try{
	      	$curl_url = env('serverURL').'user/trips';
	      	$method = "GET";
	      	$json_encode = "";

	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}

	private function tripDetails($data)
  	{
      	try{
          	$curl_url = env('serverURL').'user/trip/details';
          	$method = "GET";
          	$array = ['request_id'=>$data->request_id];
          	$json_encode = json_encode($array);

          	$timeZone = $data->timeZone;
          	$token = $data->token;

          	$curlService = new CurlService;
          	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          	$response = curl_exec($curl);
          	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

          	curl_close($curl);
          	$response=json_decode($response,true);

          	return ['message'=>"User trip details","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
      	}
      	catch(\Illuminate\Database\QueryException  $e){
          	return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      	}
      	catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          	return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      	}
  	}


	private function upCommingScheduleTrip($data)
	{
	    try{
	      	$curl_url = env('serverURL').'user/upcoming/trips';
	      	$method = "GET";
	      	$json_encode = "";

	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}

	private function scheduleTripDetails($data)
  	{
      	try{
          	$curl_url = env('serverURL').'user/upcoming/trip/details';
          	$method = "GET";
          	$array = ['request_id'=>$data->request_id];
          	$json_encode = json_encode($array);

          	$timeZone = $data->timeZone;
          	$token = $data->token;

          	$curlService = new CurlService;
          	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          	$response = curl_exec($curl);
          	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

          	curl_close($curl);
          	$response=json_decode($response,true);

          	return ['message'=>"User schedule trip details","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
      	}
      	catch(\Illuminate\Database\QueryException  $e){
          	return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      	}
      	catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          	return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      	}
  	}

	private function cancelScheduleTrip($data)
	{
	    try{
	      	$curl_url = env('serverURL').'passenger/request/schedule/cancel';
	      	$method = "POST";
          	$array = ['request_id'=>$data->request_id];
          	$json_encode = json_encode($array);

	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}
    private function cancelRideTrip($data)
	{
	    try{
	      	$curl_url = env('serverURL').'user/cancel/request';
	      	$method = "POST";
          	$array = ['request_id'=>$data->request_id,'cancel_reason'=>$data->comment];
          	$json_encode = json_encode($array);

	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      	curl_close($curl);
              $response=json_decode($response,true);

	      	return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}

    private function pay($data)
	{
	    try{
	      	$curl_url = env('serverURL').'user/payment';
	      	$method = "POST";
          	$array = ['request_id'=>$data->request_id,'tips'=>$data->tips];
          	$json_encode = json_encode($array);

	      	$timeZone = $data->timeZone;
	      	$token = $data->token;

	      	$curlService = new CurlService;
	      	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	      	curl_close($curl);
              $response=json_decode($response,true);

	      	return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	}
	public function accessEstimatedFare($data)
	{
    	return $this->estimatedFare($data);
  	}

  	public function accessRequest($data)
	{
    	return $this->request($data);
  	}

  	public function accessRatingComment($data)
	{
    	return $this->ratingComment($data);
  	}

  	public function accessScheduleRide($data)
	{
    	return $this->scheduleRide($data);
  	}

  	public function accessTripHistory($data)
	{
    	return $this->tripHistory($data);
  	}

  	public function accessTripDetails($data)
  	{
    	return $this->tripDetails($data);
  	}

  	public function accessUpCommingScheduleTrip($data)
	{
    	return $this->upCommingScheduleTrip($data);
  	}

  	public function accessScheduleTripDetails($data)
	{
    	return $this->scheduleTripDetails($data);
  	}

  	public function accessCancelScheduleTrip($data)
	{
    	return $this->cancelScheduleTrip($data);
      }
      public function accessCancelRideTrip($data)
      {
          return $this->cancelRideTrip($data);
        }
        public function accessPay($data)
        {
            return $this->pay($data);
          }


}
