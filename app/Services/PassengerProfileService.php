<?php

namespace App\Services;

use App\Services\CurlService;


class PassengerProfileService
{

	private function changePassword($data)
	{
		try{ 
			$curl_url = env('serverURL').'user/change/password';
			$method = "POST";
			$array = ['old_password'=>$data->old_password, 'password'=>$data->password, 'password_confirmation'=>$data->password_confirmation];
      		$json_encode = json_encode($array);
			$timeZone = $data->timeZone;
			$token = $data->token;
			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);

			if ($httpcode==500) {
				return ['message'=>"Please enter correct password","data"=>[],"errors"=>array("exception"=>["Invalid Request"]),'statusCode'=>422];
			}
			return ['message'=>"Password changed successfully!","data"=>[],"errors"=>[],'statusCode'=>200];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}

	private function profileImageUpdate($data)
	{
		try{
			$curl_url = env('serverURL').'passenger/profile/image';
			$postfields = ['picture'=> new \CURLFILE($data->tmp_name,$data->type,$data->name)];

			$curlService = new CurlService;
			$curl = $curlService->accessAuthFileCurl($curl_url,$postfields,$data);
			
			$response = curl_exec($curl); 
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);

			if ($httpcode == 422) {
				return ['message'=>$response['message'],"field"=>$response['field'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
			}
			return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}
	

	private function getProfile($data)
	{
		try{
			$curl_url = env('serverURL').'user/details';
			$method = "GET";
			$json_encode = "";
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);

			return ['message'=>"Passenger Details.","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
	}


	private function updateProfile($data)
	{
		try{
			$curl_url = env('serverURL').'user/update/profile';
			if($data->name!=''){
				$postfields = ['picture'=> new \CURLFILE($data->tmp_name,$data->type,$data->name), 'first_name'=>$data->first_name, 'last_name'=>$data->last_name, 'businessName'=>$data->business_name, 'mobile'=>$data->mobile, 'email'=>$data->email];
			} else {
				$postfields = ['first_name'=>$data->first_name, 'last_name'=>$data->last_name, 'businessName'=>$data->business_name, 'mobile'=>$data->mobile, 'email'=>$data->email];
			}

			$curlService = new CurlService;
			$curl = $curlService->accessAuthFileCurl($curl_url,$postfields,$data);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);
			
			return ['message'=>'Profile updated successfully.',"data"=>[],"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}


	private function userHelp($data)
	{
		try{
			$curl_url = env('serverURL').'user/help';
			$method = "GET";
			$json_encode = "";
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);

			return ['message'=>"Help","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
	}


	private function reportIssue($data)
	{
		try{
			$curl_url = env('serverURL').'user/send/feedback';
			$method = "POST";
			$array = ['app'=>$data->app, 'subject'=>$data->subject, 'description'=>$data->description];
			
      		$json_encode = json_encode($array);
			$timeZone = $data->timeZone;
			$token = $data->token;
			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);
			return ['message'=>'Reported successfully.',"data"=>[],"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}

	public function accessReportIssue($data)
	{
    	return $this->reportIssue($data);
  	}

	public function accessUserHelp($data)
	{
    	return $this->userHelp($data);
  	}

	public function accessChangePassword($data)
	{
    	return $this->changePassword($data);
  	}

  	public function accessProfileImageUpdate($data)
	{
    	return $this->profileImageUpdate($data);
  	}

  	public function accessGetProfile($data)
	{
    	return $this->getProfile($data);
  	}

  	public function accessUpdateProfile($data)
	{
    	return $this->updateProfile($data);
  	}

}
