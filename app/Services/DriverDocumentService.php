<?php

namespace App\Services;

use App\Services\CurlService;


class DriverDocumentService
{
	private function documentList($data)
	{
		try{
			$curl_url = env('serverURL').'provider/documents/list';
			$method = "POST";
			$json_encode = "";
			$timeZone = $data->timeZone;
			$token = $data->token;

			$curlService = new CurlService;
			$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			
			curl_close($curl);
			$response=json_decode($response,true);
			
			return ['message'=>"Documents List.","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
	}


	private function uploadDocument($data)
	{
		try{
			$curl_url = env('serverURL').'provider/documents/'.$data->document_id;

			$postfields = ['document_front'=> new \CURLFILE($data->tmp_name,$data->type,$data->name),'app'=>$data->app];
			$curlService = new CurlService;
			$curl = $curlService->accessAuthFileCurl($curl_url,$postfields,$data);

			$response = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$response=json_decode($response,true);

			if ($httpcode == 422) {
				return ['message'=>"Invalid file.","field"=>[],"errors"=>['file'=>'The file type must be a valid file.'],'statusCode'=>$httpcode];
			}

			return ['message'=>"Document uploaded sucessfully.","data"=>[],"errors"=>[],'statusCode'=>$httpcode];
		}
		catch(\Illuminate\Database\QueryException  $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}
		catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
			return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
		}

	}


	public function accessDocumentList($data)
	{
    	return $this->documentList($data);
  	}

  	public function accessUploadDocument($data)
	{
    	return $this->uploadDocument($data);
  	}

}