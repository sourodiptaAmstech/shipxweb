<?php

namespace App\Services;

use App\Services\CurlService;


class AdminChatSupportService
{

	private function sendPassengerMessage($data)
	{
	    try{
	    	$curl_url = env('serverURL').'passenger/admin/chat/support';
	      	$method = "POST";
	      	$array = [
	      			'user_scope'=>$data->user_scope,
	      			'message'=>$data->message
	  				];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getPassengerMessage($data)
	{
	    try{
	    	$curl_url = env('serverURL').'passenger/admin/chat/support/get/message';
	      	$method = "POST";
	      	$array = [
	      			'user_scope'=>$data->user_scope,
	  				];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function sendDriverMessage($data)
	{
	    try{
	    	$curl_url = env('serverURL').'driver/admin/chat/support';
	      	$method = "POST";
	      	$array = [
	      			'user_scope'=>$data->user_scope,
	      			'message'=>$data->message
	  				];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function getDriverMessage($data)
	{
	    try{
	    	$curl_url = env('serverURL').'driver/admin/chat/support/get/message';
	      	$method = "POST";
	      	$array = [
	      			'user_scope'=>$data->user_scope,
	  				];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
      	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	public function accessSendPassengerMessage($data)
	{
    	return $this->sendPassengerMessage($data);
  	}

  	public function accessSendDriverMessage($data)
	{
    	return $this->sendDriverMessage($data);
  	}

  	public function accessGetPassengerMessage($data)
	{
    	return $this->getPassengerMessage($data);
  	}

  	public function accessGetDriverMessage($data)
	{
    	return $this->getDriverMessage($data);
  	}

}