<?php

namespace App\Services;

use App\Services\CurlService;


class DriverWalletService
{
    private function paymentWallet($data)
	{
	    try{
	      	$curl_url = env('serverURL').'provider/profile/getproviderearnings';
	      	$method = "POST";
	      	$array = ['app'=>$data->app,
	      			'range'=>$data->range,
	  				];
	      	$json_encode = json_encode($array);
	      	$timeZone = $data->timeZone;
	      	$token = $data->token;
	      
	      	$curlService = new CurlService;
	  	  	$curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

	      	$response = curl_exec($curl);
	      	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	 
	      	curl_close($curl);
	      	$response=json_decode($response,true);

	      	return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
    }
    
    public function accessPaymentWallet($data)
	{
    	return $this->paymentWallet($data);
  	}

}