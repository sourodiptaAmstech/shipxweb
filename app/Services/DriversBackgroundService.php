<?php

namespace App\Services;

use App\Services\CurlService;


class DriversBackgroundService
{
	private function backgroundData($data)
	{
	    try{
	      $curl_url = env('serverURL').'provider/trip?latitude='.$data->device_latitude.'&longitude='.$data->device_longitude;
	      $method = "GET";
	      $array = ['latitude'=>$data->device_latitude,'longitude'=>$data->device_longitude ];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	      $token = $data->token;

	      $curlService = new CurlService;
	      $curl = $curlService->authCurlGET($curl_url,$token);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

          if($httpcode==200){
            return ['message'=>"ok","data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
         }
         return ['message'=>'',"data"=>$response,"errors"=>["curl_url"=>$curl_url],'statusCode'=>$httpcode];
       }
       catch(\Illuminate\Database\QueryException  $e){
           return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
       }
       catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
           return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
       }

	}

	public function accessBackgroundData($data)
	{
    	return $this->backgroundData($data);
  	}

}
