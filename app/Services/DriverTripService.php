<?php

namespace App\Services;

use App\Services\CurlService;


class DriverTripService
{
  private function declineRequest($data)
  {
      try{
        $curl_url = env('serverURL').'provider/trip/'.$data->request_id;
        $method = "DELETE";
        $array = ['request_id'=>$data->request_id];
        $json_encode = json_encode($array);
        $timeZone = $data->timeZone;
        $token = $data->token;

        $curlService = new CurlService;
        $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);

        return ['message'=>$response,"data"=>$response,"errors"=>$curl_url,'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }

  }

  private function cancelRequest($data)
  {
      try{
        $curl_url = env('serverURL').'provider/cancel';
        $method = "POST";
        $array = ['id'=>$data->request_id,'cancel_reason'=>$data->cancel_reason];
        $json_encode = json_encode($array);
        $timeZone = $data->timeZone;
        $token = $data->token;

        $curlService = new CurlService;
        $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);

        return ['message'=>$response,"data"=>$response,"errors"=>$curl_url,'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }

  }



  private function tripControl($data)
  {
      try{
          if($data->tripStatus=="accpect"){
               $curl_url = env('serverURL').'provider/trip/'.$data->request_id;
               $method = "POST";
               $array = ['request_id'=>$data->request_id,'tripStatus'=>$data->tripStatus];
          }
          if($data->tripStatus=="arrived"){
            $curl_url = env('serverURL').'provider/trip/'.$data->request_id;
            $method = "PATCH";
            $array = ['request_id'=>$data->request_id,'status'=>'ARRIVED'];
       }
       if($data->tripStatus=="PICKEDUP"){
        $curl_url = env('serverURL').'provider/trip/'.$data->request_id;
        $method = "PATCH";
        $array = ['request_id'=>$data->request_id,'status'=>'PICKEDUP'];
   }
   if($data->tripStatus=="DROP"){
    $curl_url = env('serverURL').'provider/trip/'.$data->request_id;
    $method = "PATCH";
    $array = ['request_id'=>$data->request_id,'status'=>'DROPPED','latitude'=>$data->latitude,'longitude'=>$data->longitude,'distance'=>$data->distance,'address'=>$data->address];
}


        $json_encode = json_encode($array);
        $timeZone = $data->timeZone;
        $token = $data->token;

        $curlService = new CurlService;
        $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);

        return ['message'=>$response,"data"=>$response,"errors"=>"",'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }

  }


  private function tripPayment($data)
  {
    try{
      $curl_url = env('serverURL').'driver/trip/control';
      $method = "POST";
      $array = ['request_id'=>$data->request_id,'tripStatus'=>$data->tripStatus,'payment_method'=>$data->payment_method];

      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $response=json_decode($response,true);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

  }


  private function ratingComment($data)
  {
    try{
      $curl_url = env('serverURL').'provider/trip/'.$data->request_id.'/rate';
      $method = "POST";
      $array = ['request_id'=>$data->request_id,'rating'=>$data->rating,'comment'=>$data->comment];

      $json_encode = json_encode($array);
      $timeZone = $data->timeZone;
      $token = $data->token;

      $curlService = new CurlService;
      $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      //print_r($httpcode);
      curl_close($curl);
      $response=json_decode($response,true);
     // dd($response);

      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
    }
    catch(\Illuminate\Database\QueryException  $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }
    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
    }

  }


  private function tripHistory($data)
  {
      try{
          $curl_url = env('serverURL').'provider/requests/history/web';
          $method = "GET";
          $json_encode = "";

          $timeZone = $data->timeZone;
          $token = $data->token;

          $curlService = new CurlService;
          $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          $response = curl_exec($curl);
          $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

          curl_close($curl);
          $response=json_decode($response,true);

          return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
  }


  private function tripDetails($data)
  {
      try{
          $curl_url = env('serverURL').'provider/requests/history/details';
          $method = "GET";
          $array = ['request_id'=>$data->request_id];
          $json_encode = json_encode($array);

          $timeZone = $data->timeZone;
          $token = $data->token;

          $curlService = new CurlService;
          $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          $response = curl_exec($curl);
          $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

          curl_close($curl);
          $response=json_decode($response,true);

          return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
  }

  private function scheduleTrips($data)
  {
      try{
          $curl_url = env('serverURL').'provider/requests/upcoming';
          $method = "GET";
          $json_encode = "";

          $timeZone = $data->timeZone;
          $token = $data->token;

          $curlService = new CurlService;
          $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          $response = curl_exec($curl);
          $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

          curl_close($curl);
          $response=json_decode($response,true);

          return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
  }

  private function scheduleTripsDetails($data)
  {
      try{
          $curl_url = env('serverURL').'provider/requests/upcoming/details';
          $method = "GET";
          $array = ['request_id'=>$data->request_id];
          $json_encode = json_encode($array);

          $timeZone = $data->timeZone;
          $token = $data->token;

          $curlService = new CurlService;
          $curl = $curlService->accessAuthCurl($curl_url,$method,$json_encode,$timeZone,$token);

          $response = curl_exec($curl);
          $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
          print_r($httpcode);

          curl_close($curl);
          $response=json_decode($response,true);
          dd($response);

          return ['message'=>[],"data"=>$response,"errors"=>[],'statusCode'=>$httpcode];
      }
      catch(\Illuminate\Database\QueryException  $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
      catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
          return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
      }
  }


  public function accessScheduleTrips($data)
  {
    return $this->scheduleTrips($data);
  }

  public function accessScheduleTripsDetails($data)
  {
    return $this->scheduleTripsDetails($data);
  }

  public function accessTripHistory($data)
  {
    return $this->tripHistory($data);
  }

  public function accessTripDetails($data)
  {
    return $this->tripDetails($data);
  }

  public function accessDeclineRequest($data)
  {
      return $this->declineRequest($data);
  }

  public function accessCancelRequest($data)
  {
      return $this->cancelRequest($data);
  }
  public function accessTripControl($data)
  {
      return $this->tripControl($data);
  }

  public function accessTripPayment($data)
  {
      return $this->tripPayment($data);
  }

  public function accessRatingComment($data)
  {
      return $this->ratingComment($data);
  }

}
