<?php

namespace App\Services;

use App\Services\CurlService;


class VehicleService
{

	private function getServiceMake($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/service/make';
	      $method = "GET";
	      $json_encode = "";
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}


	private function getServiceModel($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/service/model';
	      $method = "POST";
	      $array = ['make'=>$data->make];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	private function getServiceYear($data)
	{
	    try{
	      $curl_url = env('serverURL').'get/service/year';
	      $method = "POST";
	      $array = ['make'=>$data->make,'model'=>$data->model];
	      $json_encode = json_encode($array);
	      $timeZone = $data->timeZone;
	  
	      $curlService = new CurlService;
      	  $curl = $curlService->accessCurl($curl_url,$method,$json_encode,$timeZone);

	      $response = curl_exec($curl);
	      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	      curl_close($curl);
	      $response=json_decode($response,true);

	      return ['message'=>$response['message'],"data"=>$response['data'],"errors"=>$response['errors'],'statusCode'=>$httpcode];
	    }
	    catch(\Illuminate\Database\QueryException  $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
	    }
	
	}

	public function accessGetServiceMake($data)
	{
    	return $this->getServiceMake($data);
  	}

  	public function accessGetServiceModel($data)
	{
    	return $this->getServiceModel($data);
  	}

  	public function accessGetServiceYear($data)
	{
    	return $this->getServiceYear($data);
  	}
  	
}