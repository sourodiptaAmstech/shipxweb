<?php

namespace App\Http\Controllers\Website\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DriversBackgroundService;


class BackgroundController extends Controller
{
    public function driversBackgroundData(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'device_latitude'=>'required',
                'device_longitude'=>'required'
	        ]);

	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $request->device_latitude = $request->device_latitude;
	        $request->device_longitude = $request->device_longitude;

        	$driverBackground = new DriversBackgroundService;
			$result = $driverBackground->accessBackgroundData($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}
}
