<?php

namespace App\Http\Controllers\Website\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DriverProfileService;


class ProfileController extends Controller
{
	public function homeIndex()
	{
		return view('pages.driver.home');
	}

	public function accountIndex()
	{
		return view('pages.driver.myaccount');
	}

	public function mapLocation()
	{
		return view('pages.driver.map');
	}

	public function profileImageUpdate(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'picture' => 'required|mimes:jpeg,bmp,png',
	        ]);
	
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $tmp_name = $_FILES['picture']['tmp_name'];
	        $type = $_FILES['picture']['type'];
	        $name = basename($_FILES['picture']['name']);
	        
	        $request['tmp_name'] = $tmp_name;
	        $request['type'] = $type;
	        $request['name'] = $name;
	        $request['size']=$_FILES['picture']['size'];

        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessProfileImageUpdate($request);
	    
	      	if ($result['statusCode']==422) {
				return response(['message'=>$result['message'],"field"=>$result['field'],"errors"=>$result['errors']],$result['statusCode']);
			}
	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function getProfile(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessGetProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function updateDriverProfile(Request $request)
	{
		try{
			$this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'profile_image' => 'mimes:jpeg,bmp,png',
                'mobile_no'=>'required',
                'service_type'=>'required',
                'vehicle_make'=>'required',
                'vehicle_model'=>'required',
                'vehicle_year'=>'required',
                //'service_number'=>'required'
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
      		$request->first_name = $request->first_name;
      		$request->last_name = $request->last_name;
      		$request->mobile = $request->mobile_no;
      		//$request->email_id = $request->email_id;
      		$request->service_type = $request->service_type;
			$request->service_number = $request->vehicle_make;
			$request->service_model = $request->vehicle_model;
      		$request->vehicle_year = $request->vehicle_year;

      		$tmp_name = $_FILES['profile_image']['tmp_name'];
	        $type = $_FILES['profile_image']['type'];
	        $name = basename($_FILES['profile_image']['name']);
	        
	        $request['tmp_name'] = $tmp_name;
	        $request['type'] = $type;
	        $request['name'] = $name;
	        $request['size']=$_FILES['profile_image']['size'];

        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessUpdateProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function driverOnOffStatus(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			$request->status = $request->status;

        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessDriverOnOff($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function updateDriverSocialProfile(Request $request)
	{
		try{
			$this->validate($request, [
                'social_security_number' => 'required',
                'vehicle_color' => 'required',
                'vehicle_plate_number' => 'required',
                'vehicle_expiration_date'=>'required',
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
      		$request->ss_number = $request->social_security_number;
      		$request->vehicale_color = $request->vehicle_color;
      		$request->vehicale_number_plate_no = $request->vehicle_plate_number;
      		$request->number_plate_no_exprydate = $request->vehicle_expiration_date;
			
        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessUpdateSocialProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function helpIndex(Request $request)
	{
		return view('pages.driver.help');
	}

	public function driverHelp(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
        	$driverProfile = new DriverProfileService;
			$result = $driverProfile->accessProviderHelp($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}
	
}
