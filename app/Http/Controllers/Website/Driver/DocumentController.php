<?php

namespace App\Http\Controllers\Website\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DriverDocumentService;


class DocumentController extends Controller
{
	public function document(Request $request)
	{
		return view('pages.driver.document');
	}

    public function documentList(Request $request)
	{
		try{
			$request['token'] = $request->access_token;
			$request['timeZone'] = $request->timeZone;

			$driverDocument = new DriverDocumentService;
			$result = $driverDocument->accessDocumentList($request);
			
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function uploadDocument(Request $request)
	{
		try{
			$this->validate($request, [
	        	'file' => 'required|mimes:jpeg,bmp,png',
	        ]);
			
			$request['document_id'] = $request->document_id;
			$request['token'] = $request->access_token;
			$request['timeZone'] = $request->timeZone_f;
			$request['app'] = "web";

			$tmp_name = $_FILES['file']['tmp_name'];
	        $type = $_FILES['file']['type'];
	        $name = basename($_FILES['file']['name']);
	        
	        $request['tmp_name'] = $tmp_name;
	        $request['type'] = $type;
	        $request['name'] = $name;
	        $request['size']=$_FILES['file']['size'];

			$driverDocument = new DriverDocumentService;
			$result = $driverDocument->accessUploadDocument($request);

			if ($result['statusCode']==422) {
				return response(['message'=>$result['message'],"field"=>$result['field'],"errors"=>$result['errors']],$result['statusCode']);
			}

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
