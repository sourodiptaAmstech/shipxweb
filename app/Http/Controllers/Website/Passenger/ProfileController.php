<?php

namespace App\Http\Controllers\Website\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PassengerProfileService;


class ProfileController extends Controller
{
	public function homeIndex()
	{
		return view('pages.passenger.home');
	}

	public function accountIndex(Request $request)
	{
		return view('pages.passenger.myaccount');
	}

	public function profileImageUpdate(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'picture' => 'required|mimes:jpeg,bmp,png',
	        ]);
	        
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $tmp_name = $_FILES['picture']['tmp_name'];
	        $type = $_FILES['picture']['type'];
	        $name = basename($_FILES['picture']['name']);
	        
	        $request['tmp_name'] = $tmp_name;
	        $request['type'] = $type;
			$request['name'] = $name;
			$request['size']=$_FILES['picture']['size'];
			
			$passengerProfile = new PassengerProfileService;
			$result = $passengerProfile->accessProfileImageUpdate($request);
		
			if ($result['statusCode']==422) {
				return response(['message'=>$result['message'],"field"=>$result['field'],"errors"=>$result['errors']],$result['statusCode']);
			}
	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function getProfile(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
			$passengerProfile = new PassengerProfileService;
			$result = $passengerProfile->accessGetProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function updatePassengerProfile(Request $request)
	{
		try{
			$this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'profile_image' => 'mimes:jpeg,bmp,png',
                'mobile_no'=>'required',
                'email'=>'required|email',
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

			$request->first_name = $request->first_name;
			$request->last_name = $request->last_name;
			$request->business_name = $request->business_name;
			$request->email = $request->email;
			$request->mobile = $request->mobile_no;

			$tmp_name = $_FILES['profile_image']['tmp_name'];
	        $type = $_FILES['profile_image']['type'];
	        $name = basename($_FILES['profile_image']['name']);
	        
	        $request['tmp_name'] = $tmp_name;
	        $request['type'] = $type;
	        $request['name'] = $name;
	        $request['size']=$_FILES['profile_image']['size'];
			
        	$driverProfile = new PassengerProfileService;
			$result = $driverProfile->accessUpdateProfile($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function helpIndex(Request $request)
	{
		return view('pages.passenger.help');
	}


	public function passengerHelp(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;
			
        	$passengerProfile = new PassengerProfileService;
			$result = $passengerProfile->accessUserHelp($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function reportIssueIndex(Request $request)
	{
		return view('pages.passenger.report-issue');
	}


	public function reportIssue(Request $request)
	{
		try{
			$this->validate($request, [
                'subject' => 'required',
                'report' => 'required'
            ]);

			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

			$request->subject = $request->subject;
			$request->description = $request->report;
			$request->app = 1;
			
        	$passengerProfile = new PassengerProfileService;
			$result = $passengerProfile->accessReportIssue($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
