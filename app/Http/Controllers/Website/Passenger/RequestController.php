<?php

namespace App\Http\Controllers\Website\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PassengerRequestService;


class RequestController extends Controller
{
    public function requestBooking()
	{

		return view('pages.passenger.bookingRequest');
    }

    public function estimated(Request $request)
	{
		try{
            $request['token'] = $request->access_token;
	        $destinationCount=count($_REQUEST['destination'])-1;
            $way_point= [];
            for($i=0;$i<$destinationCount;$i++){
                $way_point[]=[
                    "location_name"=>$_REQUEST['destination'][$i]['dropPointAddress']['address'],
                    "order"=>$i+1,
                    "lat"=>$_REQUEST['destination'][$i]['dropPointAddress']['latitude'],
                    "lng"=>$_REQUEST['destination'][$i]['dropPointAddress']['longitude'],
                    "item_description"=>$_REQUEST['destination'][$i]['description'],
                    "recipient_name"=>$_REQUEST['destination'][$i]['recipients_name'],
                    "recipient_phone_no"=>$_REQUEST['destination'][$i]['signature'],
                    "no_packages"=>$_REQUEST['destination'][$i]['noPercel'],
                    "isSignature"=>1,//$_REQUEST['destination'][$i]['signature'],
                    "deliveryItemCategory"=>$_REQUEST['destination'][$i]['deliveryItemCategory'],
                    "deliveryOption"=>$_REQUEST['destination'][$i]['deliveryOptions'],
                    "inHand"=>$_REQUEST['destination'][$i]['deliveryMethods']
                ];
            }
            if(count($way_point)>0){

                $bookingRequest=[ "s_latitude"=>$_REQUEST['pickupLocations']['latitude'],
                "s_longitude"=>$_REQUEST['pickupLocations']['longitude'],
                "s_address"=>$_REQUEST['pickupLocations']['address'],
                "d_latitude"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['latitude'],
                "d_longitude"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['longitude'],
                "d_address"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['address'],
                "order"=>$destinationCount+1,
                "recipient_name"=>$_REQUEST['destination'][$destinationCount]['recipients_name'],
                "recipient_phone_no"=>$_REQUEST['destination'][$destinationCount]['signature'],
                "no_packages"=>$_REQUEST['destination'][$destinationCount]['noPercel'],
                "isSignature"=>1,//$_REQUEST['destination'][$destinationCount]['signature'],
                "item_description"=>$_REQUEST['destination'][$destinationCount]['description'],
                "service_type"=>"",
                "card_id"=>"",
                "distance"=>"",
                "estimated_fare"=>"",
                "payment_mode"=>"",
                "way_point"=>json_encode($way_point),
                "isInsured"=>"No",
                "deliveryItemCategory"=>$_REQUEST['destination'][$destinationCount]['deliveryItemCategory'],
                "deliveryOption"=>$_REQUEST['destination'][$destinationCount]['deliveryOptions'],
                "inHand"=>$_REQUEST['destination'][$destinationCount]['deliveryMethods']
            ];
            }
            else{

                $bookingRequest=[ "s_latitude"=>$_REQUEST['pickupLocations']['latitude'],
                "s_longitude"=>$_REQUEST['pickupLocations']['longitude'],
                "s_address"=>$_REQUEST['pickupLocations']['address'],
                "d_latitude"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['latitude'],
                "d_longitude"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['longitude'],
                "d_address"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['address'],
                "order"=>$destinationCount+1,
                "recipient_name"=>$_REQUEST['destination'][$destinationCount]['recipients_name'],
                "recipient_phone_no"=>$_REQUEST['destination'][$destinationCount]['signature'],
                "no_packages"=>$_REQUEST['destination'][$destinationCount]['noPercel'],
                "isSignature"=>1,//$_REQUEST['destination'][$destinationCount]['signature'],
                "item_description"=>$_REQUEST['destination'][$destinationCount]['description'],
                "service_type"=>"",
                "card_id"=>"",
                "distance"=>"",
                "estimated_fare"=>"",
                "payment_mode"=>"",
                "isInsured"=>"No",
                "deliveryItemCategory"=>$_REQUEST['destination'][$destinationCount]['deliveryItemCategory'],
                "deliveryOption"=>$_REQUEST['destination'][$destinationCount]['deliveryOptions'],
                "inHand"=>$_REQUEST['destination'][$destinationCount]['deliveryMethods']
            ];
            }
            $request['bookingRequest']=$bookingRequest;

       // exit;

            // "way_point": [
            // ];
            // {
            //    "location_name"
            //    "order"
            //    "lat"
            //    "lng"
            //    "item_description"
            //    "recipient_name"
            //     "recipient_phone_no"
            //     "no_packages"
            //     "isSignature"
            //   } */


        	$passengerRequest = new PassengerRequestService;
            $result = $passengerRequest->accessEstimatedFare($request);
            $getUserCard=   $passengerRequest->GetDefaultCard($request);

			if ($result['statusCode']==401) {
				return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
			}

	        return response(['message'=>$result['message'],"data"=>$result['data'] ,'getUserCard'=>$getUserCard,"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function confirmRequest(Request $request)
	{
		try{
            $request['token'] = $request->access_token;
	        $destinationCount=count($_REQUEST['destination'])-1;
            $way_point= [];
            for($i=0;$i<$destinationCount;$i++){
                $way_point[]=[
                    "location_name"=>$_REQUEST['destination'][$i]['dropPointAddress']['address'],
                    "order"=>$i+1,
                    "lat"=>$_REQUEST['destination'][$i]['dropPointAddress']['latitude'],
                    "lng"=>$_REQUEST['destination'][$i]['dropPointAddress']['longitude'],
                    "item_description"=>$_REQUEST['destination'][$i]['description'],
                    "recipient_name"=>$_REQUEST['destination'][$i]['recipients_name'],
                    "recipient_phone_no"=>$_REQUEST['destination'][$i]['signature'],
                    "no_packages"=>$_REQUEST['destination'][$i]['noPercel'],
                    "isSignature"=>1,//$_REQUEST['destination'][$i]['signature'],
                    "deliveryItemCategory"=>$_REQUEST['destination'][$i]['deliveryItemCategory'],
                    "deliveryOption"=>$_REQUEST['destination'][$i]['deliveryOptions'],
                    "inHand"=>$_REQUEST['destination'][$i]['deliveryMethods']
                ];
            }
            if(count($way_point)>0){

                $bookingRequest=[ "s_latitude"=>$_REQUEST['pickupLocations']['latitude'],
                "s_longitude"=>$_REQUEST['pickupLocations']['longitude'],
                "s_address"=>$_REQUEST['pickupLocations']['address'],
                "d_latitude"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['latitude'],
                "d_longitude"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['longitude'],
                "d_address"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['address'],
                "order"=>$destinationCount+1,
                "recipient_name"=>$_REQUEST['destination'][$destinationCount]['recipients_name'],
                "recipient_phone_no"=>$_REQUEST['destination'][$destinationCount]['signature'],
                "no_packages"=>$_REQUEST['destination'][$destinationCount]['noPercel'],
                "isSignature"=>1,//$_REQUEST['destination'][$destinationCount]['signature'],
                "item_description"=>$_REQUEST['destination'][$destinationCount]['description'],
                "service_type"=>$_REQUEST['service_type'],
                "card_id"=>$_REQUEST['card_id'],
                "distance"=>$_REQUEST['distance'],
                "estimated_fare"=>$_REQUEST['estimated_fare'],
                "payment_mode"=>$_REQUEST['payment_mode'],
                "way_point"=>json_encode($way_point),
                "isInsured"=>"No",
                "deliveryItemCategory"=>$_REQUEST['destination'][$destinationCount]['deliveryItemCategory'],
                "deliveryOption"=>$_REQUEST['destination'][$destinationCount]['deliveryOptions'],
                "inHand"=>$_REQUEST['destination'][$destinationCount]['deliveryMethods']
            ];
            }
            else{

                $bookingRequest=[ "s_latitude"=>$_REQUEST['pickupLocations']['latitude'],
                "s_longitude"=>$_REQUEST['pickupLocations']['longitude'],
                "s_address"=>$_REQUEST['pickupLocations']['address'],
                "d_latitude"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['latitude'],
                "d_longitude"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['longitude'],
                "d_address"=>$_REQUEST['destination'][$destinationCount]['dropPointAddress']['address'],
                "order"=>$destinationCount+1,
                "recipient_name"=>$_REQUEST['destination'][$destinationCount]['recipients_name'],
                "recipient_phone_no"=>$_REQUEST['destination'][$destinationCount]['signature'],
                "no_packages"=>$_REQUEST['destination'][$destinationCount]['noPercel'],
                "isSignature"=>1,//$_REQUEST['destination'][$destinationCount]['signature'],
                "item_description"=>$_REQUEST['destination'][$destinationCount]['description'],
                "service_type"=>$_REQUEST['service_type'],
                "card_id"=>$_REQUEST['card_id'],
                "distance"=>$_REQUEST['distance'],
                "estimated_fare"=>$_REQUEST['estimated_fare'],
                "payment_mode"=>$_REQUEST['payment_mode'],
                "isInsured"=>"No",
                "deliveryItemCategory"=>$_REQUEST['destination'][$destinationCount]['deliveryItemCategory'],
                "deliveryOption"=>$_REQUEST['destination'][$destinationCount]['deliveryOptions'],
                "inHand"=>$_REQUEST['destination'][$destinationCount]['deliveryMethods']
            ];
            }
            $request['bookingRequest']=$bookingRequest;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessRequest($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function ratingComment(Request $request)
	{
		try{
	        $this->validate($request, [
                'request_id' =>'required',
                'rating'=>'required',
                'comment'=>'required'
	        ]);
	        $request['token'] = $request->access_token;


	        $request->request_id = $request->request_id;
	        $request->rating = $request->rating;
	        $request->comment = $request->comment;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessRatingComment($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function scheduleRide(Request $request)
	{
		try{
	        $this->validate($request, [
                'service_type' => 'required',
                'date'=>'required',
                'time'=>'required',
	        ]);
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $request->request_id = $request->request_id;
	        $request->service_type_id = $request->service_type;
	        $request->payment_method = $request->payment_method;
	        if ($request->payment_method=='CARD') {
	        	$request->card_id = $request->card_id;
	        }
	        $request->date = $request->date;
	        $request->time = $request->time;
	        $request->isFemaleFriendly = $request->female_friendly;
        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessScheduleRide($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function tripHistoryIndex()
	{
		return view('pages.passenger.history.history');
	}


	public function passengerTripHistory(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessTripHistory($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function tripHistoryDetails(Request $request)
	{
		$request_id = $request->id;
		return view('pages.passenger.history.details',compact('request_id'));
	}

	public function passengerTripDetails(Request $request)
	{
	    try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

	        $request->request_id = $request->request_id;

	        $passengerRequest = new PassengerRequestService;
	        $result = $passengerRequest->accessTripDetails($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
	    }
	    catch(\Illuminate\Database\QueryException $e){
	        return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
	    }
	    catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
	        return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
	    }
	}

	public function scheduleTripIndex()
	{
		return view('pages.passenger.history.schedule');
	}


	public function upcommingScheduleTrip(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessUpCommingScheduleTrip($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

    public function scheduleTripDetailsIndex(Request $request)
    {
        $request_id = $request->id;
        return view('pages.passenger.history.schedule-details',compact('request_id'));
    }

    public function passengerScheduleTripDetails(Request $request)
    {
        try{
            $request['token'] = $request->access_token;
            $request['timeZone'] = $request->timeZone;

            $request->request_id = $request->request_id;

            $passengerRequest = new PassengerRequestService;
            $result = $passengerRequest->accessScheduleTripDetails($request);

            return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }
    

	public function cancelScheduleTrip(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
	        $request->request_id = $request->request_id;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessCancelScheduleTrip($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }
    public function cancelRideTrip(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	       // $request['timeZone'] = $request->timeZone;
            $request->request_id = $request->request_id;
            $request->comment = $request->comment;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessCancelRideTrip($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }


    public function pay(Request $request)
	{
		try{
	        $request['token'] = $request->access_token;
	       // $request['timeZone'] = $request->timeZone;
            $request->request_id = $request->request_id;
            $request->tips = $request->tips;

        	$passengerRequest = new PassengerRequestService;
			$result = $passengerRequest->accessPay($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
