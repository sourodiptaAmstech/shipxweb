<?php

namespace App\Http\Controllers\Website\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PassengerLeasingService;


class LeasingController extends Controller
{
    public function passengerLeasing()
    {
    	return view('pages.passenger.lease.lease');
    }

    public function hourlyLeaseRequest(Request $request)
    {
    	try{
	        $this->validate($request, [
	        	'dateOfJourny' =>'required',
				'fromTime' => 'required',
				'toTime' => 'required',
				'services_type'=>'required',
				'address' => 'required',
				'longitude'=>'required',
				'latitude'=>'required',
				'no_passenger'=>'required',
				'details'=>'required',
	        ]);

	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
	        
	        $fromTime = explode(' ', $request->fromTime);
			$request->fromTime = $fromTime[0];

	        $toTime = explode(' ', $request->toTime);
	        $request->toTime = $toTime[0];

	        $request->vehicle_equipment = $request->vehicle_equipment;
	        $request->services_type_id = $request->services_type;

			$passengerLeasing = new PassengerLeasingService;
			$result = $passengerLeasing->accessHourlyLease($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function dailyLeaseRequest(Request $request)
    {
    	try{
	        $this->validate($request, [
	        	'dateOfJourny' =>'required',
				'services_type'=>'required',
				'address' => 'required',
				'longitude'=>'required',
				'latitude'=>'required',
				'no_passenger'=>'required',
				'details'=>'required',
	        ]);
	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
	        
	        $request->vehicle_equipment = $request->vehicle_equipment;
	        $request->services_type_id = $request->services_type;

			$passengerLeasing = new PassengerLeasingService;
			$result = $passengerLeasing->accessDailyLease($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function longLeaseRequest(Request $request)
    {
    	try{
	        $this->validate($request, [
	        	'fromdate' =>'required',
                'todate' =>'required',
				'services_type'=>'required',
				'address' => 'required',
				'longitude'=>'required',
				'latitude'=>'required',
				'details'=>'required',
	        ]);

	        $request['token'] = $request->access_token;
	        $request['timeZone'] = $request->timeZone;
	       	if($request->fromdate>$request->todate){
	       		return response(['message'=>'End date cannot be before the Start date',"data"=>(object)[],"errors"=>(object)[]],401);
	       	}
	        $request->vehicle_equipment = $request->vehicle_equipment;
	        $request->services_type_id = $request->services_type;

			$passengerLeasing = new PassengerLeasingService;
			$result = $passengerLeasing->accessLongLease($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function serviceLeasingList(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;

			$passengerLeasing = new PassengerLeasingService;
			$result = $passengerLeasing->accessServiceLeasingList($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
