<?php

namespace App\Http\Controllers\Website\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\MessageService;

class ContactController extends Controller
{
    public function contactUs(){
        return view('pages.contact.contact');
    }

    public function driverContactUs(){
        return view('pages.contact.driver_contact');
    }

    public function passengerContactUs(){
        return view('pages.contact.passenger_contact');
    }

    public function aboutUs(){
        return view('pages.contact.about');
    }

    public function driverAboutUs(){
        return view('pages.contact.driver_about');
    }

    public function passengerAboutUs(){
        return view('pages.contact.passenger_about');
    }

    public function ContactToUs(Request $request)
    {
        try{
			$this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'message' => 'required',
            ]);
			
			$request['timeZone'] = $request->timeZone;
			
			$request->first_name = $request->first_name;
			$request->last_name = $request->last_name;
            $request->email = $request->email;
			$request->message = $request->message;

        	$messageService = new MessageService;
			$result = $messageService->accessContactUS($request);

            return response(['message'=>"Your message has been sent successfully.","data"=>[],"errors"=>[]],200);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function privacyPolicy()
    {
        return view('pages.contact.privacy-policy');
    }

    public function termsConditions()
    {
        return view('pages.contact.terms-conditions');
    }

    public function passengerPrivacyPolicy()
    {
        return view('pages.contact.pass-privacy-policy');
    }

    public function passengerTermsConditions()
    {
        return view('pages.contact.pass-terms-conditions');
    }

    public function driverPrivacyPolicy()
    {
        return view('pages.contact.driver-privacy-policy');
    }

    public function driverTermsConditions()
    {
        return view('pages.contact.driver-terms-conditions');
    }

    public function subscribeNewsLetter(Request $request)
    {
        try{
			$this->validate($request, [
                'email' => 'required|email',
            ]);
			$request['timeZone'] = $request->timeZone;
			$request->email = $request->email;

        	$messageService = new MessageService;
			$result = $messageService->accessNewsLetter($request);

            return response(['message'=>"Subscribed to the news letter.","data"=>$result['data'],"errors"=>[]],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

}
