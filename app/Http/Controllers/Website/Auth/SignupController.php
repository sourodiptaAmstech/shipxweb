<?php

namespace App\Http\Controllers\Website\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Curl;
use App\Services\PassengerRegistrationService;
use App\Services\DriverRegistrationService;
use App\Services\PassengerAuthService;
use App\Services\DriverAuthService;


class SignupController extends Controller
{
	
	public function passengerSocialSignup(Request $request)
	{
		$arrayName=json_decode(decrypt($request->param));
		
		return view('pages.auth.social_signup',compact('arrayName'));
	}

	public function signup()
	{
		return view('pages.auth.signup');
	}

	public function sendOTPFirstPassenger(Request $request)
	{
		try{
			$this->validate($request, [
				'user' => 'required',
	            'first_name' => 'required|min:3|max:255',
	            'last_name' => 'required|min:3|max:255',
	            'mobile_no' => 'required',
	            'email' => 'required|email',
	            'password' => 'required|between:6,255|confirmed',
	            'password_confirmation'=>'required',
	            'isChecked'=>'required'
	        ],
	        [
		        'isChecked.required' => 'Please read terms & conditions and privacy policy.',
		    ]
	    	);
	        $request->isdCode = '+'.$request->isdCode;
	        $request['timeZone'] = $request->timeZone;
			$obj = new PassengerRegistrationService;
			$result = $obj->accessSendOTP($request);
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

//DRIVER PART.........
	public function masterServiceList(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$obj = new DriverRegistrationService;
			$result = $obj->accessGetMasterServices($request);
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function modelList(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$obj = new DriverRegistrationService;
			$result = $obj->accessGetModels($request);
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function sendOTPFirstDriver(Request $request)
	{
		try{
			$this->validate($request, [
				'user' => 'required',
	            'first_name' => 'required|min:3|max:255',
	            'last_name' => 'required|min:3|max:255',
	            'mobile_no' => 'required',
	            'email' => 'required|email',
	            'address' => 'required',
	            'service_type' => 'required',
	            'make' => 'required',
	            'model' => 'required',
	            'model_year' => 'required',
	            'service_number' => 'required',
	            'password' => 'required|between:6,255|confirmed',
	            'password_confirmation'=>'required',
	            'disChecked'=>'required'
	        ],
	        [
		        'disChecked.required' => 'Please read terms & conditions and privacy policy.',
		    ]
	    	);
	        $request->isdCode = '+'.$request->isdCode;
	        $request['timeZone'] = $request->timeZone;
			$obj = new DriverRegistrationService;
			$result = $obj->accessSendOTP($request);
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}
//END DRIVER PART.........

	public function verifyOTP(Request $request)
	{
		if ($request->hidden_otp==$request->otp) {
			return response(['message'=>'OTP verified successfully.',"errors"=>array("exception"=>["Everything is OK."],"e"=>[]),"isActive"=>1,"hidden_user"=>$request->hidden_user],201);
		} else {
			return response(['message'=>'Please enter valid OTP.',"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],404);
		}
	}


	public function passengerRegister(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request->login_by = 'manual';
	        $request->device_id = 'device_id';
	        $request->device_token = 'device_token';
	        $request->device_type = 'web';
	        $request->first_name = $request->first_name;
	        $request->last_name = $request->last_name;
	        $request->mobile_no = $request->mobile_no;
	        $request->businessName = $request->businessName;
	        $request->email = $request->email;
	        $request->password = $request->password;
	        $request->password_confirmation = $request->password_confirmation;
	        $request->isActive = (integer)$request->isActive;
			$obj = new PassengerRegistrationService;
			$result = $obj->accessRegister($request);
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}
	

	public function sendOTPFirstSocialPassenger(Request $request)
	{
		try{
			$this->validate($request, [
	            'mobile_no' => 'required',
	        ]);
	        $request['timeZone'] = $request->timeZone;
			$obj = new PassengerRegistrationService;
			$result = $obj->accessSendOTP($request);
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function passengerSocialRegister(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request->social_unique_id = $request->social_unique_id;
			$request->login_by = $request->login_by;
	        $request->device_id = 'device_id';
	        $request->device_token = 'device_token';
	        $request->device_type = 'web';
	        $request->first_name = $request->first_name;
	        $request->last_name = $request->last_name;
	        $request->mobile_no = $request->mobile_no;
	        $request->isdCode = '+'.$request->isdCode;
	        $request->email_id = $request->email_id;
	        $request->picture = $request->picture;
	        $request->isActive = (integer)$request->isActive;

			$obj = new PassengerRegistrationService;
			$result = $obj->accessSocialRegistration($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function driverRegister(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request->login_by = 'manual';
	        $request->device_id = 'device_id';
	        $request->device_token = 'device_token';
	        $request->device_type = 'web';
	        $request->first_name = $request->first_name;
	        $request->last_name = $request->last_name;
	        $request->mobile_no = $request->mobile_no;
	        $request->isdCode = '+'.$request->isdCode;
	        $request->service_type = $request->service_type;
			$request->make = $request->make;
			$request->model = $request->model;
			$request->model_year = $request->model_year;
			$request->email = $request->email;
			$request->service_number = $request->service_number;
			$request->address = $request->address;
	        $request->password = $request->password;
	        $request->password_confirmation = $request->password_confirmation;
	        $request->isActive = (integer)$request->isActive;
	        $obj = new DriverRegistrationService;
			$result = $obj->accessRegister($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function driverEmailIndex()
	{
		return view('pages.auth.email');
	}

	public function updateEmail(Request $request)
	{
		try{
			$this->validate($request, [
				'email_id' => 'required|email|max:255',
	        ]);
	        $request['token'] = $request->access_token;
		    $request['timeZone'] = $request->timeZone;
	        $request->email_id = $request->email_id;

	        $driverReg = new DriverRegistrationService;
			$result = $driverReg->accessUpdateEmail($request);
			
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function emergencyContactIndex(Request $request)
	{
		$base64Decode = base64_decode($request->data);
        $decode_data = json_decode($base64Decode);
        if($decode_data!=null){
        	$email_id=$decode_data->email_id;
			return view('pages.auth.contact',compact('email_id'));
        }
        return view('pages.auth.contact');
	}

	public function passengerEmergencyContact(Request $request)
	{
		try{
			$this->validate($request, [
	            'first_contact_name' => 'required|min:3|max:255',
	            'first_contact_mobile_no' => 'required|digits_between:5,10',
	            'second_contact_name' => 'required|min:3|max:255',
	            'second_contact_mobile_no' => 'required|digits_between:5,10',
	        ]);
	        
	        $request['token'] = $request->access_token;
		    $request['timeZone'] = $request->timeZone;
	        $request->email_id = $request->email_id;

	        $request->emergOne_id = 0;
	        $request->emergOne_name = $request->first_contact_name;
	        $request->emergOne_isdCode = '+'.$request->first_code;
	        $request->emergOne_contact_no = $request->first_contact_mobile_no;

	        $request->emergTwo_id = 0;
	        $request->emergTwo_name = $request->second_contact_name;
	        $request->emergTwo_isdCode = '+'.$request->second_code;
	        $request->emergTwo_contact_no = $request->second_contact_mobile_no;

	        $passengerReg = new PassengerRegistrationService;
			$result = $passengerReg->accessEmergencyContact($request);
			
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function userTermsConditions(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$obj = new PassengerAuthService;
			$result = $obj->accessGetTermCondition($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function driverTermsConditions(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$obj = new DriverAuthService;
			$result = $obj->accessGetTermCondition($request);
			
			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}