<?php

namespace App\Http\Controllers\Website\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PassengerAuthService;
use App\Services\DriverAuthService;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;


class LoginController extends Controller
{
	public function homeIndex()
    {
    	return view('pages.home.home');
    }

	public function redirectToProvider(Request $request)
    {
        $cookie_name='user_type';
        $cookie_value=$request->user_type;
        setcookie($cookie_name, $cookie_value, 0, "/");
        return Socialite::driver('google')->redirect();
    }

  
    public function handleProviderCallback(Request $request)
    {
        $user = Socialite::driver('google')->user();
        
        $request['social_unique_id'] = $user->user['id'];
        $request['login_by'] = 'google';
        $request['timeZone'] = $_COOKIE['timezoneCookie'];
        $request['user_type'] = $_COOKIE['user_type'];

        $arrayName = array(
            'social_unique_id'=>$user->user['id'],
            'given_name'=>$user->user['given_name'],
            'family_name'=>$user->user['family_name'],
            'email'=>$user->user['email'],
            'avatar'=>$user->user['picture'],
            'login_by'=>'google',
            'user_type'=>$request['user_type'],
            'accessToken'=>$user->token
        );
        
        $encodeData = json_encode($arrayName);

        return redirect('social-signup?param='.encrypt($encodeData));
    }


    public function redirectToFacebook(Request $request)
    {
        $cookie_name='user_type';
        $cookie_value=$request->user_type;
        setcookie($cookie_name, $cookie_value, 0, "/");
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback(Request $request)
    {
    
        $user = Socialite::driver('facebook')->user();
        
        $request['social_unique_id'] = $user->user['id'];
        $request['login_by'] = 'facebook';
        $request['user_type'] = 'passenger';
        $request['timeZone'] = $_COOKIE['timezoneCookie'];
        $request['accessToken'] = $user->token;
        $request['user_type'] = $_COOKIE['user_type'];

        $arrayName = array(
            'social_unique_id'=>$user->user['id'],
            'given_name'=>$user->user['name'],
            'family_name'=>$user->nickname,
            'email'=>$user->user['email'],
            'avatar'=>$user->avatar,
            'login_by'=>'facebook',
            'user_type'=>$request['user_type'],
            'accessToken'=>$user->token
        );

        $encodeData = json_encode($arrayName);

        return redirect('social-signup?param='.encrypt($encodeData));
    }


    public function socialLogin(Request $request)
	{
		try{
	        $request['social_unique_id'] = $request->social_unique_id;
	        $request['timeZone'] = $request->timeZone;
	        $request->device_id = 'website';
	        $request->device_token = 'device_token';
	        $request->device_type = 'web';
            $request->accessToken = $request->accessToken;
	        $request->login_by = $request->login_by;

            $request->name = $request->given_name.' '.$request->family_name;
            $request->email = $request->email;
            $request->avatar = $request->avatar;
            $request->mobile = $request->mobile_no;
            $request->user_type = $request->user_type;

            if($request->user_type=='passenger'){
                $passengerAuth = new PassengerAuthService;
                $result = $passengerAuth->accessSocialLogin($request);
            }

            if($request->user_type=='driver'){
                $driverAuth = new DriverAuthService;
                $result = $driverAuth->accessSocialLogin($request);
            }

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function index()
	{
		return view('pages.auth.signin');
	}


	public function login(Request $request)
	{
		try{
	        $this->validate($request, [
	        	'user' => 'required',
	            'email' => 'required|email',
	            'password' => 'required|between:6,255',
	        ]);
	        
	        $request['timeZone'] = $request->timeZone;
            $request->grant_type = 'password';
            $request->client_id  = 2;
            $request->client_secret = 'XPobzDss6EBINLzfu7vVGXss5i3bmmfT1sPlKjUY';

	        $request->device_id = 'website';
	        $request->device_token = 'device_token';
	        $request->device_type = 'web';
	        $request->scope = '';

	        $request->username = $request->email;
	        $request->password = $request->password;
            
			if($request->user=='passenger'){
				$passengerAuth = new PassengerAuthService;
				$result = $passengerAuth->accessLogin($request);
                
	        }

	        if($request->user=='driver'){
	        	$driverAuth = new DriverAuthService;
				$result = $driverAuth->accessLogin($request);
	        }

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function passengerLogout(Request $request)
	{
		try{
	        $request['timeZone'] = $request->timeZone;
	        $request['token'] = $request->access_token;
	       
			$passengerAuth = new PassengerAuthService;
			$result = $passengerAuth->accessLogout($request);

	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


	public function driverLogout(Request $request)
	{
		try{
	        $request['timeZone'] = $request->timeZone;
	        $request['token'] = $request->access_token;
	       
   			$driverAuth = new DriverAuthService;
			$result = $driverAuth->accessLogout($request);
	    
	        return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}


}
