<?php

namespace App\Http\Controllers\Website\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PaymentService;


class PaymentController extends Controller
{
	public function verifyEmail(Request $request,$param)
    {
    	$ute_id = base64_decode($param);
    	$request->ute_id=$ute_id;
    	$passengerPayment = new PaymentService;
		$result = $passengerPayment->accessVerifyTransactionEmail($request);
        return view('verify-email');
    }

	public function paymentIndex(Request $request)
	{
		return view('pages.passenger.payment');
	}

	public function addPayment()
	{
		return view('pages.passenger.addpayment');
	}


	public function UpdateTranscationEmail(Request $request)
	{
		try{
			$this->validate($request, [
                'email_id' => 'required|email',
            ]);

			$request->email_id = $request->email_id;
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

        	$passengerPayment = new PaymentService;
			$result = $passengerPayment->accessUpdateTranscationEmail($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function CheckVerifyEmail(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

			$passengerPayment = new PaymentService;
			$result = $passengerPayment->accessCheckVerifyEmail($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function AddTransactionCard(Request $request)
	{
		// try{
			//$request->reference = $request->reference;
			//$request['timeZone'] = "GMT+05:30";//$request->timeZone;
			$request['timeZone'] = $_COOKIE['timezoneCookie'];
			$request['token'] = $request->access_token;
			//dd($request); exit;
        	$passengerPayment = new PaymentService;
			$result = $passengerPayment->accessAddCard($request);

			return redirect()->route('passenger.addpayment');

		// 	return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		// }
		// catch(\Illuminate\Database\QueryException $e){
  //           return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
  //       }
  //       catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
  //           return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
  //       }
	}

	public function setDefaultTransactionCard(Request $request)
	{
		try{
			$request->user_cards_id = $request->user_cards_id;
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

        	$passengerPayment = new PaymentService;
			$result = $passengerPayment->accessSetDefaultCard($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function deleteTransactionCard(Request $request)
	{
		try{
			$request->user_cards_id = $request->user_cards_id;
			$request['timeZone'] = $request->timeZone;
			$request['token'] = $request->access_token;

        	$passengerPayment = new PaymentService;
			$result = $passengerPayment->accessDeleteCard($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

}
