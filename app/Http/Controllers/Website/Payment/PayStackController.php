<?php

namespace App\Http\Controllers\Website\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\PaymentService;


class PayStackController extends Controller
{
    public function verifyTransaction(Request $request)
	{
		try{
	        $request['timeZone'] = $request->timeZone;
	        $request->reference = $request->reference;
			
			$paymentService = new PaymentService;
			$result = $paymentService->accessVerifyTransaction($request);

	        return ['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors'],'statusCode'=>$result['statusCode']];
    	}
    	catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
	}
}
