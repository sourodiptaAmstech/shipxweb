<?php

namespace App\Http\Controllers\Website\Vehicle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\VehicleService;


class VehicleController extends Controller
{
    public function getMake(Request $request)
	{
		try{
			$request['timeZone'] = $request->timeZone;

			$vehicleService = new VehicleService;
			$result = $vehicleService->accessGetServiceMake($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function getModel(Request $request)
	{
		try{
			$request->make = $request->make;
			$request['timeZone'] = $request->timeZone;

			$vehicleService = new VehicleService;
			$result = $vehicleService->accessGetServiceModel($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}

	public function getYear(Request $request)
	{
		try{
			$request->make = $request->make;
			$request->model = $request->model;
			$request['timeZone'] = $request->timeZone;

			$vehicleService = new VehicleService;
			$result = $vehicleService->accessGetServiceYear($request);

			return response(['message'=>$result['message'],"data"=>$result['data'],"errors"=>$result['errors']],$result['statusCode']);
		}
		catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
	}
}
