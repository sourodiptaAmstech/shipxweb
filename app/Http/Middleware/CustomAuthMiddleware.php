<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Encryption\DecryptException;
use Closure;

class CustomAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     private function signature(){
      $IP = $_SERVER['REMOTE_ADDR'];
      $mac = shell_exec("ip link | awk '{print $2}'");
      preg_match_all('/([a-z0-9]+):\s+((?:[0-9a-f]{2}:){5}[0-9a-f]{2})/i', $mac, $matches);
      $output = array_combine($matches[1], $matches[2]);
      $mac_address_values=$IP.":".env('APP_KEY');
      foreach($output as $key=>$val){
        if($val!=""){
          if($mac_address_values!=="")
          {  $mac_address_values=":".$mac_address_values.$val;}
          else
          {  $mac_address_values=$mac_address_values.$val;}
        }
      }
       //$mac_address_values;
      $shipxSignature= base64_encode(($mac_address_values));
      
        return base64_encode(($shipxSignature));
     }
    
     private function authToketDecode($token){
      return ((decrypt(base64_decode($token))));
     }

    
    public function handle($request, Closure $next)
    {
        if(!isset($_COOKIE["shipxSecurity"]) && !isset($_COOKIE["shipxSignature"])) {
          if($request->expectsJson()){
            return response()->json(array("msg"=>"Access Denied","reason"=>"Login Failed","code"=>401), 401, [], JSON_PRETTY_PRINT); 
          }
          else{
            return redirect()->route('home');
          }
        } else {
              $shipxSecurity=$_COOKIE["shipxSecurity"];
              $shipxSignature=$_COOKIE["shipxSignature"];
              // validate cookies signature
              if($this->signature()===$shipxSignature){
                  $request['access_token']=$this->authToketDecode($_COOKIE["shipxSecurity"]);
              }
              else{
                           
                if($request->expectsJson()){
                  return response()->json(array("msg"=>"Access Denied","reason"=>"Login Failed","code"=>401), 401, [], JSON_PRETTY_PRINT); 
                }
                else{
                  return redirect()->route('home');
                }
              }
            }
            return $next($request);
    }
}
