
            </section>

<div id="footer" class="d-flex align-items-center border-top" class="mt-5">
    <div class="container">
        <div class="row justify-content-center">
            <nav class="navbar navbar-expand-lg navbar-light">
            <div id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto my-3">
                    <li class="nav-item {{ Request::is('home') ? 'left-nav-active' : '' }}">
                        <a class="nav-link text-footer" href="{{ url('home') }}">
                            <strong class="px-2 py-0">Home</strong>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('about/us') ? 'left-nav-active' : '' }}">
                        <a class="nav-link text-footer" href="{{route('about.us')}}">
                            <strong class="px-2 py-0">About</strong>
                        </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link text-footer" href="#">
                            <strong class="px-2 py-0">Book a Delivery</strong>
                        </a>
                    </li> --}}
                    <li class="nav-item {{ Request::is('contact/us') ? 'left-nav-active' : '' }}">
                        <a class="nav-link text-footer" href="{{route('contact.us')}}">
                            <strong class="px-2 py-0">Contact</strong>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('privacy/policy') ? 'left-nav-active' : '' }}">
                        <a class="nav-link text-footer" href="{{route('privacy.policy')}}">
                            <strong class="px-2 py-0">Privacy Policy</strong>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('terms/conditions') ? 'left-nav-active' : '' }}">
                        <a class="nav-link text-footer" href="{{route('terms.conditions')}}">
                            <strong class="px-2 py-0">Terms & Conditions</strong>
                        </a>
                    </li>
                </ul>
            </div>
            </nav>
        </div>
        <div class="row justify-content-center">
            <nav class="navbar navbar-expand-lg navbar-light">
            <div id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto my-3">
                    <li class="nav-item">
                        <a class="nav-link text-footer" href="javascript:void(0)">
                            <span class="px-md-5 py-0 border-right-footer">
                            <img style="height: 20px; width: 20px"
                             class="logo" src="{{URL::asset('/')}}assets/img/phone.png" alt="">
                             +9498008455
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-footer" href="javascript:void(0)">
                            <span class="px-md-5 py-0">
                            <img style="height: 20px; width: 20px"
                             class="logo" src="{{URL::asset('/')}}assets/img/email.png" alt="">
                             info@shipxnow.com
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            </nav>
        </div>
        <div class="row w-100 h-100 align-items-center">
            <ul class="list-group list-group-horizontal footer">
                <a href="https://www.facebook.com/ShipX-technology-102287534974490" target="_blank">
                    <li class="list-group-item rounded m-2 text-dark">
                            <i class="fab fa-facebook-f"></i>
                    </li>
                </a>
                <li class="list-group-item rounded m-2">
                    <i class="fab fa-twitter"></i>
                </li>
                <li class="list-group-item rounded m-2">
                    <i class="fab fa-linkedin-in"></i>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid border-top">
    <div class="row justify-content-center text-footer my-3">
        &copy; 2020 <span class="font-weight-bold mx-1 text-dark">ShipX</span>. All Right Reserved.
    </div>
</div>

</div>
</div>

<button onclick="topFunction()" id="myBtn" title="Go to top">
<i class="fa fa-chevron-up" aria-hidden="true"></i>
</button>

{{-- <script src="{{URL::asset('/')}}assets/js/jquery-slim.min.js" crossorigin="anonymous"></script> --}}
<script src="{{URL::asset('/')}}assets/js/popper.min.js" crossorigin="anonymous"></script>
<script src="{{URL::asset('/')}}assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{URL::asset('/')}}assets/slick/slick.js"></script>

<script src="{{URL::asset('/')}}assets/js/bootstrap-datetimepicker.min.js" crossorigin="anonymous"></script>
<script src="{{URL::asset('/')}}assets/js/bootstrap-select.min.js" crossorigin="anonymous"></script>
{{-- <script src="{{URL::asset('/')}}assets/js/intlTelInput.min.js" crossorigin="anonymous"></script>
<script src="{{URL::asset('/')}}assets/js/intlTelInput-jquery.min.js" crossorigin="anonymous"></script> --}}
<!-- Custom JS -->
<script src="{{URL::asset('/')}}assets/js/main.js" crossorigin="anonymous"></script>
{{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9HoYIv5QNTwWvPHE62rc6gJ2UaoXJ5P8&libraries=places&callback=initMap"></script> --}}

</body>
</html>
