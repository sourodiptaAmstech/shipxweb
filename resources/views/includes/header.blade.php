<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ShipX</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="{{URL::asset('/')}}assets/img/icon.png" type="image/gif" sizes="16x16">

        <!-- Font Muli -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap-datetimepicker.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap-select.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        {{-- <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/intlTelInput.css">
        <!-- slick slider --> --}}
        <link rel="stylesheet" type="text/css" href="{{URL::asset('/')}}assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('/')}}assets/slick/slick-theme.css"/>
        <!-- Animate Css -->
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        />
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/main.css" crossorigin="anonymous">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="{{URL::asset('/')}}assets/js/google.map.js" crossorigin="anonymous"></script>

        <style>
            .slick-list {
                padding: 65px !important;
            }

            .slick-slide {
                margin: 0px 20px;
            }

            .slick-slide img {
                width: 100%;
                object-fit: cover;
            }

            .slick-prev:before,
            .slick-next:before {
                color: black;
            }


            .slick-slide {
                transition: all ease-in-out .3s;
                opacity: .2;
            }

            .slick-active {
                opacity: .5;
            }

            .slick-current {
                opacity: 1;
                transform: scale(1.2);
            }

            .sidenav {
                height: 100%;
                width: 0;
                position: fixed;
                z-index: 1;
                top: 0;
                left: 0;
                background-color: #e6e6e6;
                overflow-x: hidden;
                transition: 0.5s;
                padding-top: 60px;
            }
            .openSidenav {
                    display: none;
            }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 16px;
                color: #000;
                display: block;
                transition: 0.3s;
            }

            .sidenav a:hover {
                color: #a3a7b1;
            }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 8px;
                font-size: 36px;
                margin-left: 50px;
            }

            @media screen and (max-width: 999px) {
                .openSidenav {display: block;}
            }

            @media screen and (max-height: 450px) {
                .sidenav {padding-top: 15px;}
                .sidenav a {font-size: 18px;}
            }
        </style>
    </head>
    <body>

        <div class="container-fluid">
            <div class="row">

                <!-- section main -->
                <section id="main">

               {{--  <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <a href="{{url('myAccount')}}" class="{{ Request::is('myAccount') ? 'side-nav-active' : '' }}">Edit Profile</a>
                    <a href="#">Change Email Address</a>
                    <a href="#">Change Password</a>
                    <a href="#">History</a>
                    <a href="#">My Upcoming Bookings</a>
                    <a href="#">Payments</a>
                    <a href="#">Report an Issue</a>
                    <a href="#">Logout</a>
                </div> --}}


                    <div class="container-fluid bg-header">
                        <nav class="navbar navbar-expand-lg navbar-light py-4">
                            {{-- <span class="m-0 openSidenav" style="font-size:30px;cursor:pointer" onclick="openNav()">&#8942;</span> --}}
                            <a class="navbar-brand ml-3 ml-md-5" href="{{route('home')}}">
                                <img class="logo" src="{{URL::asset('/')}}assets/img/logo.png" alt="">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-2">
                                    <li class="nav-item py-2 {{ Request::is('home') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{ url('home') }}">
                                            <strong>Home</strong>
                                        </a>
                                    </li>
                                    <li class="nav-item py-2 {{ Request::is('about/us') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{route('about.us')}}">
                                            <strong>About</strong>
                                        </a>
                                    </li>
                                    {{-- <li class="nav-item py-4 {{ Request::is('bookingRequest') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{ url('bookingRequest') }}">
                                            <strong>Book a Delivery</strong>
                                        </a>
                                    </li> --}}
                                    <li class="nav-item py-2 {{ Request::is('contact/us') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{route('contact.us')}}">
                                            <strong>Contact</strong>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="list-group list-group-horizontal mx-md-5">
                                    <li class="list-group-item bg-header border-0 p-0 mr-3
                                        {{ Request::is('signin') ? 'right-nav-active' : '' }}">
                                        <a class="nav-link px-3 rounded text-muted font-weight-bold"
                                            href="{{ url('signin') }}">
                                            <img src="{{URL::asset('/')}}assets/img/avatar.png" alt="">
                                            Sign In
                                        </a>
                                    </li>
                                    <li class="list-group-item bg-header border-0 p-0
                                        {{ Request::is('signup') ? 'right-nav-active' : '' }}">
                                        <a class="nav-link px-3 rounded text-muted font-weight-bold"
                                            href="{{ url('signup') }}">
                                            <img src="{{URL::asset('/')}}assets/img/padlock.png" alt="">
                                            Sign Up
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>

             <div id="bootboxModal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body bootboxBody">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary bootboxBtn">OK</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="overlay">
                <div class="overlay__inner">
                    <div class="overlay__content"><span class="spinner"></span></div>
                </div>
            </div>
