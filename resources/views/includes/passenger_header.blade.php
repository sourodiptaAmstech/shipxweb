<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ShipX</title>
        <script type="text/javascript">
            var APP_URL = "{{ url('/') }}";
            </script>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="{{URL::asset('/')}}assets/img/icon.png" type="image/gif" sizes="16x16">

        <!-- Font Muli -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap-datetimepicker.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap-select.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        {{-- <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/intlTelInput.css"> --}}
        <!-- slick slider -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('/')}}assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('/')}}assets/slick/slick-theme.css"/>
        <!-- Animate Css -->
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        />
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/main.css" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="{{URL::asset('/')}}assets/js/rateyo-js/jquery.rateyo.min.css"/>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


        <style>
            .slick-list {
                padding: 65px !important;
            }

            .slick-slide {
                margin: 0px 20px;
            }

            .slick-slide img {
                width: 100%;
                object-fit: cover;
            }

            .slick-prev:before,
            .slick-next:before {
                color: black;
            }
            .slick-slide {
                transition: all ease-in-out .3s;
                opacity: .2;
            }

            .slick-active {
                opacity: .5;
            }

            .slick-current {
                opacity: 1;
                transform: scale(1.2);
            }

            .sidenav {
                height: 100%;
                width: 0;
                position: fixed;
                z-index: 1;
                top: 0;
                left: 0;
                background-color: #e6e6e6;
                overflow-x: hidden;
                transition: 0.5s;
                padding-top: 60px;
            }
            .openSidenav {
                    display: none;
            }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 16px;
                color: #000;
                display: block;
                transition: 0.3s;
            }

            .sidenav a:hover {
                color: #a3a7b1;
            }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 8px;
                font-size: 36px;
                margin-left: 50px;
            }

            @media screen and (max-width: 999px) {
                .openSidenav {display: block;}
            }

            @media screen and (max-height: 450px) {
                .sidenav {padding-top: 15px;}
                .sidenav a {font-size: 18px;}
            }
        </style>
    </head>
    <body>

        <div class="container-fluid">
            <div class="row">

                <!-- section main -->
                <section id="main">

                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <a href="{{route('passenger.account')}}" class="{{ Request::is('passenger/account') ? 'side-nav-active' : '' }}">Edit Profile</a>
                    {{-- <a href="#">Change Email Address</a> --}}
                    <a href="{{route('passenger.change.password.index')}}" class="{{ Request::is('passenger/change/password/index') ? 'side-nav-active' : '' }}">Change Password</a>
                    <a href="{{route('passenger.trip-history')}}" class="{{ Request::is('passenger/trip-history') ? 'side-nav-active' : '' }}{{ Request::is('passenger/trip-details') ? 'side-nav-active' : '' }}">History</a>
                    <a href="{{route('passenger.upcoming-trip')}}" class="{{ Request::is('passenger/upcoming-trip') ? 'side-nav-active' : '' }}{{ Request::is('passenger/upcoming-trip/details') ? 'side-nav-active' : '' }}">My Upcoming Bookings</a>
                    <a href="{{route('passenger.payment')}}" class="{{ Request::is('passenger/payment') ? 'side-nav-active' : '' }}{{ Request::is('passenger/addpayment') ? 'side-nav-active' : '' }}{{ Request::is('passenger/promocode') ? 'side-nav-active' : '' }}">Payments</a>
                    <a href="{{route('passenger.report-issue')}}" class="{{ Request::is('passenger/report-issue') ? 'side-nav-active' : '' }}">Report an Issue</a>
                    <a href="{{route('passenger.help')}}" class="{{ Request::is('passenger/help') ? 'side-nav-active' : '' }}">Help</a>
                    {{-- <a href="#">Share</a> --}}
                    {{-- <a href="#">Logout</a> --}}
                </div>


                    <div class="container-fluid bg-header">
                        <nav class="navbar navbar-expand-lg navbar-light py-4">
                            <span class="m-0 openSidenav" style="font-size:30px;cursor:pointer" onclick="openNav()">&#8942;</span>
                            <a class="navbar-brand ml-3 ml-md-5" href="{{route('passenger.home')}}">
                                <img class="logo" src="{{URL::asset('/')}}assets/img/logo.png" alt="">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-2">
                                    <li class="nav-item py-2 {{ Request::is('passenger/home') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{ route('passenger.home') }}">
                                            <strong>Home</strong>
                                        </a>
                                    </li>
                                    <li class="nav-item py-2 {{ Request::is('passenger/request/booking') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{ route('passenger.request.booking') }}">
                                            <strong>Book a Delivery</strong>
                                        </a>
                                    </li>

                                    <li class="nav-item py-2 {{ Request::is('passenger/about/us') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{route('passenger.about.us')}}">
                                            <strong>About</strong>
                                        </a>
                                    </li>

                                    <li class="nav-item py-2 {{ Request::is('passenger/contact/us') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{route('passenger.contact.us')}}">
                                            <strong>Contact</strong>
                                        </a>
                                    </li>

                                    <li class="nav-item py-2 {{ Request::is('passenger/account') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{route('passenger.account')}}">
                                            <strong>My account</strong>
                                        </a>
                                    </li>
                                    <li class="nav-item py-2">
                                        <a class="nav-link px-4" href="javascript:void(0)" id="logout"><strong>Logout</strong> <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                                    </li>
                                </ul>

                            </div>
                        </nav>
                    </div>

             <div id="bootboxModal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body bootboxBody">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary bootboxBtn">OK</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="overlay">
                <div class="overlay__inner">
                    <div class="overlay__content"><span class="spinner"></span></div>
                </div>
            </div>

<script>
t = new Date().toUTCString().split(' ');
var timeZone = t[t.length-1] + moment().format('Z');
$('#logout').on('click', function(event){
    event.preventDefault();
    bootbox.confirm('Are you sure to logout?', function (res) {
        if (res){
            $(".overlay").show();
            $.ajax({
                url: "{{ route('passenger.logout') }}",
                method:"GET",
                data:{ timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    document.cookie = "shipxSecurity= ; expires=0; path=/;";
                    document.cookie = "shipxSignature= ; expires=0; path=/;";
                    location.href="{{route('signin')}}";
                },
                error: function(response){
                    $(".overlay").hide();
                }
            });
        }
    });
});
</script>
