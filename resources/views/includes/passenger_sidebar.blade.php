<div class="pb-3 bg-white sidebar-wp">
    <ul class="list-group sidebar mb-4">
        <li class="list-group-item font-weight-bold {{ Request::is('passenger/account') ? 'side-nav-active' : '' }}">
            <a href="{{route('passenger.account')}}">Edit Profile</a>
        </li>
       {{--  <li class="list-group-item font-weight-bold">
            Change Email Address
        </li> --}}
        <li class="list-group-item font-weight-bold {{ Request::is('passenger/change/password/index') ? 'side-nav-active' : '' }}">
            <a href="{{route('passenger.change.password.index')}}">Change Password</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('passenger/trip-history') ? 'side-nav-active' : '' }}{{ Request::is('passenger/trip-details') ? 'side-nav-active' : '' }}">
            <a href="{{route('passenger.trip-history')}}">History</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('passenger/upcoming-trip') ? 'side-nav-active' : '' }}{{ Request::is('passenger/upcoming-trip/details') ? 'side-nav-active' : '' }}">
            <a href="{{route('passenger.upcoming-trip')}}">My Upcoming Bookings</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('passenger/payment') ? 'side-nav-active' : '' }}{{ Request::is('passenger/addpayment') ? 'side-nav-active' : '' }}{{ Request::is('passenger/promocode') ? 'side-nav-active' : '' }}">
            <a href="{{route('passenger.payment')}}">Payments</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('passenger/report-issue') ? 'side-nav-active' : '' }}">
            <a href="{{route('passenger.report-issue')}}">Report an Issue</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('passenger/help') ? 'side-nav-active' : '' }}">
            <a href="{{route('passenger.help')}}">Help</a>
        </li>
        {{-- <li class="list-group-item font-weight-bold">
            Share
        </li> --}}
       {{--  <li class="list-group-item font-weight-bold">
            Logout
        </li> --}}
    </ul>
</div>