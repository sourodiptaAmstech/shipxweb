<div class="pb-3 bg-white sidebar-wp">
    <ul class="list-group sidebar mb-4">
        <li class="list-group-item font-weight-bold {{ Request::is('driver/account') ? 'side-nav-active' : '' }}{{ Request::is('driver/document') ? 'side-nav-active' : '' }}">
            <a href="{{route('driver.account')}}">Edit Profile</a>
        </li>
        {{-- <li class="list-group-item font-weight-bold">
            Change Email Address
        </li> --}}
        <li class="list-group-item font-weight-bold {{ Request::is('driver/change/password/index') ? 'side-nav-active' : '' }}">
            <a href="{{route('driver.change.password.index')}}">Change Password</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('driver/trip-history') ? 'side-nav-active' : '' }}{{ Request::is('driver/trip-details') ? 'side-nav-active' : '' }}">
            <a href="{{route('driver.trip-history')}}">History</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('driver/upcoming-trip') ? 'side-nav-active' : '' }}{{ Request::is('driver/upcoming-trip/details') ? 'side-nav-active' : '' }}">
            <a href="{{route('driver.upcoming-trip')}}">My Upcoming Bookings</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('driver/wallet') ? 'side-nav-active' : '' }}">
            <a href="{{route('driver.wallet')}}">Wallet</a>
        </li>
        <li class="list-group-item font-weight-bold {{ Request::is('driver/help') ? 'side-nav-active' : '' }}">
            <a href="{{route('driver.help')}}">Help</a>
        </li>
        {{-- <li class="list-group-item font-weight-bold">
            Share
        </li> --}}
        {{-- <li class="list-group-item font-weight-bold">
            Logout
        </li> --}}
    </ul>
</div>