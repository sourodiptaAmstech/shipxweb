<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ShipX</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="{{URL::asset('/')}}assets/img/icon.png" type="image/gif" sizes="16x16">

        <!-- Font Muli -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap-datetimepicker.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/bootstrap-select.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        {{-- <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/intlTelInput.css">
        <!-- Custom CSS --> --}}
        <link rel="stylesheet" href="{{URL::asset('/')}}assets/css/main.css" crossorigin="anonymous">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    </head>
    <body>

        <div class="container-fluid">
            <div class="row">

                <!-- section main -->
                <section id="main">

                    <div class="container-fluid bg-header border-bottom">
                        <nav class="navbar navbar-expand-lg navbar-light pb-0">
                            <a class="navbar-brand ml-5">
                                <img class="logo" src="{{URL::asset('/')}}assets/img/logo.png" alt="">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-2">
                                    <li class="nav-item py-4 {{ Request::is('home') ? 'left-nav-active' : '' }}">
                                        <a class="nav-link px-4" href="{{ url('home') }}">
                                            <strong>Home</strong>
                                        </a>
                                    </li>
                                    <li class="nav-item py-4">
                                        <a class="nav-link px-4" href="#">
                                            <strong>Book a truck</strong>
                                        </a>
                                    </li>
                                    <li class="nav-item py-4">
                                        <a class="nav-link px-4" href="#">
                                            <strong>About us</strong>
                                        </a>
                                    </li>
                                    <li class="nav-item py-4">
                                        <a class="nav-link px-4" href="#">
                                            <strong>Contact us</strong>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="list-group list-group-horizontal mx-5">
                                    <li class="list-group-item bg-header border-0 p-0 mr-3
                                        {{ Request::is('signin') ? 'right-nav-active' : '' }}">
                                        <a class="nav-link px-3 rounded text-muted font-weight-bold"
                                            href="{{ url('signin') }}">
                                            <img class="logo" src="{{URL::asset('/')}}assets/img/avatar.png" alt="">
                                            Sign In
                                        </a>
                                    </li>
                                    <li class="list-group-item bg-header border-0 p-0
                                        {{ Request::is('signup') ? 'right-nav-active' : '' }}">
                                        <a class="nav-link px-3 rounded text-muted font-weight-bold"
                                            href="{{ url('signup') }}">
                                            <img class="logo" src="{{URL::asset('/')}}assets/img/padlock.png" alt="">
                                            Sign Up
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>

            <div id="bootboxModal" class="modal fade" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body bootboxBody">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary bootboxBtn">OK</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="overlay">
                <div class="overlay__inner">
                    <div class="overlay__content"><span class="spinner"></span></div>
                </div>
            </div>
