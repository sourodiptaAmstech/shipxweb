<!-- header -->
@include('includes.driver_header')

<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.driver_sidebar')
            </div>
            <div class="col-lg-9 px-0">

                <div class="px-md-5 mx-md-5">
                    <div class="px-3 upper pt-5">
                        
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success changsuc_alert" style="display: none;">
                            <p class="text-success" id="changsuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.MY_WALLET')}}</h2>
                       
                        <div class="list-group-item w-75 shadow-lg p-3 mb-2 rounded border-default" style="height: 250px;background-color: #f9b12e;">
                            {{-- <div class="row">
                                <div class="col-md-6 col-sm-6 col-6"> --}}
                                <div class="justify-content-center text-center p-5 text-white" >
                                    <h5 class="">Earnings This Week</h5>
                                    <h3 class="pt-3 font-weight-bold" id="balance"></h3>
                                </div>
                                    
                                {{-- </div>
                            </div> --}}
                        </div>
                        <div class="row pt-4">
                            <div class="col-lg-6">
                                <div class="list-group-item w-50 shadow-lg p-5 mb-2 rounded border-default">
                                    {{-- <div class="row">
                                        <div class="col-md-6 col-sm-6 col-6"> --}}
                                        <div class="justify-content-center text-center text-dark" >
                                            <h5 class="">Bookings</h5>
                                            <h3 class="pt-3 font-weight-bold" id="trip"></h3>
                                        </div>
                                            
                                        {{-- </div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="list-group-item w-50 shadow-lg p-5 mb-2 rounded border-default">
                                    {{-- <div class="row">
                                        <div class="col-md-6 col-sm-6 col-6"> --}}
                                        <div class="justify-content-center text-center text-dark" >
                                            <h5 class="">{{trans('weblng.BALANCE')}}</h5>
                                            <h3 class="pt-3 font-weight-bold" id="balan"></h3>
                                        </div>

                                        {{-- </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>

                        {{-- <div class="col-lg-3 col-md-6 col-xs-12"> --}}
                            {{-- <div class="box box-block bg-white tile tile-1 mb-2">
                                <div class="t-icon right"><span class="bg-primary"></span><i class="ti-view-grid"></i></div>
                                <div class="t-content">
                                    <h6 class="text-uppercase mb-1">User Cancelled Count</h6>
                                    <h1 class="mb-1"></h1>
                                </div>
                            </div> --}}
                        {{-- </div> --}}

                        <div class="d-flex justify-content-start mb-3 mt-5">
                            {{-- <button type="button" class="btn btn-lrg text-white">View All Transaction</button> --}}
                        <a href="javascript:void(0)" class="btn btn-lg btn-success-theme text-white">View Transactions</a>
                        </div>
                        {{-- <button type="button" class="btn btn-outline-secondary">Secondary</button> --}}
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    $(".overlay").show();
    $.ajax({
        url: "{{ route('driver.payment.earning') }}",
        method:"GET",
        data:{ timeZone: timeZone },
        success: function(response){
            console.log(response);
            $('#balance').text(response.data.weekly_earning);
            $('#balan').text(response.data.weekly_earning);
            $('#trip').text(response.data.weeklyridecount);
            $(".overlay").hide();
        },
        error: function(response){
            $(".overlay").hide();
        }
    });
</script>
<!-- footer -->
@include('includes.driver_footer')