<!-- header -->
@include('partials.driver_header')
<div class="bg-white mt-5 py-5 px-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                @include('layouts.driver_sidebar')
            </div>
            <div class="col-lg-8">
              <div class="px-sm-3 pt-5">
                <h2 class="font-weight-bold mb-4">{{trans('weblng.MY_TRANSACTION')}}</h2>

                <ul class="list-group listTransaction">
                </ul>

              </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        jq.ajax({
            url: "{{ route('driver.transaction.list') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                console.log(response);
                $(".overlay").hide();
                var options = '';
                if(response.data.data.length==0){
                    options=`<h6 style="font-size: x-large;" class="mt-5 text-muted">{{trans('weblng.TRANSACTION')}}</h6>`;
                }
                $.each(response.data.data, function(key, value) {
                    if (value.request_status=='CANCELBYDRIVER') {
                        var types = 'Dr';
                        var clas = 'text-danger';
                    } else {
                        var types = 'Cr';
                        var clas = '';
                    }
                    if(value.payment_method=='CARD'){
                        var card_img = `<img src="{{URL::asset('/')}}storage/assets/img/card.png" alt="" class="tran-logo">`;
                    } else if(value.payment_method=='NONE'){
                        var card_img = '';
                    } else {
                        var card_img = `<div style="width: 57px;height: 32px;background: black;color: white;text-align: center;float: right;padding-top: 5px;">{{trans('weblng.Cash')}}</div>`;
                    }
                    options += `
                        <li class="list-group-item w-100 shadow-lg p-3 mb-2 bg-white rounded border-default">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-8">
                                    <h5 class="font-weight-bold1">{{trans('weblng.BOOKINGID')}}:  `+value.request_no+`</h5>
                                    <h6 class="pt-3">`+value.transactionCreated+`</h6>

                                </div>
                                <div class="col-md-4 col-sm-4 col-4">
                                    <h6 class="font-weight-bold text-right `+clas+`">`+types+` ₦`+value.cost+`</h6>
                                    <div class="text-right mt-2 tran-img">
                                        `+card_img+`
                                    </div>
                                </div> 
                            </div>
                        </li>
                        `;
                });
                $('.listTransaction').append(options);
            },
            error: function(response){
                $(".overlay").hide();
            }
        });
        
    });
</script>

<!-- footer -->
@include('partials.driver_footer')