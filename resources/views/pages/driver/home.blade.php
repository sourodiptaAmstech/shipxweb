<!-- header -->
@include('includes.driver_header')

<!-- Banner section -->
<div class="container-fluid banner pb-5 pt-3">
    <div class="row pt-0 py-5 pl-0">
        <div class="col-md-6 my-5 ml-5 py5 pl-0">
            <h1 class="py-2 font-weight-bold">You click it. We pick it.</h1>
            <h5 class="py-2 font-weight-bold"><span class="text-success-theme">From your door with live real time</span> Tracking to your destination</h5>
            <div class="my-5">
                <a class="btn btn-lrg-banner btn-store text-white mr-3 mb-3" href="https://apps.apple.com/us/app/id1528462958">
                    <div class="d-flex">
                        <img src="{{URL::asset('/')}}assets/img/apple.png" alt="">  
                        <div>
                            <div class="d-block">
                                <h6 style="font-size: 15px">Download on the</h6>
                            </div>
                            <div class="d-block">App store</div>
                        </div>
                    </div>
                </a>
                <a class="btn btn-lrg-banner btn-store text-white mr-3 mb-3" href="https://play.google.com/store/apps/details?id=com.shipx.user">
                    <div class="d-flex">
                        <img src="{{URL::asset('/')}}assets/img/google.png" alt="">  
                        <div>
                            <div class="d-block">
                                <h6 style="font-size: 15px">Android app on</h6>
                            </div>
                            <div class="d-block">Google Play</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- About -->
<div class="container about">
        <h2 class="text-center font-weight-bold pt-md-5 pb-md-3">What We Do</h2>
        <h3 class="text-center pb-3">You can schedule multiple stop routes</h3>
    <div class="row py-5 mb-5">
        <div class="col-md-6">
            {{-- <video src="{{URL::asset('/')}}assets/img/video.mp4" width="100%" height="100%" controls="controls">
                Your browser does not support the HTML5 Video element.
            </video> --}}

            <iframe width="100%" height="315" src="https://www.youtube.com/embed/jeXMmJAukwU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
            
        </div>
        <div class="col-md-6">
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                We are new and unique app that will redefine the delivery system, customers will have and experience new technology called SHIPX, by finger tip they can order what they want and delivered right to there destination and they will enjoy the live tracking, free certified signature on delivery, picture of the items delivered and mostly in affordable and competitive pricing from other delivery companies. We deliver mails, legal documents, certified mail, gift boxes, unlimited mail boxes, office supplies, auto part supplies to dealers or customers, medical clinic supplies, medical blood supplies and samples.
            </h6>
            <a href="{{route('driver.about.us')}}">
            <div class="d-flex justify-content-start mt-5">
                <button type="button" class="btn btn-lrg btn-success-theme text-white">Read More</button>
            </div>
            </a>
        </div>
    </div>
</div>
<!-- About1 -->
<div class="container about">
    <h2 class="text-center font-weight-bold pb-md-3">For Driver</h2>
    <div class="row py-5">
        <div class="col-md-6">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/Ea3XcbKV9wA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
        </div>
        <div class="col-md-6">
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                If you interested in becoming a driver with us please create an account and start earning top rate per mile.
            </h6>
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                No more people seated in your car, coupe cars welcome, chose your car type X or XL, enjoy the road with ShipX delivery now and unlock the future.
            </h6>
        </div>
    </div>
</div>
<!-- Process -->
<div class="container-fluid process pt-5">
        <h2 class="text-center pb-5 font-weight-bold">Advance Features</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="py-5">
                    <div class="row align-items-center mx-0 mb-5 animate__animated animate__fadeInLeft">
                        <div class="col-3 px-0">
                            <div class="feature-icon bg-white rounded p-3 mr-md-0 d-flex align-items-center" style="height:70px; width: 80px">
                                <img class="img-fluid" src="{{url('assets/img/feature_1.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-9 px-0">
                            <h5 class="text-left font-weight-bold pl-3">Live tracking</h5>
                        </div>
                    </div>
                    <div class="row align-items-center mx-0 mb-5 animate__animated animate__fadeInLeft">
                        <div class="col-3 px-0">
                            <div class="feature-icon bg-white rounded p-3 mr-md-0 d-flex align-items-center" style="height:70px; width: 80px">
                                <img class="img-fluid" src="{{url('assets/img/feature_2.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-9 px-0">
                            <h5 class="text-left font-weight-bold pl-3">All done with us on flat rate - same day delivery</h5>
                        </div>
                    </div>
                    <div class="row align-items-center mx-0 mb-5 animate__animated animate__fadeInLeft">
                        <div class="col-3 px-0">
                            <div class="feature-icon bg-white rounded p-3 mr-md-0 d-flex align-items-center" style="height:70px; width: 80px">
                                <img class="img-fluid" src="{{url('assets/img/feature_3.png')}}" alt="">
                            </div>
                        </div>
                        <div class="col-9 px-0">
                            <h5 class="text-left font-weight-bold pl-3">Affordable and competitive pricing</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 position-relative">
                <img class="img-fluid feature-md" src="{{url('assets/img/feature.png')}}" alt="">
            </div>
            <div class="col-md-4">
                <div class="py-5">
                    <div class="row align-items-center mx-0 mb-5 animate__animated animate__fadeInRight">
                        <div class="col-9 px-0">
                            <h5 class="text-right font-weight-bold pr-3">No more extra chargers for signature on delivery</h5>
                        </div>
                        <div class="col-3 px-0">
                            <div class="feature-icon bg-white rounded p-3 ml-md-0 d-flex align-items-center" style="height:70px; width: 80px">
                                <img class="img-fluid" src="{{url('assets/img/feature_4.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center mx-0 mb-5 animate__animated animate__fadeInRight">
                        <div class="col-9 px-0">
                            <h5 class="text-right font-weight-bold pr-3">No more wasting times on getting quotes</h5>
                        </div>
                        <div class="col-3 px-0">
                            <div class="feature-icon bg-white rounded p-3 ml-md-0 d-flex align-items-center" style="height:70px; width: 80px">
                                <img class="img-fluid" src="{{url('assets/img/feature_5.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center mx-0 mb-5 animate__animated animate__fadeInRight">
                        <div class="col-9 px-0">
                            <h5 class="text-right font-weight-bold pr-3">No more extra chargers for same delivery</h5>
                        </div>
                        <div class="col-3 px-0">
                            <div class="feature-icon bg-white rounded p-3 ml-md-0 d-flex align-items-center" style="height:70px; width: 80px">
                                <img class="img-fluid" src="{{url('assets/img/feature_6.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- How it Works -->
<div class="container-fluid py-5 my-5">
        <h1 class="text-center pt-md-5 mt-md-5 font-weight-bold">How it Works?</h1>
        <section class="center slider mt-5">
            <div>
                <img src="{{url('assets/img/hit_1.png')}}">
                <h5 class="text-center font-weight-bold my-4">SHIPX Signin</h5>
                <p class="text-center text-muted mb-4 px-md-3">Alerts and notifications on picking up shipments, delivery, driver arrival and signatures</p>
            </div>
            <div>
                <img src="{{url('assets/img/hit_3.png')}}">
                <h5 class="text-center font-weight-bold my-4">Track Driver</h5>
                <p class="text-center text-muted mb-4 px-md-3">Real-time navigation and shipping tracking</p>
            </div>
            <div>
                <img src="{{url('assets/img/hit_2.png')}}">
                <h5 class="text-center font-weight-bold my-4">Choose Car</h5>
                <p class="text-center text-muted mb-4 px-md-3">Choose the car fit your needs and budget</p>
            </div>
            <div>
                <img src="{{url('assets/img/hit_1.png')}}">
                <h5 class="text-center font-weight-bold my-4">SHIPX Signin</h5>
                <p class="text-center text-muted mb-4 px-md-3">Alerts and notifications on picking up shipments, delivery, driver arrival and signatures</p>
            </div>
            <div>
                <img src="{{url('assets/img/hit_3.png')}}">
                <h5 class="text-center font-weight-bold my-4">Track Driver</h5>
                <p class="text-center text-muted mb-4 px-md-3">Real-time navigation and shipping tracking</p>
            </div>
            <div>
                <img src="{{url('assets/img/hit_2.png')}}">
                <h5 class="text-center font-weight-bold my-4">Choose Car</h5>
                <p class="text-center text-muted mb-4 px-md-3">Choose the car fit your needs and budget</p>
            </div>
        </section>

</div>

<!-- Gallery -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 px-0 position-relative">
            <img src="{{url('assets/img/gallery_1.png')}}" style="width:100%; height: 100%; object-fit: cover;">
            <h4 class="text-white text-left font-weight-bold" style="
                position: absolute;
                left: 10px;
                bottom: 15px;
            ">Person wearing mask for delivery</h4>
        </div>
        <div class="col-md-8 px-0">
            <div class="row mx-0">
                <div class="col-md-3 px-0 position-relative">
                    <img src="{{url('assets/img/gallery_2.png')}}" style="width:100%; height: 100%; object-fit: cover;">
                    <h5 class="text-white text-left font-weight-bold" style="
                        position: absolute;
                        left: 10px;
                        bottom: 15px;
                    ">Auto parts</h5>
                </div>
                <div class="col-md-3 px-0 position-relative">
                    <img src="{{url('assets/img/gallery_4.png')}}" style="width:100%; height: 100%; object-fit: cover;">
                    <h5 class="text-white text-left font-weight-bold" style="
                        position: absolute;
                        left: 10px;
                        bottom: 15px;
                    ">RX medicine image</h5>
                </div>
                <div class="col-md-3 px-0 position-relative">
                    <img src="{{url('assets/img/gallery_6.png')}}" style="width:100%; height: 100%; object-fit: cover;">
                    <h5 class="text-white text-left font-weight-bold" style="
                        position: absolute;
                        left: 10px;
                        bottom: 15px;
                    ">Blood Samples</h5>
                </div>
                <div class="col-md-3 px-0 position-relative">
                    <img src="{{url('assets/img/gallery_7.png')}}" style="width:100%; height: 100%; object-fit: cover;">
                    <h5 class="text-white text-left font-weight-bold" style="
                        position: absolute;
                        left: 10px;
                        bottom: 15px;
                    ">Legal Documents</h5>
                </div>
            </div>
            <div class="row mx-0">
                <div class="col-md-3 px-0 position-relative">
                    <img src="{{url('assets/img/gallery_3.png')}}" style="width:100%; height: 100%; object-fit: cover;">
                    <h5 class="text-white text-left font-weight-bold" style="
                        position: absolute;
                        left: 10px;
                        bottom: 15px;
                    ">Medical <br /> Clinical Supply</h5>
                </div>
                <div class="col-md-3 px-0 position-relative">
                    <img src="{{url('assets/img/gallery_5.png')}}" style="width:100%; height: 100%; object-fit: cover;">
                    <h5 class="text-white text-left font-weight-bold" style="
                        position: absolute;
                        left: 10px;
                        bottom: 15px;
                    ">Blood Samples</h5>
                </div>
                <div class="col-md-6 px-0 position-relative">
                    <img src="{{url('assets/img/gallery_8.png')}}" style="width:100%; height: 100%; object-fit: cover;">
                    <h5 class="text-white text-left font-weight-bold" style="
                        position: absolute;
                        left: 10px;
                        bottom: 15px;
                    ">Blood Samples</h5>
                </div>
            </div>
        </div>
        <div class="col-md-2 px-0 position-relative">
            <img src="{{url('assets/img/gallery_9.png')}}" style="width:100%; height: 100%; object-fit: cover;">
            <h4 class="text-white text-left font-weight-bold" style="
                position: absolute;
                left: 10px;
                bottom: 15px;
            ">Signature on delivery</h4>
        </div>
    </div>
</div>
<!-- Subscribe Form -->
@include('common.newsletter')

<!-- footer -->
@include('includes.driver_footer')