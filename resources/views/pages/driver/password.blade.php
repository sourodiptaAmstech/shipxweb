<!-- header -->
@include('includes.driver_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.driver_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <form class="" id="changePasswordForm">
                    <div class="px-md-5 mx-md-5">
                      <div class="px-3 upper pt-5">
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success changsuc_alert" style="display: none;">
                            <p class="text-success" id="changsuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">Change Password</h2>
                        <div class="form-group">
                            {{-- <label for="old_password" class="font-weight-bold">Enter Old Password</label> --}}
                            <input type="password" name="old_password" id="old_password" class="form-control inset-input" placeholder="Old Password" onkeyup="return validate()">
                            <span class="text-danger" id="oldpass"></span>
                        </div>
                        <div class="form-group">
                            {{-- <label for="password" class="font-weight-bold">Enter New Password</label> --}}
                            <input type="password" name="password" id="password" class="form-control inset-input" placeholder="New Password" onkeyup="return validate()">
                            <span class="text-danger" id="passw"></span>
                        </div>
                        <div class="form-group">
                            {{-- <label for="password_confirmation" class="font-weight-bold">Confirm New Password</label> --}}
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control inset-input" placeholder="Confirm Password" onkeyup="return validate()">
                            <span class="text-danger" id="passwc"></span>
                        </div>

                        <div class="d-flex justify-content-start mb-3 mt-5">
                            <button type="submit" class="btn btn-success-theme btn-lrg grad text-white mb-5">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
      function validate(){
        var status=null;

        var old_password = document.getElementById('old_password').value;
        if (old_password == '') {
            document.getElementById("oldpass").innerHTML='The old password field is required.'
            document.getElementById('old_password').classList.add('has-error')
            status = false
        } else if (old_password.length<6||old_password.length>255) {
            document.getElementById("oldpass").innerHTML='The old password must be between 6 and 255 characters.'
            document.getElementById('old_password').classList.add('has-error')
            status=false
        } else {
            $('.upper').addClass('pt-5');
            $('.dupdate_alert').hide();
            $('#dupdate-msg').html('');
            $('.changsuc_alert').hide();
            $('#changsuc-msg').html('');
            document.getElementById("oldpass").innerHTML='';
            document.getElementById('old_password').classList.remove('has-error')
            status=true;
        }

        var password = document.getElementById('password').value
        if (password == '') {
            document.getElementById("passw").innerHTML='The password field is required.'
            document.getElementById('password').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("passw").innerHTML='The password must be between 6 and 255 characters.'
            document.getElementById('password').classList.add('has-error')
            status=false

        } else {
            document.getElementById("passw").innerHTML=''
            document.getElementById('password').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('password_confirmation').value
        if (password_confirm == '') {
            document.getElementById("passwc").innerHTML='The password confirmation field is required.'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("passwc").innerHTML='The password confirmation must be between 6 and 255 characters.'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("passwc").innerHTML=''
            document.getElementById('password_confirmation').classList.remove('has-error')
        }
        
        return status
    }

    $(document).ready(function(){
        $('#changePasswordForm').on('submit', function(event){
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            var old_password = $('#old_password').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            $(".overlay").show();
            $.ajax({
                url: "{{ route('driver.change.password') }}",
                method:"POST",
                data:{ old_password: old_password, password: password, password_confirmation: password_confirmation, timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.changsuc_alert').show();
                    $('#changsuc-msg').html(response.message);
                    $('#old_password').val('');
                    $('#password').val('');
                    $('#password_confirmation').val('');
                },
                error: function(response){
                    $(".overlay").hide();
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('old_password')) {
                            $('#oldpass').html(responseMsg.errors.old_password).promise().done(function(){
                                $('#old_password').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('password')) {
                            $('#passw').html(responseMsg.errors.password).promise().done(function(){
                                $('#password').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                            $('#passwc').html(responseMsg.errors.password_confirmation).promise().done(function(){
                                $('#password_confirmation').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('exception')) {
                            $('.upper').removeClass('pt-5');
                            $('.upper').addClass('pt-4');
                            $('.dupdate_alert').show();
                            $('#dupdate-msg').html(responseMsg.message);
                        } 
                    }
                    if (response.status == 403){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.upper').removeClass('pt-5');
                        $('.upper').addClass('pt-4');
                        $('.dupdate_alert').show();
                        $('#dupdate-msg').html(responseMsg.message);
                    }
                    if (response.status == 400){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.upper').removeClass('pt-5');
                        $('.upper').addClass('pt-4');
                        $('.dupdate_alert').show();
                        $('#dupdate-msg').html(responseMsg.message);
                    }
                    if (response.status == 500){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.upper').removeClass('pt-5');
                        $('.upper').addClass('pt-4');
                        $('.dupdate_alert').show();
                        $('#dupdate-msg').html(responseMsg.message);
                    }
                }
            });
        });
    });
</script>
<!-- footer -->
@include('includes.driver_footer')