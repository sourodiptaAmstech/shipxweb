
@include('includes.driver_header')
<link rel="stylesheet" href="{{URL::asset('/')}}assets/css/signature-pad.css" crossorigin="anonymous">
<div class="container my-5">
    <div id="map" style="position: relative; overflow: hidden; width: 100%;height: 500px;">

    </div>
    <div class="container-fluid" id="requestContainer">
        <!-- A fluid container that uses the full witdh -->
    </div>

<div class="offBody container-fluid" style="display:none" id="signatureBlock" onselectstart="return false">


  <div id="signature-pad" class="signature-pad">
    <div class="signature-pad--body" style="width: 100%">
      <canvas id="signaCanvas"></canvas>
    </div>
    <div class="signature-pad--footer">
      <div class="description">Sign above</div>

      <div class="signature-pad--actions">
        <div class="row">
          <div class="col-lg-4">
          <button type="button" style="width: 143px;height: 40px;" class="mb-3 button clear btn btn-success-theme btn-lrg text-white" id="clearSignature" data-action="clear">Clear</button>
          </div>
          <div class="col-lg-4">
          <button type="button" style="width: 143px;height: 40px;" class="mb-3 button clear btn btn-success-theme btn-lrg text-white" id="cancelSignature">Cancel</button>
          </div>
          <div class="col-lg-4">
          <button type="button" style="width: 143px;height: 40px;" class="mb-3 button save btn btn-success-theme btn-lrg text-white" id="saveSignature">Save</button>
          </div>
          {{-- <button  type="button" class="button" data-action="change-color">Change color</button>
          <button type="button" class="button" data-action="undo">Undo</button> --}}
        {{--   <button type="button" class="button save" data-action="save-png">Save as PNG</button>
          <button type="button" class="button save" data-action="save-jpg">Save as JPG</button>
          <button type="button" class="button save" data-action="save-svg">Save as SVG</button> --}}
        </div>
      </div>
    </div>
  </div>
</div>
<!-- The Modal -->
<div id="myModal" class="mymodal">

    <!-- Modal content -->
    <div id="modalContent" class="modalContent">
      <span class="close">&times;</span>
      <p>Some text in the Modal..</p>
    </div>

  </div>

<script>
    var isRecive=true;

</script>
  <script src="{{URL::asset('/')}}assets/js/signature_pad.umd.js" ></script>
  <script src="{{URL::asset('/')}}assets/js/app.js" ></script>
  <script>


    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];


    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it

    </script>
<style>


    /* The Modal (background) */
    .mymodal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modalContent {
      background-color: #fefefe;
      margin: auto;
      padding: 20px;
      border: 1px solid #888;
      width: 80%;
    }

    /* The Close Button */
    .close {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
    </style>
<!-- footer -->
@include('includes.driver_footer')
