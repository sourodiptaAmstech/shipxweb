<!-- header -->
@include('includes.driver_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.driver_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <div class="px-md-5 mx-md-5">
                    <div class="px-3 upper pt-5">
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <h2 class="font-weight-bold mb-0">Trip History</h2>
                        <div class="container my-2" id="tripHistory">  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.trip.history') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                var options = '';
                if(response.data.length==0){
                    options=`<h6 style="font-size: x-large;" class="mt-5 text-muted">You did not apply for any trip yet.</h6>`;
                    $('#tripHistory').addClass('px-0');
                }
                $.each(response.data, function(key, value) {
                    options += `
    <div class="row shadow-lg bg-white" style="border: 1px solid lightgrey;">

        <div class="col-lg-12 px-0 d-flex">
            <img src="`+value.static_map+`" alt="" style="width: 100%;height: auto;">
        </div>

        <div class="col-lg-12 px-5 py-3">
            <div class="row mb-2">
                <div class="col-sm-6 mx-0 px-0">
                    <h5 class="font-weight-bold text-muted">
                        Booking ID : <span>`+value.booking_id+`</span>
                    </h5>
                </div>
                <div class="col-sm-6 mx-0 px-0">
                    <h5 class="text-muted textalign">
                        <span style="color: orange;">$`+value.payment.total+`</span>
                    </h5>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-6 mx-0 px-0">
                    <h6 class="text-muted">
                        `+moment(value.assigned_at).format('MMM Do YYYY, h:mm A')+`
                    </h6>
                </div>
                <div class="col-sm-6 mx-0 px-0">
                    <h6 class="text-muted textalign">
                        <a href="{{ route('driver.trip-details') }}?id=`+value.id+`" class="btn btn-sm btn-success-theme text-white trip-detail">Details</a>
                    </h6>
                </div>
            </div>
        </div>

    </div><br>
                    `;
                });
                $('#tripHistory').append(options);
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.dupdate_alert').show();
                    $('#dupdate-msg').html(responseMsg.message);
                }
            }
        });
    });
    
</script>

<!-- footer -->
@include('includes.driver_footer')