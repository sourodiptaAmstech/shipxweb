<!-- header -->
@include('includes.driver_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.driver_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <div class="px-md-5 mx-md-5">
                    <div class="px-3 upper pt-5">
                        <div class="alert alert-danger update_alert" style="display: none;">
                            <p class="text-danger" id="update-msg"></p>
                        </div>
                        <h2 class="font-weight-bold mb-0">Scheduled Trips</h2>
                        <div class="container my-2" id="tripHistory">  
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.booking').addClass('btn-primary');
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.upcoming.scheduleTrip') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                var options = '';
                if(response.data.length==0){
                    options=`<h6 style="font-size: x-large;" class="mt-5 text-muted">You have not scheduled any trip yet.</h6>`;
                    $('#tripHistory').addClass('px-0');
                }
                $.each(response.data, function(key, value) {
                    options += `
    <div class="row shadow-lg bg-white" style="border: 1px solid lightgrey;">

        <div class="col-lg-12 px-0 d-flex">
            <img src="`+value.static_map+`" alt="" style="width: 100%;height: auto;">
        </div>

        <div class="col-lg-12 px-5 py-2">

                <div class="row mb-2">
                    <div class="col-sm-6 mx-0 px-0">
                        <h5 class="font-weight-bold text-muted">
                           Booking ID : 
                        </h5>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h5 class="text-muted">`+value.booking_id+`</h5>
                    </div>
                </div>
               
                <div class="row mb-2">
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="font-weight-bold text-muted">
                            Date: 
                        </h6>
                    </div>
                    <div class="col-sm-6 mx-0 px-0">
                        <h6 class="text-muted">`+moment(value.assigned_at).format('MMM Do YYYY, h:mm A')+`</h6>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-sm-12 mx-0 px-0 text-center">
                        <div class="service-image position-relative my-1 d-flex justify-content-center align-items-center img-fluid">
                            <img src="`+value.service_type.image+`">
                        </div>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-sm-12 mx-0 px-0 text-center">
                        <h6 class="text-muted">
                            `+value.service_type.name+`
                        </h6>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-sm-12 mx-0 px-0 text-center">
                        <h5 class="font-weight-bold text-muted">
                            <a style="width:16%;cursor: pointer;" href="javascript:void(0)" class="btn btn-sm btn-danger cancelSchedule" key="`+value.id+`" id="cancelSchedule">Cancel Trip</a>
                        </h5>
                        <h5 class="font-weight-bold text-muted">
                            <a href="{{ route('driver.upcoming-trip.details') }}?id=`+value.id+`" class="btn btn-sm btn-info trip-detail">Details</a>
                        </h5>
                    </div>
                </div>
        </div>
    </div><br>
                    `;
                });
                $('#tripHistory').append(options);

                $('.cancelSchedule').on('click',function(){
                    var request_id = $(this).attr('key');
                    t = new Date().toUTCString().split(' ');
                    var timeZone = t[t.length-1] + moment().format('Z');
                    bootbox.confirm('Are you sure to cancel scheduled trip?', function (res) {
                        if (res){
                            $(".overlay").show();
                            $.ajax({
                                url: "{{ route('passenger.cancel.schedule') }}",
                                method:"POST",
                                data:{ request_id: request_id, timeZone: timeZone },
                                success: function(response){
                                    location.reload();
                                    $(".overlay").hide();
                                },
                                error: function(response){
                                    $(".overlay").hide();
                                }
                            });
                        }
                    });
                });
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                } 
            }
        });
    });
    
</script>

<!-- footer -->
@include('includes.driver_footer')