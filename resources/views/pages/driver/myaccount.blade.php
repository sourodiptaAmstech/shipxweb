<!-- header -->
@include('includes.driver_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.driver_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <form class="px-md-5 mx-md-5" id="driverProfileUpdateFrom">
                    <div class="px-3 upper pt-5">
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success dupdatesuc_alert" style="display: none;">
                            <p class="text-success" id="dupdatesuc-msg"></p>
                        </div>

                        <div class="row justify-content-center mb-5 mx-0 below pt-5">
                            <div style="height: 157px;width: 157px;background-color: #ddd;border: 9px solid #f18933;border-radius: 50%; cursor: pointer;"
                            class="m-0 profile-picture" data-toggle="tooltip" title="Edit Your Image">
                                <img style="object-fit: cover;height: 140px; width: 140px; background-color: #ddd; border-radius: 50%"
                                src="{{url('assets/img/profile-user.png')}}" alt="" id="image">
                                <span class="edit-profile bg-white rounded-circle d-inline-flex justify-content-center align-items-center position-absolute" data-placement="bottom">
                                    <i class="fas fa-edit"></i>
                                </span>
                            </div>
                        </div>
                        <div style="display: none;">
                            <input type="hidden" name="timeZone" id="timeZone">
                            <input type="file" name="profile_image" id="profile_image">
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" name="first_name" id="first_name" class="form-control inset-input pl-4" placeholder="First Name" onkeyup="return validate()">
                            <span class="text-danger" id="fname"></span>
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" name="last_name" id="last_name" class="form-control inset-input pl-4" placeholder="Last Name" onkeyup="return validate()">
                            <span class="text-danger" id="lname"></span>
                        </div>

                        <div class="form-group mb-4">
                            {{-- style="background: #fff url('assets/img/edit.png') center right 25px no-repeat;" --}}
                            <input style="background: #fff" type="email" id="email" name="email" class="form-control inset-input pl-4" placeholder="Email Address" readonly>
                        </div>
                        {{-- style="background: #fff url('assets/img/edit.png') center right 25px no-repeat;" --}}
                        <div class="form-group mb-4">
                            <input type="text" name="mobile_no" id="mobile_no" class="form-control inset-input pl-4" placeholder="Mobile Number">
                            <span class="text-danger" id="mobileno"></span>
                        </div>

                        <div class="form-group mb-4">
                            <select class="form-control inset-input select-box pl-4" name="service_type" id="service_type" onchange="return validate()">

                            </select>
                            <span class="text-danger" id="service"></span>
                        </div>

                        <div class="form-group mb-4">
                            <input type="text" name="vehicle_make" id="vehicle_make" value="{{ old('vehicle_make') }}" class="form-control inset-input pl-4" placeholder="Vehicle Make" onkeyup="return validate()">
                            <span class="text-danger" id="serviceno"></span>
                        </div>

                        <div class="form-group mb-4">
                            <input type="text" name="vehicle_model" id="vehicle_model" class="form-control inset-input pl-4" placeholder="Vehicle Model" onkeyup="return validate()">
                            <span class="text-danger" id="model"></span>
                        </div>

                        <div class="form-group mb-4">
                            <input type="number" name="vehicle_year" id="vehicle_year" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" class="form-control inset-input pl-4" placeholder="Vehicle Year" onkeyup="return validate()">
                            <span class="text-danger" id="year"></span>
                        </div>

                        <a href="{{ route('driver.document') }}" class="btn btn-success-theme btn-lg text-white">Document Upload</a>

                        {{-- <a href="javascript:void(0)" class="btn btn-success-theme btn-lg text-white">Bank Details</a> --}}

                        <div class="d-flex justify-content-center my-5">
                            <button type="submit" class="btn btn-success-theme btn-lrg text-white mb-5">Save</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    $(".overlay").show();
    $.ajax({
        url: "{{ route('service.list') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            $('#service_type').empty();
            var options = '<option value="">Choose Service Type</option>';
            $.each(response.data, function(key, value) {
              options += '<option value="'+value.id+'">'+value.name+'</option>';
            });
            $('#service_type').append(options);
            $(".overlay").hide();
        },
        error: function(response){
            $(".overlay").hide();
            if (response.status == 400){
                var responseMsg = $.parseJSON(response.responseText);
                $('.upper').removeClass('pt-5');
                $('.upper').addClass('pt-4');
                $('.below').removeClass('pt-5');
                $('.below').addClass('pt-1');
                $('.dupdate_alert').show();
                $('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        $('#service_type').addClass('has-error');
                    });
            }
            if (response.status == 500){
                var responseMsg = $.parseJSON(response.responseText);
                $('.upper').removeClass('pt-5');
                $('.upper').addClass('pt-4');
                $('.below').removeClass('pt-5');
                $('.below').addClass('pt-1');
                $('.dupdate_alert').show();
                $('#dupdate-msg').html(responseMsg.message).promise().done(function(){
                        $('#service_type').addClass('has-error');
                    });
            }
        }
    });

    driverProfileList();

function driverProfileList(){

    $(".overlay").show();
    $.ajax({
        url: "{{ route('driver.profile.list') }}",
        method:"GET",
        data:{ timeZone: timeZone },
        success: function(response){
            $(".overlay").hide();
            $('#first_name').val(response.data.first_name);
            $('#last_name').val(response.data.last_name);
            $('#mobile_no').val(response.data.mobile);
            $('#email').val(response.data.email);

            if (response.data.service.length!=0) {
                $('#service_type').val(response.data.service[0].service_type_id);
            }
            if (response.data.provider!=null) {
                $('#vehicle_make').val(response.data.provider.service_number);
                $('#vehicle_model').val(response.data.provider.service_model);
                $('#vehicle_year').val(response.data.provider.vehicle_year);
            }

            var picture = response.data.avatar;
            if(picture!=null||picture!=''){
                var isValid = picture.includes("http");
                if (isValid) {
                    $("#image").attr({ "src": picture });
                } else {
                    $("#image").attr({ "src": "https://app.shipxnow.com/storage/"+picture });
                   // $("#image").attr({ "src": "https://demos.mydevfactory.com/debarati/shipx/storage/"+picture });
                }
            }
        },
        error: function(response){
            console.log(response);
            $(".overlay").hide();
            var responseMsg = $.parseJSON(response.responseText);
            $('.upper').removeClass('pt-5');
            $('.upper').addClass('pt-4');
            $('.below').removeClass('pt-5');
            $('.below').addClass('pt-1');
            $('.update_alert').show();
            $('#update-msg').html(responseMsg.message);
        }
    });

}

    $('#timeZone').val(timeZone);
    $(".profile-picture").click(function(e) {
        $("#profile_image").click();
    });
    function fasterPreview(uploader) {
        if ( uploader.files && uploader.files[0] ){
        $('#image').attr('src',
        window.URL.createObjectURL(uploader.files[0]) );
        }
    }
    $('input:file').change(function() {
        fasterPreview(this);
        $('.upper').removeClass('pt-4');
        $('.upper').addClass('pt-5');
        $('.below').removeClass('pt-1');
        $('.below').addClass('pt-5');
        $('.dupdate_alert').hide();
        $('#dupdate-msg').html('');
        $('.dupdatesuc_alert').hide();
        $('#dupdatesuc-msg').html('');
    });


    function validate(){
        var status=null;

        var fname = document.getElementById('first_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='The first name field is required.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='The first name must be at least 3 characters.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='The first name may not be greater than 255 characters.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else {
            $('.upper').removeClass('pt-4');
            $('.upper').addClass('pt-5');
            $('.below').removeClass('pt-1');
            $('.below').addClass('pt-5');
            $('.dupdate_alert').hide();
            $('#dupdate-msg').html('');
            $('.dupdatesuc_alert').hide();
            $('#dupdatesuc-msg').html('');
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_name').classList.remove('has-error')
            status=true
        }

        var lname = document.getElementById('last_name').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='The last name field is required.'
            document.getElementById('last_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("lname").innerHTML='The last name must be at least 3 characters.'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("lname").innerHTML='The last name may not be greater than 255 characters.'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('last_name').classList.remove('has-error')
        }

        var serviceType = document.getElementById('service_type').value
        if (serviceType == '') {
            document.getElementById("service").innerHTML='The service type field is required.'
            document.getElementById('service_type').classList.add('has-error')
            status=false
        } else {
            document.getElementById("service").innerHTML=''
            document.getElementById('service_type').classList.remove('has-error')
        }

        var mobileno = document.getElementById('mobile_no').value
        if (mobileno == '') {
            document.getElementById("mobileno").innerHTML='The mobile no field is required.'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        }else {
            document.getElementById("mobileno").innerHTML=''
            document.getElementById('mobile_no').classList.remove('has-error')
        }

        var carModel = document.getElementById('vehicle_model').value
        if (carModel == '') {
            document.getElementById("model").innerHTML='The vehicle model field is required.'
            document.getElementById('vehicle_model').classList.add('has-error')
            status=false
        } else {
            document.getElementById("model").innerHTML=''
            document.getElementById('vehicle_model').classList.remove('has-error')
        }

        var modelYear = document.getElementById('vehicle_year').value
        if (modelYear == '') {
            document.getElementById("year").innerHTML='The vehicle year field is required.'
            document.getElementById('vehicle_year').classList.add('has-error')
            status=false
        } else {
            document.getElementById("year").innerHTML=''
            document.getElementById('vehicle_year').classList.remove('has-error')
        }

        var carNumber = document.getElementById('vehicle_make').value
        if (carNumber == '') {
            document.getElementById("serviceno").innerHTML='The vehicle make field is required.'
            document.getElementById('vehicle_make').classList.add('has-error')
            status=false
        } else {
            document.getElementById("serviceno").innerHTML=''
            document.getElementById('vehicle_make').classList.remove('has-error')
        }

        return status
    }

$(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    //SUBMIT DRIVER FORM.................................
    $('#driverProfileUpdateFrom').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.profile.update') }}",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                $(".overlay").hide();
                $('.upper').removeClass('pt-5');
                $('.upper').addClass('pt-4');
                $('.below').removeClass('pt-5');
                $('.below').addClass('pt-1');
                $('.dupdatesuc_alert').show();
                $('#dupdatesuc-msg').html(response.message);
            },

            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        $('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                            $('#first_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        $('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                            $('#last_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        $('#mobileno').html(responseMsg.errors.mobile_no).promise().done(function(){
                            $('#mobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('service_type')) {
                        $('#service').html(responseMsg.errors.service_type).promise().done(function(){
                            $('#service_type').addClass('has-error');
                        });
                    }

                    if (responseMsg.errors.hasOwnProperty('vehicle_model')) {
                        $('#model').html(responseMsg.errors.vehicle_model).promise().done(function(){
                            $('#vehicle_model').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('vehicle_year')) {
                        $('#year').html(responseMsg.errors.vehicle_year).promise().done(function(){
                            $('#vehicle_year').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('vehicle_make')) {
                        $('#serviceno').html(responseMsg.errors.vehicle_make).promise().done(function(){
                            $('#vehicle_make').addClass('has-error');
                        });
                    }

                    if (responseMsg.errors.hasOwnProperty('profile_image')) {
                        $('.upper').removeClass('pt-5');
                        $('.upper').addClass('pt-4');
                        $('.below').removeClass('pt-5');
                        $('.below').addClass('pt-1');
                        $('.dupdate_alert').show();
                        $('#dupdate-msg').html(responseMsg.errors.profile_image);
                    }
                    // if (responseMsg.errors.hasOwnProperty('exception')) {
                    //     $('#emailadd').html(responseMsg.message).promise().done(function(){
                    //         $('#email_id').addClass('has-error');
                    //     });
                    // }
                }
                if (response.status == 403){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.below').removeClass('pt-5');
                    $('.below').addClass('pt-1');
                    $('.dupdate_alert').show();
                    $('#dupdate-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.below').removeClass('pt-5');
                    $('.below').addClass('pt-1');
                    $('.dupdate_alert').show();
                    $('#dupdate-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT DRIVER FORM.................................
});

</script>
<!-- footer -->
@include('includes.driver_footer')
