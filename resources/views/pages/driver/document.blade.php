<!-- header -->
@include('includes.driver_header')

<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.driver_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <div class="px-md-5 mx-md-5">
                    <div class="px-3 pt-5">
                        
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success changsuc_alert" style="display: none;">
                            <p class="text-success" id="changsuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">Document Upload</h2>
                        <div class="alert alert-warning document_alert" style="display: none;">
                            <p style="color: red;font-size: 20px;">Please upload required documents to qualify as a driver. Qualification required to offer your ride service.</p>
                        </div>
                        <div class="alert alert-success document_success" style="display: none;">
                            <p style="color:#3a8418;font-size: 20px;">All the required documents are uploaded. You are qualified as a driver.</p>
                        </div>
                        <h4 class="font-weight-bold1 text-info mb-1">Driver Documents</h4> 
                        <table class="table table-bordered">
                            <tbody class="driver_doc">
                                
                            </tbody>
                        </table>

                        <h4 class="font-weight-bold1 text-info mb-1">Vehicle Documents</h4> 
                        <table class="table table-bordered">
                            <tbody class="vehicle_doc">
                                
                            </tbody>
                        </table>

                        {{-- <h4 class="font-weight-bold1 text-info mb-1">Vehicle Image</h4> 
                        <table class="table table-bordered">
                            <tbody class="vehicle_img">
                                
                            </tbody>
                        </table> --}}
               
                        <form id="updatesocialno">
                            <div class="form-group mb-4 mt-4">
                                <input type="text" name="vehicle_plate_number" id="vehicle_plate_number" class="form-control inset-input pl-4" placeholder="Enter Vehicale Plate Number" onkeyup="return validate()">
                                <span class="text-danger" id="plateno"></span>
                            </div>
                            <div class="form-group mb-4">
                                <!-- <label for="dateOfJournyd" class="font-weight-bold1">Choose a Date</label> -->
                                <input type="text" class="form-control inset-input pl-4" name="vehicle_expiration_date" id="vehicle_expiration_date" placeholder="Expiration Date" onblur="return validate()">
                                <span class="text-danger" id="dated"></span>
                            </div>
                            <div class="form-group mb-4">
                                <select class="form-control inset-input select-box pl-4" name="vehicle_color" id="vehicle_color" onchange="return validate()">
                                    <option value="">Choose color</option>
                                    <option value="Beige">Beige</option>
                                    <option value="Black">Black</option>
                                    <option value="Blue">Blue</option>
                                    <option value="Brown">Brown</option>
                                    <option value="Gold">Gold</option>
                                    <option value="Gray">Gray</option>
                                    <option value="Green">Green</option>
                                    <option value="Orange">Orange</option>
                                    <option value="Pink">Pink</option>
                                    <option value="Purple">Purple</option>
                                    <option value="Red">Red</option>
                                    <option value="Silver">Silver</option>
                                    <option value="White">White</option>
                                    <option value="Yellow">Yellow</option>
                                    <option value="Other">Other</option>
                                </select>
                                <span class="text-danger" id="color"></span>
                            </div>
                            <div class="form-group mb-4">
                                <input type="text" name="social_security_number" id="social_security_number" class="form-control inset-input pl-4" placeholder="Social Security Number" onkeyup="return validate()">
                                <span class="text-danger" id="ssno"></span>
                            </div>

                            <div class="d-flex justify-content-center my-5">
                                <button type="submit" class="btn btn-success-theme btn-lg text-white mb-5">Save</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
t = new Date().toUTCString().split(' ');
var timeZone = t[t.length-1] + moment().format('Z');

documentList();

function documentList(){
    $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.document.list') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                $('.driver_doc').empty();
                var optiond = '';
                $.each(response.data.DriverDocuments, function(key, value) {
                    if (value.provider_document_status!=null) {
                        var status = value.provider_document_status;
                    } else {
                        var status = 'Missing';
                    }
                  optiond += '<tr><td>'+value.name+'</td><td>'+status+'</td><td><div style="display: none;"><form id="'+value.id+'formdata_f" method="POST" enctype="multipart/form-data"><input type="hidden" name="document_id" id="document_id" value="'+value.id+'"><input type="file" name="file" id="'+value.id+'file"><input type="hidden" name="timeZone_f" class="timeZone_f"><button type="submit" id="'+value.id+'submit_f"></button></form></div><button class="btn btn-info" id="'+value.id+'doc_button" onclick="uploadDocument('+value.id+')">Upload</button></td></tr>';
                });
                $('.driver_doc').append(optiond);


                $('.vehicle_doc').empty();
                var optionv = '';
                $.each(response.data.VehicleDocuments, function(key, value) {
                    if (value.provider_document_status!=null) {
                        var status = value.provider_document_status;
                    } else {
                        var status = 'Missing';
                    }
                  optionv += '<tr><td>'+value.name+'</td><td>'+status+'</td><td><div style="display: none;"><form id="'+value.id+'formdata_f" method="POST" enctype="multipart/form-data"><input type="hidden" name="document_id" id="document_id" value="'+value.id+'"><input type="file" name="file" id="'+value.id+'file"><input type="hidden" name="timeZone_f" class="timeZone_f"><button type="submit" id="'+value.id+'submit_f"></button></form></div><button class="btn btn-info" id="'+value.id+'doc_button" onclick="uploadDocument('+value.id+')">Upload</button></td></tr>';
                });
                $('.vehicle_doc').append(optionv);


                // $('.vehicle_img').empty();
                // var optioni = '';
                // $.each(response.data.VehicleImage, function(key, value) {
                //     if (value.provider_document_status!=null) {
                //         var status = value.provider_document_status;
                //     } else {
                //         var status = 'Missing';
                //     }
                //   optioni += '<tr><td>'+value.name+'</td><td>'+status+'</td><td><div style="display: none;"><form id="'+value.id+'formdata_f" method="POST" enctype="multipart/form-data"><input type="hidden" name="document_id" id="document_id" value="'+value.id+'"><input type="file" name="file" id="'+value.id+'file"><input type="hidden" name="timeZone_f" class="timeZone_f"><button type="submit" id="'+value.id+'submit_f"></button></form></div><button class="btn btn-info" id="'+value.id+'doc_button" onclick="uploadDocument('+value.id+')">Upload</button></td></tr>';
                // });
                // $('.vehicle_img').append(optioni);
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                var responseMsg = $.parseJSON(response.responseText);
                $('.px-3').removeClass('pt-5');
                $('.px-3').addClass('pt-4');
                $('.dupdate_alert').show();
                $('#dupdate-msg').html(responseMsg.message);
            }
        });
    }

    $(document).ajaxComplete(function(event,xhr,settings) {
        if(settings.url == "{{ route('driver.document.list') }}?timeZone=GMT%2B05%3A30")
        {
            $('.timeZone_f').val(timeZone);  
        }
    });

    function uploadDocument(id){
        $(document).on('click','#'+id+'doc_button',function(e) {
        // $('#'+id+'doc_button').click(function(e) {
            $("#"+id+"file").click();

            $('.px-3').removeClass('pt-4');
            $('.px-3').addClass('pt-5');
            $('.dupdate_alert').hide();
            $('#dupdate-msg').html('');
            $('.changsuc_alert').hide();
            $('#changsuc-msg').html('');
        });

        $(document).on('change','#'+id+'file',function() {
        // $('#'+id+'file').change(function() {
            $("#"+id+"submit_f").click();
        });

        $(document).on('submit','#'+id+'formdata_f', function(event){
        // $('#'+id+'formdata_f').on('submit', function(event){ 
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".overlay").show();
            $.ajax({
                url: "{{ route('driver.document.upload') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response){
                    $(".overlay").hide();
                    $('#bootboxModal').modal('show');
                    $('.bootboxBody').text('Document uploaded successfully.');
                    $('button.bootboxBtn').on('click', function(){
                        $("#bootboxModal").modal('hide');
                        //location.reload();
                        documentList();
                    });
                },
                error: function(response){
                    $(".overlay").hide();
                    if (response.status == 422) {
                        var responseMsg = $.parseJSON(response.responseText);
                        console.log(responseMsg)
                        $('.px-3').removeClass('pt-5');
                        $('.px-3').addClass('pt-4');
                        $('.dupdate_alert').show();
                        $('#dupdate-msg').html(responseMsg.errors.file);
                    }
                }
            });
        });
    }


    function driverProfileList(){
        $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.profile.list') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                $('#social_security_number').val(response.data.social_security_number);
                $('#vehicle_plate_number').val(response.data.service[0].vehicale_number_plate_no);
                $('#vehicle_color').val(response.data.service[0].vehicale_color);
                $('#vehicle_expiration_date').val(moment(response.data.service[0].number_plate_no_exprydate).format('YYYY-MM-DD'));

            },
            error: function(response){
                console.log(response);
                $(".overlay").hide();
                var responseMsg = $.parseJSON(response.responseText);
                $('.px-3').removeClass('pt-5');
                $('.px-3').addClass('pt-4');
                $('.dupdate_alert').show();
                $('#dupdate-msg').html(responseMsg.message);
            }
        });
    }
    driverProfileList();


    function validate(){
        var status=null;
        var plateNumber = document.getElementById('vehicle_plate_number').value
        if (plateNumber == '') {
            document.getElementById("plateno").innerHTML='The vehicle plate number field is required.'
            document.getElementById('vehicle_plate_number').classList.add('has-error')
            status=false
        } else {
            $('.px-3').removeClass('pt-4');
            $('.px-3').addClass('pt-5');
            $('.dupdate_alert').hide();
            $('#dupdate-msg').html('');
            $('.changsuc_alert').hide();
            $('#changsuc-msg').html('');
            document.getElementById("plateno").innerHTML=''
            document.getElementById('vehicle_plate_number').classList.remove('has-error')
            status=true
        }

        var ssnumber = document.getElementById('social_security_number').value
        if (ssnumber == '') {
            document.getElementById("ssno").innerHTML='The social security number field is required.'
            document.getElementById('social_security_number').classList.add('has-error')
            status=false
        } else {
            document.getElementById("ssno").innerHTML=''
            document.getElementById('social_security_number').classList.remove('has-error')
        }

        var vehicale_color = document.getElementById('vehicle_color').value
        if (vehicale_color == '') {
            document.getElementById("color").innerHTML='The vehicle color field is required.'
            document.getElementById('vehicle_color').classList.add('has-error')
            status=false
        } else {
            document.getElementById("color").innerHTML=''
            document.getElementById('vehicle_color').classList.remove('has-error')
        }

        var expiredate = document.getElementById('vehicle_expiration_date').value
        if (expiredate == '') {
            document.getElementById("dated").innerHTML='The vehicle expiration date field is required.'
            document.getElementById('vehicle_expiration_date').classList.add('has-error')
            status=false
        } else {
            document.getElementById("dated").innerHTML=''
            document.getElementById('vehicle_expiration_date').classList.remove('has-error')
        }
        return status
    }

    $('#updatesocialno').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
        var vehicle_plate_number = $('#vehicle_plate_number').val();
        var social_security_number = $('#social_security_number').val();
        var vehicle_color = $('#vehicle_color').val();
        var vehicle_expiration_date = $('#vehicle_expiration_date').val();
        $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.social.update') }}",
            method:"POST",
            data:{ vehicle_plate_number: vehicle_plate_number, social_security_number: social_security_number, vehicle_color: vehicle_color, vehicle_expiration_date: vehicle_expiration_date, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                $('.px-3').removeClass('pt-5');
                $('.px-3').addClass('pt-4');
                $('.changsuc_alert').show();
                $('#changsuc-msg').html("Updated successfully.");
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('vehicle_plate_number')) {
                        $('#plateno').html(responseMsg.errors.vehicle_plate_number).promise().done(function(){
                            $('#vehicle_plate_number').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('social_security_number')) {
                        $('#ssno').html(responseMsg.errors.social_security_number).promise().done(function(){
                            $('#social_security_number').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('vehicle_color')) {
                        $('#color').html(responseMsg.errors.vehicle_color).promise().done(function(){
                            $('#vehicle_color').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('vehicle_expiration_date')) {
                        $('#dated').html(responseMsg.errors.vehicle_expiration_date).promise().done(function(){
                            $('#vehicle_expiration_date').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('ss_number')) {
                        $('#ssno').html(responseMsg.errors.ss_number).promise().done(function(){
                            $('#social_security_number').addClass('has-error');
                        });
                    }
                }
                if (response.status == 400){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.px-3').removeClass('pt-5');
                    $('.px-3').addClass('pt-4');
                    $('.dupdate_alert').show();
                    $('#dupdate-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.px-3').removeClass('pt-5');
                    $('.px-3').addClass('pt-4');
                    $('.dupdate_alert').show();
                    $('#dupdate-msg').html(responseMsg.message);
                }
            }
        });
    });

</script>
<!-- footer -->
@include('includes.driver_footer')