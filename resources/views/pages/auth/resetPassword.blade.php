<!-- header -->
@include('includes.header')

<div class="container my-5">
    <form class="bg-white mx-md-5 py-5 px-md-5 rounded-border shadow-lg" id="resetPasswordForm">
        <h1 class="text-center font-weight-bold">Reset Your Password</h1>
        <div class="col-lg-10 offset-lg-1 px-sm-3 pt-5">

            <input type="hidden" name="otpH" id="otpH" value="{{$otp}}">
            <input type="hidden" name="userH" id="userH" value="{{$user}}">
            <input type="hidden" name="isdCodeH" id="isdCodeH" value="{{$id}}">
            <input type="hidden" name="email" id="email" value="{{$email}}">

            <div class="alert alert-danger reset_alert" style="display: none;">
                <p class="text-danger" id="reset-msg">gshgsj hshjs jhafj</p>
            </div>

            <div class="form-group mb-4">
                <input type="text" name="otp" id="otp" class="form-control inset-input pl-4" placeholder="Enter Your OTP" onkeyup="return validate()">
                <span class="text-danger" id="otp-err"></span>
            </div>
            <div class="form-group mb-4">
                <input type="password" name="password" id="password" class="form-control inset-input pl-4" placeholder="New Password" onkeyup="return validate()">
                <span class="text-danger" id="passw"></span>
            </div>
            <div class="form-group mb-4">
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control inset-input pl-4" placeholder="Confirm Password" onkeyup="return validate()">
                <span class="text-danger" id="cpassw"></span>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
               <button type="submit" class="btn btn-success-theme btn-lrg text-white">Submit</button>
            </div>


        </div>
    </form>
</div>

<script>

      function validate(){
        var status=null;

        var otp = document.getElementById('otp').value;
        if (otp == '') {
            document.getElementById("otp-err").innerHTML='The otp field is required.';
            document.getElementById('otp').classList.add('has-error');
            status = false
        } else if (!otp.match(/^([0-9\s\-\+\(\)]*)$/) || otp.length>4) {
            document.getElementById("otp-err").innerHTML='Please enter valid OTP.';
            document.getElementById('otp').classList.add('has-error')
            status = false;
        } else {
            $('.offset-lg-1').removeClass('pt-2');
            $('.offset-lg-1').addClass('pt-5');
            $('.reset_alert').hide();
            $('#reset-msg').html('');
            document.getElementById("otp-err").innerHTML='';
            document.getElementById('otp').classList.remove('has-error');
            status=true;
        }

        var password = document.getElementById('password').value
        if (password == '') {
            document.getElementById("passw").innerHTML='The password field is required.'
            document.getElementById('password').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("passw").innerHTML='The password must be between 6 and 255 characters.'
            document.getElementById('password').classList.add('has-error')
            status=false

        } else {
            document.getElementById("passw").innerHTML=''
            document.getElementById('password').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('password_confirmation').value
        if (password_confirm == '') {
            document.getElementById("cpassw").innerHTML='The password confirmation field is required.'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("cpassw").innerHTML='The password confirmation must be between 6 and 255 characters.'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("cpassw").innerHTML=''
            document.getElementById('password_confirmation').classList.remove('has-error')
        }
        
        return status
    }

    $(document).ready(function(){
        $('#resetPasswordForm').on('submit', function(event){
            event.preventDefault();
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            var user = $("#userH").val();
            var email = $('#email').val();
            var id = $('#isdCodeH').val();
            var otpH = $('#otpH').val();
            var otp = $('#otp').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            $(".overlay").show();
            $.ajax({
                url: "{{ route('reset.password') }}",
                method:"POST",
                data:{ email: email, id: id, user: user, otpH: otpH, otp: otp, password: password, password_confirmation: password_confirmation, timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    $('#bootboxModal').modal('show');
                    $('.bootboxBody').text(response.message);
                    $('button.bootboxBtn').on('click', function(){
                        $("#bootboxModal").modal('hide');
                        window.location.href = '{{ route('signin') }}'; 
                    });
                },
                error: function(response){
                    $(".overlay").hide();
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('otp')) {
                            $('#otp-err').html(responseMsg.errors.otp).promise().done(function(){
                                $('#otp').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('password')) {
                            $('#passw').html(responseMsg.errors.password).promise().done(function(){
                                $('#password').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                            $('#cpassw').html(responseMsg.errors.password_confirmation).promise().done(function(){
                                $('#password_confirmation').addClass('has-error');
                            });
                        }
                        if (responseMsg.errors.hasOwnProperty('exception')) {
                            $('.offset-lg-1').removeClass('pt-5');
                            $('.offset-lg-1').addClass('pt-2');
                            $('.reset_alert').show();
                            $('#reset-msg').html(responseMsg.message);
                        } 
                    }
                    if (response.status == 404){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('#otp-err').html(responseMsg.message).promise().done(function(){
                            $('#otp').addClass('has-error');
                        });
                    }
                    if (response.status == 403){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.offset-lg-1').removeClass('pt-5');
                        $('.offset-lg-1').addClass('pt-2');
                        $('.reset_alert').show();
                        $('#reset-msg').html(responseMsg.message);
                    }
                    if (response.status == 400){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.offset-lg-1').removeClass('pt-5');
                        $('.offset-lg-1').addClass('pt-2');
                        $('.reset_alert').show();
                        $('#reset-msg').html(responseMsg.message);
                    }
                    if (response.status == 500){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.offset-lg-1').removeClass('pt-5');
                        $('.offset-lg-1').addClass('pt-2');
                        $('.reset_alert').show();
                        $('#reset-msg').html(responseMsg.message);
                    }
                }
            });
        });
    });
</script>

<!-- footer -->
@include('includes.footer')