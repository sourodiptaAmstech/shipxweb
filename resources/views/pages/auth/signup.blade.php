<!-- header -->
@include('includes.header')

<div class="container mb-5">
    <div class="bg-white mx-md-5 py-5 px-md-5 rounded-border shadow-lg">
        <h1 class="text-center font-weight-bold">Sign Up</h1>
        <div class="col-lg-10 offset-lg-1 px-sm-3 pt-5">

            <div class="alert alert-danger signup_alert" style="display: none;">
                <p class="text-danger" id="signup-msg"></p>
            </div>

            <div class="mb-4">
            <div class="row justify-content-start">
                <div class="d-inline">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio1" name="user" value="passenger" onchange="return validate()" checked>
                        <label class="custom-control-label" for="radio1">Customer</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio2" name="user" value="driver" onchange="return validate()">
                        <label class="custom-control-label" for="radio2">Driver</label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <span id="utype" class="text-danger" style="margin-right: 250px;"></span>
            </div>
            </div>

            <div class="passenger_fields">
                <form id="submitPassengerForm">
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group mb-4">
                        <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" class="form-control inset-input pl-4" placeholder="Enter First Name" onkeyup="return validate()" >
                        <span class="text-danger" id="fname"></span>
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group mb-4">
                        <input type="text" class="form-control inset-input pl-4" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="Enter Last Name" onkeyup="return validate()" >
                        <span class="text-danger" id="lname"></span>
                    </div>
                    </div>
                </div>

                <div class="form-group mb-4">
                    <input type="text" class="form-control inset-input pl-4" name="businessName" id="businessName" placeholder="Enter Business Name (Optional)">
                </div>
              
                <div class="form-group mb-4">
                    <input type="hidden" name="code" id="code">
                    <input type="text" class="form-control inset-input pl-4" name="mobile_no" id="mobile_no" value="{{ old('mobile_no') }}" placeholder="Enter Mobile Number" onkeyup="return validate()">
                    <span class="text-danger" id="mobile"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="email" class="form-control inset-input pl-4" name="email" id="email" value="{{ old('email') }}" placeholder="Enter Email" onkeyup="return validate()" >
                    <span class="text-danger" id="mailid"></span>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    <div class="form-group mb-4">
                        <input type="password" class="form-control inset-input pl-4" name="password" id="password" placeholder="Password" onkeyup="return validate()" >
                        <span class="text-danger" id="pass"></span>
                    </div>
                    </div>
                    <div class="col-lg-6">
                    <div class="form-group mb-4">
                        <input type="password" class="form-control inset-input pl-4" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" onkeyup="return validate()" >
                        <span class="text-danger" id="cpass"></span>
                    </div>
                    </div>
                </div>

                <div style="font-size: 18px">
                    <input type="checkbox" name="isChecked" id="isChecked" onchange="checkedCheck()"> <span> I agree to <u class="text-danger ptermCondition" style="cursor: pointer;">Terms & Conditions</u> and <u class="text-danger pprivacyPolicy" style="cursor: pointer;">Privacy Policy</u></span>
                </div>
                <span class="text-danger" id="check"></span>

                <div class="d-flex justify-content-center mb-3 mt-5">
                    <button type="submit" class="btn btn-success-theme btn-lrg text-white">Submit</button>
                </div>
                </form>
            </div>

            {{--DRIVER SIGNUP  --}}
            <div class="driver_fields" style="display: none;">
                <form id="submitDriverForm">
                <div class="form-group my-4">
                    <input type="text" name="first_name" id="dfirst_name" class="form-control inset-input pl-4" value="{{ old('first_name') }}" placeholder="Enter First Name" onkeyup="return validateDriver()">
                    <span class="text-danger" id="fdname"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="text" class="form-control inset-input pl-4" name="last_name" id="dlast_name" value="{{ old('last_name') }}" placeholder="Enter Last Name" onkeyup="return validateDriver()">
                    <span class="text-danger" id="ldname"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="email" name="email" id="demail" class="form-control inset-input pl-4" value="{{ old('email') }}" placeholder="Enter Email Id" onkeyup="return validateDriver()">
                    <span class="text-danger" id="dmailid"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="hidden" name="dcode" id="dcode">
                    <input type="tel" name="mobile" id="dmobile_no" 
                    class="form-control inset-input pl-4"
                    placeholder="Enter Phone Number" value="{{ old('mobile_no') }}" onkeyup="return validateDriver()">
                    <span class="text-danger" id="dmobile"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="text" name="address" id="address" value="{{ old('address') }}" class="form-control inset-input pl-4" placeholder="Enter Your Address" onkeyup="return validateDriver()">
                    <span class="text-danger" id="addr"></span>
                </div>
                <div class="form-group mb-4">
                    <select name="service_type" class="form-control inset-input select-box pl-4" id="service_type" onchange="return validateDriver()">
                    </select>
                    <span class="text-danger" id="service"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="text" name="service_number" id="service_number" value="{{ old('service_number') }}" class="form-control inset-input pl-4" placeholder="Enter Service Number" onkeyup="return validateDriver()">
                    <span class="text-danger" id="serviceno"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="text" name="make" id="make" class="form-control inset-input pl-4" placeholder="Enter Your Vehicle Make" onkeyup="return validateDriver()">

                    {{-- <select name="car_make" id="car_make" class="form-control inset-input" onchange="return validateDriver()">
                    </select> --}}
                    <span class="text-danger" id="dmake"></span>
                </div>
                
                <div class="form-group mb-4">
                    <input type="text" name="model" id="model" class="form-control inset-input pl-4" placeholder="Enter Your Vehicle Model" onkeyup="return validateDriver()">
                    {{-- <select name="car_model" id="car_model" class="form-control inset-input" onchange="return validateDriver()">
                    </select> --}}
                    <span class="text-danger" id="dmodel"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="number" name="model_year" id="model_year" class="form-control inset-input pl-4" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;"  placeholder="Enter Your Vehicle Year" onkeyup="return validateDriver()">
                   {{--  <select name="model_year" class="form-control inset-input select-box" id="selectpicker1" onchange="return validateDriver()">
                            <option selected disabled>Choose Your Vehicle Year</option>
                            <option>2020</option>
                            <option>2021</option>
                    </select> --}}
                    <span class="text-danger" id="year"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="password" class="form-control inset-input pl-4" name="password" id="dpassword" placeholder="Password" onkeyup="return validateDriver()">
                    <span class="text-danger" id="dpass"></span>
                </div>
                <div class="form-group mb-4">
                    <input type="password" class="form-control inset-input pl-4" name="password_confirmation" id="dpassword_confirmation" placeholder="Confirm Password" onkeyup="return validateDriver()">
                    <span class="text-danger" id="dcpass"></span>
                </div>

                <div style="font-size: 18px">
                    <input type="checkbox" name="disChecked" id="disChecked" onchange="checkedCheck()"> <span> I agree to <u class="text-danger dtermCondition" style="cursor: pointer;">Terms & Conditions</u> and <u class="text-danger dprivacyPolicy" style="cursor: pointer;">Privacy Policy</u></span>
                </div>
                <span class="text-danger" id="dcheck"></span>

                <div class="d-flex justify-content-center mb-3 mt-5">
                    <button type="submit" class="btn btn-success-theme btn-lrg text-white">Submit</button>
                </div>
                </form>
            </div>
        </div>

        <h5 class="text-muted text-center pt-3">Already have an account? 
            <a href="{{ url('signin') }}"><span class="text-success-theme signin">Sign In</span></a>
        </h5>

    </div>
</div>

<!--Hidden-->
<input type="hidden" name="hidden_otp" id="hidden_otp">
<input type="hidden" name="hidden_user" id="hidden_user">
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="verificationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <div class="p-2 w-100">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <img src="{{URL::asset('/')}}assets/img/close.png" alt="" class="m-close"
        style="filter: hue-rotate(267deg);">
        </button>
    </div>
    <form id="modalOTP">
        <div class="modal-body w-100">
            <h1 class="text-center">Verify your <span class="text-theme">Account</span></h1>
            <p class="text-center text-muted my-2">You have received an OTP in your phone Number</p>
            <p class="text-center my-2">Enter your OTP</p>
            <p class="text-center text-danger my-1" id="otperror"></p>
            <div class="d-flex">
                <div class="col-md-8 offset-md-2">
                <input type="text" name="otp" id="otp" class="form-control inset-input" placeholder="Enter the OTP" onkeyup="return validateOTP()">
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center mb-5">
            <button type="submit" class="btn btn-lrg btn-success-theme text-white mx-auto">Submit</button>
        </div>
    </form>    
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="ptermCondition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header w-100">
           
        </div>
        <div class="modal-body w-100" style="overflow-y: auto;overflow-x: hidden;height: 500px;">
            <p class="text-left text-muted my-0 pConditionText"></p>
        </div>
        <div class="modal-footer w-100 py-1">
            <button type="button" class="btn btn-warning w-100" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="dtermCondition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header w-100">
           
        </div>
        <div class="modal-body w-100" style="overflow-y: auto;overflow-x: hidden;height: 500px;">
            <p class="text-left text-muted my-0 dConditionText"></p>
        </div>
        <div class="modal-footer w-100 py-1">
            <button type="button" class="btn btn-warning w-100" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>

<script>
    //$ = $uery.noConflict();
    // use :
    //     var s = $("#mobile_no").intlTelInput({
    //         autoPlaceholder: 'polite',
    //         separateDialCode: true,
    //         formatOnDisplay: true,
    //         initialCountry: 'ng',
    //         preferredCountries:["ng"]
    //     });
    //     var s = $("#dmobile_no").intlTelInput({
    //         autoPlaceholder: 'polite',
    //         separateDialCode: true,
    //         formatOnDisplay: true,
    //         initialCountry: 'ng',
    //         preferredCountries:["ng"]
    //     });

    // insteadof :
    //     var d = $("#mobile_no").intlTelInput("getSelectedCountryData").dialCode;
    //     $("#code").val(d);
    //     $(document).on('countrychange', function (e, countryData) {
    //         $("#code").val(($("#mobile_no").intlTelInput("getSelectedCountryData").dialCode));
    //     });

    //     var d = $("#dmobile_no").intlTelInput("getSelectedCountryData").dialCode;
    //     $("#dcode").val(d);
    //     $(document).on('countrychange', function (e, countryData) {
    //         $("#dcode").val(($("#dmobile_no").intlTelInput("getSelectedCountryData").dialCode));
    //     });


    function validate(){
        var status=null;

        var option=document.getElementsByName('user');
        if (!(option[0].checked || option[1].checked)) {
            document.getElementById("utype").innerHTML='The user field is required.'
            status = false;
        } else {
            $('.offset-lg-1').removeClass('pt-2');
            $('.offset-lg-1').addClass('pt-5');
            $('.signup_alert').hide();
            $('#signup-msg').html('');
            document.getElementById("utype").innerHTML=''
            status=true
        }
        
        var fname = document.getElementById('first_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='The first name field is required.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='The first name must be at least 3 characters.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='The first name may not be greater than 255 characters.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else {
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_name').classList.remove('has-error')
        }

        var lname = document.getElementById('last_name').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='The last name field is required.'
            document.getElementById('last_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("lname").innerHTML='The last name must be at least 3 characters.'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("lname").innerHTML='The last name may not be greater than 255 characters.'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('last_name').classList.remove('has-error')
        }

        var mobileno = document.getElementById('mobile_no').value
        if (mobileno == '') {
            document.getElementById("mobile").innerHTML='The mobile no field is required.'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("mobile").innerHTML=''
            document.getElementById('mobile_no').classList.remove('has-error')
        }

        var email = document.getElementById('email').value
        if (email == '') {
            document.getElementById("mailid").innerHTML='The email field is required.'
            document.getElementById('email').classList.add('has-error')
            status=false
        } else {
            document.getElementById("mailid").innerHTML=''
            document.getElementById('email').classList.remove('has-error')
        }

        var password = document.getElementById('password').value
        if (password == '') {
            document.getElementById("pass").innerHTML='The password field is required.'
            document.getElementById('password').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("pass").innerHTML='The password must be between 6 and 255 characters.'
            document.getElementById('password').classList.add('has-error')
            status=false

        } else {
            document.getElementById("pass").innerHTML=''
            document.getElementById('password').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('password_confirmation').value
        if (password_confirm == '') {
            document.getElementById("cpass").innerHTML='The password confirmation field is required.'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("cpass").innerHTML='The password confirmation must be between 6 and 255 characters.'
            document.getElementById('password_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("cpass").innerHTML=''
            document.getElementById('password_confirmation').classList.remove('has-error')
        }
        
        return status
    }

    function validateDriver(){

        var status=null;

        var option=document.getElementsByName('user');
        if (!(option[0].checked || option[1].checked)) {
            document.getElementById("utype").innerHTML='The user field is required.'
            status = false;
        } else {
            $('.offset-lg-1').removeClass('pt-2');
            $('.offset-lg-1').addClass('pt-5');
            $('.signup_alert').hide();
            $('#signup-msg').html('');
            document.getElementById("utype").innerHTML=''
            status=true
        }
        
        var dfname = document.getElementById('dfirst_name').value
        if (dfname == '') {
            document.getElementById("fdname").innerHTML='The first name field is required.'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else if (dfname.length<3) {
            document.getElementById("fdname").innerHTML='The first name must be at least 3 characters.'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else if (dfname.length>255) {
            document.getElementById("fdname").innerHTML='The first name may not be greater than 255 characters.'
            document.getElementById('dfirst_name').classList.add('has-error')
            status=false
        } else {
            document.getElementById("fdname").innerHTML=''
            document.getElementById('dfirst_name').classList.remove('has-error')
        }

        var dlname = document.getElementById('dlast_name').value
        if (dlname == '') {
            document.getElementById("ldname").innerHTML='The last name field is required.'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false
        } else if (dlname.length<3) {
            document.getElementById("ldname").innerHTML='The last name must be at least 3 characters.'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false

        } else if (dlname.length>255) {
            document.getElementById("ldname").innerHTML='The last name may not be greater than 255 characters.'
            document.getElementById('dlast_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("ldname").innerHTML=''
            document.getElementById('dlast_name').classList.remove('has-error')
        }

        var demail = document.getElementById('demail').value
        if (demail == '') {
            document.getElementById("dmailid").innerHTML='The email field is required.'
            document.getElementById('demail').classList.add('has-error')
            status=false
        } else {
            document.getElementById("dmailid").innerHTML=''
            document.getElementById('demail').classList.remove('has-error')
        }

        var mobileno = document.getElementById('dmobile_no').value
        if (mobileno == '') {
            document.getElementById("dmobile").innerHTML='The mobile no field is required.'
            document.getElementById('dmobile_no').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dmobile").innerHTML=''
            document.getElementById('dmobile_no').classList.remove('has-error')
        }

        var serviceType = document.getElementById('service_type').value
        if (serviceType == '') {
            document.getElementById("service").innerHTML='The service type field is required.'
            document.getElementById('service_type').classList.add('has-error')
            status=false
        } else {
            document.getElementById("service").innerHTML=''
            document.getElementById('service_type').classList.remove('has-error')
        }

        var serviceNo = document.getElementById('service_number').value
        if (serviceNo == '') {
            document.getElementById("serviceno").innerHTML='The service number type field is required.'
            document.getElementById('service_number').classList.add('has-error')
            status=false
        } else {
            document.getElementById("serviceno").innerHTML=''
            document.getElementById('service_number').classList.remove('has-error')
        }

        var carMake = document.getElementById('make').value
        if (carMake == '') {
            document.getElementById("dmake").innerHTML='The make field is required.'
            document.getElementById('make').classList.add('has-error')
            status=false
        } else {
            document.getElementById("dmake").innerHTML=''
            document.getElementById('make').classList.remove('has-error')
        }

        var carModel = document.getElementById('model').value
        if (carModel == '') {
            document.getElementById("dmodel").innerHTML='The model field is required.'
            document.getElementById('model').classList.add('has-error')
            status=false
        } else {
            document.getElementById("dmodel").innerHTML=''
            document.getElementById('model').classList.remove('has-error')
        }

        var modelYear = document.getElementById('model_year').value
        if (modelYear == '') {
            document.getElementById("year").innerHTML='The model year field is required.'
            document.getElementById('model_year').classList.add('has-error')
            status=false
        } else {
            document.getElementById("year").innerHTML=''
            document.getElementById('model_year').classList.remove('has-error')
        }

        var carNumber = document.getElementById('address').value
        if (carNumber == '') {
            document.getElementById("addr").innerHTML='The address field is required.'
            document.getElementById('address').classList.add('has-error')
            status=false
        } else {
            document.getElementById("addr").innerHTML=''
            document.getElementById('address').classList.remove('has-error')
        }

        var password = document.getElementById('dpassword').value
        if (password == '') {
            document.getElementById("dpass").innerHTML='The password field is required.'
            document.getElementById('dpassword').classList.add('has-error')
            status=false
        } else if (password.length<6||password.length>255) {
            document.getElementById("dpass").innerHTML='The password must be between 6 and 255 characters.'
            document.getElementById('dpassword').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dpass").innerHTML=''
            document.getElementById('dpassword').classList.remove('has-error')
        }

        var password_confirm = document.getElementById('dpassword_confirmation').value
        if (password_confirm == '') {
            document.getElementById("dcpass").innerHTML='The password confirmation field is required.'
            document.getElementById('dpassword_confirmation').classList.add('has-error')
            status=false
        } else if (password_confirm.length<6||password_confirm.length>255) {
            document.getElementById("dcpass").innerHTML='The password confirmation must be between 6 and 255 characters.'
            document.getElementById('dpassword_confirmation').classList.add('has-error')
            status=false

        } else {
            document.getElementById("dcpass").innerHTML=''
            document.getElementById('dpassword_confirmation').classList.remove('has-error')
        }
        
        return status
    }

    function validateOTP(){
        var otp = document.getElementById('otp').value;
        if (!otp.match(/^([0-9\s\-\+\(\)]*)$/) || otp.length>4) {
            document.getElementById("otperror").innerHTML='Please enter valid OTP.';
            return false;
        } else {
            document.getElementById("otperror").innerHTML='';
            return true;
        }
    }

function checkedCheck(){
    if($('#disChecked').is(":checked")){
        $('#dcheck').html("")
    }
    if($('#isChecked').is(":checked")){
        $('#check').html("")
    }
}

$(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    $('.ptermCondition').click(function(){
        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.terms-conditions') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $('.pConditionText').html(response.data.terms_conditions)
                $(".overlay").hide();

                $("#ptermCondition").modal('show');
                $('#isChecked').prop('checked', true);
                $('#check').html("")
            },
            error: function(response){
                $(".overlay").hide();
                var responseMsg = $.parseJSON(response.responseText);
                $('.offset-lg-1').removeClass('pt-5');
                $('.offset-lg-1').addClass('pt-2');
                $('.signup_alert').show();
                $('#signup-msg').html(responseMsg.message)
            }
        });
    });

    $('.pprivacyPolicy').click(function(){
        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.terms-conditions') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $('.pConditionText').html(response.data.privecy)
                $(".overlay").hide();

                $("#ptermCondition").modal('show');
                $('#isChecked').prop('checked', true);
                $('#check').html("")
            },
            error: function(response){
                $(".overlay").hide();
                var responseMsg = $.parseJSON(response.responseText);
                $('.offset-lg-1').removeClass('pt-5');
                $('.offset-lg-1').addClass('pt-2');
                $('.signup_alert').show();
                $('#signup-msg').html(responseMsg.message)
            }
        });
    });

    $('.dtermCondition').click(function(){
        $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.terms-conditions') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $('.dConditionText').html(response.data.terms_conditions)
                $(".overlay").hide();

                $("#dtermCondition").modal('show');
                $('#disChecked').prop('checked', true);
                $('#dcheck').html("")
            },
            error: function(response){
                $(".overlay").hide();
                var responseMsg = $.parseJSON(response.responseText);
                $('.offset-lg-1').removeClass('pt-5');
                $('.offset-lg-1').addClass('pt-2');
                $('.signup_alert').show();
                $('#signup-msg').html(responseMsg.message)
            }
        });
    });

    $('.dprivacyPolicy').click(function(){
        $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.terms-conditions') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                $('.dConditionText').html(response.data.privecy)
                $(".overlay").hide();

                $("#dtermCondition").modal('show');
                $('#disChecked').prop('checked', true);
                $('#dcheck').html("")
            },
            error: function(response){
                $(".overlay").hide();
                var responseMsg = $.parseJSON(response.responseText);
                $('.offset-lg-1').removeClass('pt-5');
                $('.offset-lg-1').addClass('pt-2');
                $('.signup_alert').show();
                $('#signup-msg').html(responseMsg.message)
            }
        });
    });

    var user = $('input[name="user"]:checked').val()
    if (user=='passenger') {
        $('.passb_header').show();
        $('.passl_header').show();
        $('.driv_header').hide();
        $('.passb_footer').show();
        $('.passl_footer').show();
        $('.driv_footer').hide();
    }

    $('#radio1').on('click',function(){
       if(this.checked){
        $('.passenger_fields').show();
        $('.driver_fields').hide();

        $('.passb_header').show();
        $('.passl_header').show();
        $('.driv_header').hide();
        $('.passb_footer').show();
        $('.passl_footer').show();
        $('.driv_footer').hide();
       }
    });

//DRIVER SERVICE MAKE..........
    $('#radio2').on('click',function(){
       if(this.checked){
        $('.passenger_fields').hide();
        $('.driver_fields').show();

        $('.passb_header').hide();
        $('.passl_header').hide();
        $('.driv_header').show();
        $('.passb_footer').hide();
        $('.passl_footer').hide();
        $('.driv_footer').show();

        //$("input[name='mobile_no']").css('padding-left','95px');
        $(".overlay").show();
        $.ajax({
            url: "{{ route('service.list') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                //$('#service_type').selectpicker();
                $('#service_type').empty();
                var options = '<option value="">Choose Service Type</option>';
                $.each(response.data, function(key, value) {
                  options += '<option value="'+value.id+'">'+value.name+'</option>';
                });

                $('#service_type').append(options);
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 400){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-1').removeClass('pt-5');
                    $('.offset-lg-1').addClass('pt-2');
                    $('.signup_alert').show();
                    $('#signup-msg').html(responseMsg.message).promise().done(function(){
                            $('#service_type').addClass('has-error');
                        });
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-1').removeClass('pt-5');
                    $('.offset-lg-1').addClass('pt-2');
                    $('.signup_alert').show();
                    $('#signup-msg').html(responseMsg.message).promise().done(function(){
                            $('#service_type').addClass('has-error');
                        });
                }
            }
        });

       }
    });
//END DRIVER SERVICE MAKE..........

//MODEL OTP FOR PASSENGER.......................
    $('#modalOTP').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var otp = $('#otp').val();
        var hidden_otp = $('#hidden_otp').val();
        var hidden_user = $('#hidden_user').val();
        $.ajax({
            url: "{{ route('verifyotp') }}",
            method: "POST",
            data: { otp: otp, hidden_otp: hidden_otp, hidden_user: hidden_user},
            success: function(response){
                $("#verificationModal").modal('hide');
                $('#bootboxModal').modal('show');
                $('.bootboxBody').text('OTP verified successfully.');
                //PASSENGER REGISTER.....................
                if (response.hidden_user=='passenger') {
                    var first_name = $('#first_name').val();
                    var last_name = $('#last_name').val();
                    var mobile_no = $('#mobile_no').val();
                    var businessName = $('#businessName').val();
                    var email = $('#email').val();
                    var password = $('#password').val();
                    var password_confirmation = $('#password_confirmation').val();
                    var isActive = response.isActive;
                    $.ajax({
                        url: "{{ route('passenger.signup') }}",
                        method: "POST",
                        data: { first_name: first_name, last_name: last_name, mobile_no: mobile_no, businessName: businessName, email:email, password: password, password_confirmation: password_confirmation, isActive: isActive, timeZone: timeZone },
                        success: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                var user = "passenger";
                                $.ajax({
                                    url: "{{ route('loggedin') }}",
                                    method:"POST",
                                    data:{ email: email, password: password, user: user, timeZone: timeZone },
                                    success: function(response){
                                       location.href="{{ route('passenger.home') }}"; 
                                    },
                                    error: function(response){
                                    }
                                });
                            });
                        },
                        error: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                if (response.status == 422){
                                    var responseMsg = $.parseJSON(response.responseText);
                                    $('#mailid').html(responseMsg.errors.email).promise().done(function(){
                                        $('#email').addClass('has-error');
                                    });
                                }
                                if (response.status == 404){
                                    var responseMsg = $.parseJSON(response.responseText);
                                    $('.offset-lg-1').removeClass('pt-5');
                                    $('.offset-lg-1').addClass('pt-2');
                                    $('.signup_alert').show();
                                    $('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 403){
                                    var responseMsg = $.parseJSON(response.responseText);
                                    $('.offset-lg-1').removeClass('pt-5');
                                    $('.offset-lg-1').addClass('pt-2');
                                    $('.signup_alert').show();
                                    $('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 500){
                                    var responseMsg = $.parseJSON(response.responseText);
                                    $('.offset-lg-1').removeClass('pt-5');
                                    $('.offset-lg-1').addClass('pt-2');
                                    $('.signup_alert').show();
                                    $('#signup-msg').html(responseMsg.message)
                                }
                            });
                        }
                    })
                }
                //END PASSENGER REGISTER.................
                //DRIVER REGISTER........................
                if (response.hidden_user=='driver') {
                    var first_name = $('#dfirst_name').val();
                    var last_name = $('#dlast_name').val();
                    var mobile_no = $('#dmobile_no').val();
                    var isdCode = $('#dcode').val();
                    var service_type = $('#service_type').val();
                    var make = $('#make').val();
                    var model = $('#model').val();
                    var model_year = $('#model_year').val();
                    var service_number = $('#service_number').val();
                    var address = $('#address').val();
                    var email = $('#demail').val();
                    var password = $('#dpassword').val();
                    var password_confirmation = $('#dpassword_confirmation').val();
                    var isActive = response.isActive;
                    $.ajax({
                        url: "{{ route('driver.signup') }}",
                        method: "POST",
                        data: { first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, service_type: service_type, make: make, model: model, model_year: model_year, service_number: service_number, password: password, password_confirmation: password_confirmation, isActive: isActive, address: address, email:email, timeZone: timeZone },
                        success: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                var user = "driver";
                                $.ajax({
                                    url: "{{ route('loggedin') }}",
                                    method:"POST",
                                    data:{ email: email, password: password, user: user, timeZone: timeZone },
                                    success: function(response){

                                        location.href="{{ route('driver.home') }}";
                                    },
                                    error: function(response){
                                    }
                                });
                            });
                        },
                        error: function(response){
                            $('button.bootboxBtn').on('click', function(){
                                $("#bootboxModal").modal('hide');
                                if (response.status == 422){
                                    var responseMsg = $.parseJSON(response.responseText);
                                    $('#dmailid').html(responseMsg.errors.email).promise().done(function(){
                                        $('#demail').addClass('has-error');
                                    });
                                }
                                if (response.status == 404){
                                    var responseMsg = $.parseJSON(response.responseText);
                                    $('.offset-lg-1').removeClass('pt-5');
                                    $('.offset-lg-1').addClass('pt-2');
                                    $('.signup_alert').show();
                                    $('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 403){
                                    var responseMsg = $.parseJSON(response.responseText);
                                    $('.offset-lg-1').removeClass('pt-5');
                                    $('.offset-lg-1').addClass('pt-2');
                                    $('.signup_alert').show();
                                    $('#signup-msg').html(responseMsg.message)
                                }
                                if (response.status == 500){
                                    var responseMsg = $.parseJSON(response.responseText);
                                    $('.offset-lg-1').removeClass('pt-5');
                                    $('.offset-lg-1').addClass('pt-2');
                                    $('.signup_alert').show();
                                    $('#signup-msg').html(responseMsg.message)
                                }
                            });
                        }
                    })
                }
                //END DRIVER REGISTER....................
            },
            error: function(response){
                $('#otperror').html(response.responseJSON.message);
            }

        });
    });
//END MODEL OTP FOR PASSENGER.......................
                
//SUBMIT DRIVER FORM.................................
    $('#submitDriverForm').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var user = $("input[type=radio][name=user]:checked").val();
        var first_name = $('#dfirst_name').val();
        var last_name = $('#dlast_name').val();
        var mobile_no = $('#dmobile_no').val();
        var isdCode = $('#dcode').val();
        var email = $('#demail').val();
        var address = $('#address').val();
        var service_type = $('#service_type').val();
        var make = $('#make').val();
        var model = $('#model').val();
        var model_year = $('#model_year').val();
        var service_number = $('#service_number').val();
        var password = $('#dpassword').val();
        var password_confirmation = $('#dpassword_confirmation').val();
        var disChecked = $('#disChecked:checked').val();
        $(".overlay").show();
        $.ajax({
            url: "{{ route('driver.signup.otp') }}",
            method:"POST",
            data:{ user: user, first_name: first_name, last_name: last_name, mobile_no: mobile_no, isdCode: isdCode, email:email,address:address, service_type: service_type, make: make, model: model, model_year: model_year, service_number: service_number, password: password, password_confirmation: password_confirmation, disChecked: disChecked, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                var otp = response.data;
                $('#hidden_otp').val(otp);
                $('#hidden_user').val('driver');
                $("#verificationModal").modal('show');
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        $('#fdname').html(responseMsg.errors.first_name).promise().done(function(){
                            $('#dfirst_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        $('#ldname').html(responseMsg.errors.last_name).promise().done(function(){
                            $('#dlast_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('email')) {
                        $('#dmailid').html(responseMsg.errors.email).promise().done(function(){
                            $('#demail').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('address')) {
                        $('#addr').html(responseMsg.errors.address).promise().done(function(){
                            $('#address').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        $('#dmobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            $('#dmobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('service_type')) {
                        $('#service').html(responseMsg.errors.service_type).promise().done(function(){
                            $('#service_type').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('make')) {
                        $('#dmake').html(responseMsg.errors.make).promise().done(function(){
                            $('#make').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('model')) {
                        $('#dmodel').html(responseMsg.errors.model).promise().done(function(){
                            $('#model').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('model_year')) {
                        $('#year').html(responseMsg.errors.model_year).promise().done(function(){
                            $('#model_year').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('service_number')) {
                        $('#serviceno').html(responseMsg.errors.service_number).promise().done(function(){
                            $('#service_number').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password')) {
                        $('#dpass').html(responseMsg.errors.password).promise().done(function(){
                            $('#dpassword').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                        $('#dcpass').html(responseMsg.errors.password_confirmation).promise().done(function(){
                            $('#dpassword_confirmation').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('disChecked')) {
                        $('#dcheck').html(responseMsg.errors.disChecked)
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        $('#dmobile').html(responseMsg.message).promise().done(function(){
                            $('#dmobile_no').addClass('has-error');
                        });
                    }
                    $('#utype').html(responseMsg.errors.user);
                }
                if (response.status == 400){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('#dmobile').html(responseMsg.message).promise().done(function(){
                        $('#dmobile_no').addClass('has-error');
                    });
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-1').removeClass('pt-5');
                    $('.offset-lg-1').addClass('pt-2');
                    $('.signup_alert').show();
                    $('#signup-msg').html(responseMsg.message);
                }
            }
      });
    });
//END SUBMIT DRIVER FORM.................................

//SUBMIT PASSENGER FORM.................................
    $('#submitPassengerForm').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var user = $("input[type=radio][name=user]:checked").val();
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var mobile_no = $('#mobile_no').val();
        var isdCode = $('#code').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var password_confirmation = $('#password_confirmation').val();
        var isChecked = $('#isChecked:checked').val();
        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.signup.otp') }}",
            method:"POST",
            data:{ first_name: first_name, last_name: last_name, mobile_no: mobile_no, email: email, isdCode: isdCode, password: password, user: user, password_confirmation: password_confirmation, isChecked: isChecked, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                var otp = response.data;
                $('#hidden_otp').val(otp);
                $('#hidden_user').val('passenger');
                $("#verificationModal").modal('show');
            },

            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        $('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                            $('#first_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        $('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                            $('#last_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        $('#mobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            $('#mobile_no').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('email')) {
                        $('#mailid').html(responseMsg.errors.email).promise().done(function(){
                            $('#email').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password')) {
                        $('#pass').html(responseMsg.errors.password).promise().done(function(){
                            $('#password').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password_confirmation')) {
                        $('#cpass').html(responseMsg.errors.password_confirmation).promise().done(function(){
                            $('#password_confirmation').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('isChecked')) {
                        $('#check').html(responseMsg.errors.isChecked)
                    }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        $('#mobile').html(responseMsg.message).promise().done(function(){
                            $('#mobile_no').addClass('has-error');
                        });
                    }
                    $('#utype').html(responseMsg.errors.user);
                }
                if (response.status == 400){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('#mobile').html(responseMsg.message).promise().done(function(){
                        $('#mobile_no').addClass('has-error');
                    });
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-1').removeClass('pt-5');
                    $('.offset-lg-1').addClass('pt-2');
                    $('.signup_alert').show();
                    $('#signup-msg').html(responseMsg.message);
                }
            }
      });
    });
//END SUBMIT PASSENGER FORM.................................

  });  
</script>

<!-- footer -->
@include('includes.footer')