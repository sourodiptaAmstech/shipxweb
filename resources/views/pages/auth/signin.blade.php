<!-- header -->
@include('includes.header')

<div class="container mb-5">
    <form class="bg-white mx-md-5 py-5 px-md-5 rounded-border shadow-lg" id="loginForm">
        <h1 class="text-center font-weight-bold">Login to your account</h1>
        <div class="col-lg-10 offset-lg-1 px-sm-3 pt-5">

            <div class="alert alert-danger danger_alert" style="display: none;">
                <p class="text-danger" id="error-msg"></p>
            </div>

            <div class="mb-4">
            <div class="row justify-content-center">
                <div class="d-inline">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio1" name="user" value="passenger" onchange="return validate()" checked>
                        <label class="custom-control-label" for="radio1">I am a Customer</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio2" name="user" value="driver" onchange="return validate()">
                        <label class="custom-control-label" for="radio2">I am a Driver</label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <span id="usertype" class="text-danger" style="margin-right: 250px;"></span>
            </div>
            </div>

            <div class="form-group mb-4">
                <input type="email" name="email" id="email" class="form-control inset-input pl-4 shadow" placeholder="Email"
                style="background: #fff url('assets/img/envelope.png') center right 25px no-repeat;padding-right: 73px;" onkeyup="return validate()" autocomplete="off">
                <span class="text-danger" id="mobile"></span>
            </div>

            <div class="form-group mb-4">
                <input type="password" name="password" id="password" class="form-control inset-input pl-4 shadow" placeholder="Password"
                style="background: #fff url('assets/img/padlock_1.png') center right 25px no-repeat;padding-right: 73px;" onkeyup="return validate()" autocomplete="off">
                <span class="text-danger" id="pass"></span>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-success-theme btn-lrg text-white"
                >Sign In</button>
            </div>
           
            <h6 class="text-center pt-4">
                <a href="{{ url('forgotPassword') }}" class="text-muted">Forgot your password?</a>
            </h6>
            <div class="my-4 row align-items-center h-100">
                <div class="col-5" style="height: 2px; width: 100%; background-image: linear-gradient(90deg, #fff, #ced4da)"></div>
                <div class="col-1 text-dark font-weight-bold rounded-circle border d-inline text-center Or">Or</div>
                <div class="col-5" style="height: 2px; width: 100%; background-image: linear-gradient(90deg, #ced4da, #fff)"></div>
            </div>
            <div id="passengerSocial">
                <div class="d-flex mb-3 mt-5">
                    <a href="{{ route('login.facebook') }}?user_type=passenger" class="btn btn-primary btn-lrg w-100 text-white"
                    style="filter: saturate(0.6); background: #4868b9 url(assets/img/facebook.png) center left 25px no-repeat;line-height: 50px;"
                    >
                    <span class="login-text">Customer Sign in with Facebook</span>
                    </a>
                </div>
                <div class="d-flex mb-3 mt-5">
                    <a href="{{ route('login.google') }}?user_type=passenger" class="btn btn-danger btn-lrg w-100 text-white"
                    style="background: #d22c1f url(assets/img/google-plus.png) center left 25px no-repeat;line-height: 50px;"
                    >
                    <span class="login-text">Customer Sign in with Google</span>
                    </a>
                </div>
            </div>
            <div id="driverSocial">
                <div class="d-flex mb-3 mt-5">
                    <a href="{{ route('login.facebook') }}?user_type=driver" class="btn btn-primary btn-lrg w-100 text-white"
                    style="filter: saturate(0.6); background: #4868b9 url(assets/img/facebook.png) center left 25px no-repeat;line-height: 50px;"
                    >
                    <span class="login-text">Driver Sign in with Facebook</span>
                    </a>
                </div>
                <div class="d-flex mb-3 mt-5">
                    <a href="{{ route('login.google') }}?user_type=driver" class="btn btn-danger btn-lrg w-100 text-white"
                    style="background: #d22c1f url(assets/img/google-plus.png) center left 25px no-repeat;line-height: 50px;"
                    >
                    <span class="login-text">Driver Sign in with Google</span>
                    </a>
                </div>
            </div>

        </div>
        <h5 class="text-muted text-center pt-5">Have no account yet? 
            <a href="{{ url('signup') }}"><span class="text-success-theme">Sign Up</span></a>
        </h5>
    </form>
</div>

<script>

t = new Date().toUTCString().split(' ');
var timeZone = t[t.length-1] + moment().format('Z');
document.cookie = "timezoneCookie="+timeZone+"; expires=0; path=/;";

function validate(){
    var status=null;

    var option=document.getElementsByName('user');
    if (!(option[0].checked || option[1].checked)) {
        document.getElementById("usertype").innerHTML='The user field is required.'
        status = false;
    } else {
        $('.offset-lg-1').removeClass('pt-2');
        $('.offset-lg-1').addClass('pt-5');
        $('.danger_alert').hide();
        $('#error-msg').html('');
        document.getElementById("usertype").innerHTML=''
        status=true
    }

    var email = document.getElementById('email').value
    if (email == '') {
        document.getElementById("mobile").innerHTML='The email field is required.'
        document.getElementById('email').classList.add('has-error')
        status=false

    } else {
        document.getElementById("mobile").innerHTML=''
        document.getElementById('email').classList.remove('has-error')
    }

    var password = document.getElementById('password').value
    if (password == '') {
        document.getElementById("pass").innerHTML='The password field is required.'
        document.getElementById('password').classList.add('has-error')
        return false
    } else if (password.length<6||password.length>255) {
        document.getElementById("pass").innerHTML='The password must be between 6 and 255 characters.'
        document.getElementById('password').classList.add('has-error')
        status=false

    } else {
        document.getElementById("pass").innerHTML=''
        document.getElementById('password').classList.remove('has-error')
    }
    
    return status
}

$(document).ready(function(){

    var user = $('input[name="user"]:checked').val()
    if (user=='passenger') {
        $('#passengerSocial').show();
        $('#driverSocial').hide();

        $('.passb_header').show();
        $('.passl_header').show();
        $('.driv_header').hide();
        $('.passb_footer').show();
        $('.passl_footer').show();
        $('.driv_footer').hide();
    }
    $('#radio1').on('click',function(){
       if(this.checked){
        $('#passengerSocial').show();
        $('#driverSocial').hide();

        $('.google_facebook').show();

        $('.passb_header').show();
        $('.passl_header').show();
        $('.driv_header').hide();
        $('.passb_footer').show();
        $('.passl_footer').show();
        $('.driv_footer').hide();
       }
    });
    $('#radio2').on('click',function(){
       if(this.checked){
        $('#passengerSocial').hide();
        $('#driverSocial').show();

        $('.google_facebook').hide();

        $('.passb_header').hide();
        $('.passl_header').hide();
        $('.driv_header').show();
        $('.passb_footer').hide();
        $('.passl_footer').hide();
        $('.driv_footer').show();
       }
    });

    $('#loginForm').on('submit', function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var user = $("input[type=radio][name=user]:checked").val();
        var email = $('#email').val();
        var password = $('#password').val();
        $(".overlay").show();
        $.ajax({
            url: "{{ route('loggedin') }}",
            method:"POST",
            data:{ email: email, password: password, user: user, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                if(user=='passenger'){
                    location.href="{{ route('passenger.home') }}";
                } else {
                    location.href="{{ route('driver.home') }}";
                }
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    
                    if (responseMsg.errors.hasOwnProperty('email')) {
                        $('#mobile').html(responseMsg.errors.email).promise().done(function(){
                            $('#email').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('password')) {
                        $('#pass').html(responseMsg.errors.password).promise().done(function(){
                            $('#password').addClass('has-error');
                        });
                    }
                }
                if (response.status == 401){
                    console.log(response.responseText);
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-1').removeClass('pt-5');
                    $('.offset-lg-1').addClass('pt-2');
                    $('.danger_alert').show();
                    $('#error-msg').html(responseMsg.message);
                }

                if (response.status == 400){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-1').removeClass('pt-5');
                    $('.offset-lg-1').addClass('pt-2');
                    $('.danger_alert').show();
                    $('#error-msg').html(responseMsg.message);
                }

                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-1').removeClass('pt-5');
                    $('.offset-lg-1').addClass('pt-2');
                    $('.danger_alert').show();
                    $('#error-msg').html(responseMsg.message);
                }
            }
        });
    });

});
</script>

<!-- footer -->
@include('includes.footer')