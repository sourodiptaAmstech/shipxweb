@include('includes.header')

<div class="container mb-5">
    <div class="bg-white mx-md-5 py-5 px-md-5 rounded-border shadow-lg">

        <h1 class="text-center font-weight-bold">Social login to shipx</h1>

        <div class="col-lg-10 offset-lg-1 px-sm-3 pt-5">
            <div class="alert alert-danger signup_alert" style="display: none;">
                <p class="text-danger" id="signup-msg"></p>
            </div>

        <form id="submitPassengerForm">

            <input type="hidden" name="social_unique_id" id="social_unique_id" value="{{ $arrayName->social_unique_id }}">
            <input type="hidden" name="avatar" id="avatar" value="{{ $arrayName->avatar }}">
            <input type="hidden" name="login_by" id="login_by" value="{{ $arrayName->login_by }}">
            <input type="hidden" name="email_id" id="email_id" value="{{ $arrayName->email }}">

            <input type="hidden" name="given_name" id="given_name" value="{{ $arrayName->given_name }}">
            <input type="hidden" name="family_name" id="family_name" value="{{ $arrayName->family_name }}">

            <input type="hidden" name="user_type" id="user_type" value="{{ $arrayName->user_type }}">
            <input type="hidden" name="accessToken" id="accessToken" value="{{ $arrayName->accessToken }}">

            <div class="form-group">

               {{--  <input type="hidden" name="code" id="code"> --}}
                <input type="text" class="form-control inset-input pl-4" name="mobile_no" id="mobile_no" value="{{ old('mobile_no') }}" placeholder="Enter Mobile Number" onkeyup="return validate()" >
                <span class="text-danger" id="mobile"></span>
            </div>

            {{-- <div class="form-group">
                <label for="email_id" class="font-weight-bold">Email Address</label>
                <input type="email" class="form-control inset-input" name="email_id" id="email_id" value="{{ $arrayName->email }}" placeholder="Enter Email Address" onkeyup="return validate()" >
                <span class="text-danger" id="email"></span>
            </div> --}}

            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-success-theme btn-lrg text-white">Submit</button>
            </div>
        </form>

        </div>
    </div>
    <!--Hidden-->
    <input type="hidden" name="hidden_otp" id="hidden_otp">

    <div class="modal fade bd-example-modal-lg" id="verificationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="p-2 w-100">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <img src="{{URL::asset('/')}}assets/img/close.png" alt="" class="m-close"
            style="filter: hue-rotate(267deg);">
            </button>
        </div>
        <form id="modalOTP">
            <div class="modal-body w-100">
                <h1 class="text-center">Verify your <span class="text-theme">Account</span></h1>
                <p class="text-center text-muted my-2">You have received an OTP in your phone Number</p>
                <p class="text-center my-2">Enter your OTP</p>
                <p class="text-center text-danger my-1" id="otperror"></p>
                <div class="d-flex">
                    <div class="col-md-8 offset-md-2">
                    <input type="text" name="otp" id="otp" class="form-control inset-input" placeholder="Enter the OTP" onkeyup="return validateOTP()">
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center mb-5">
                <button type="submit" class="btn btn-lrg btn-success-theme text-white mx-auto">Submit</button>
            </div>
        </form>
        </div>
    </div>
    </div>

</div>

<script>
    // $ = $uery.noConflict();
    // use :
    //     var s = $("#mobile_no").intlTelInput({
    //         autoPlaceholder: 'polite',
    //         separateDialCode: true,
    //         formatOnDisplay: true,
    //         initialCountry: 'ng',
    //         preferredCountries:["ng"]
    //     });

    // insteadof :
    //     var d = $("#mobile_no").intlTelInput("getSelectedCountryData").dialCode;
    //     $("#code").val(d);
    //     $(document).on('countrychange', function (e, countryData) {
    //         $("#code").val(($("#mobile_no").intlTelInput("getSelectedCountryData").dialCode));
    //     });


    function validate(){
        var status=null;

        var mobileno = document.getElementById('mobile_no').value
        if (mobileno == '') {
            document.getElementById("mobile").innerHTML='The mobile no field is required.'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        } else {
            $('.offset-lg-1').removeClass('pt-2');
            $('.offset-lg-1').addClass('pt-5');
            $('.signup_alert').hide();
            $('#signup-msg').html('');
            document.getElementById("mobile").innerHTML=''
            document.getElementById('mobile_no').classList.remove('has-error')
            status=true
        }

        return status
    }

    function validateOTP(){
        var otp = document.getElementById('otp').value;
        if (!otp.match(/^([0-9\s\-\+\(\)]*)$/) || otp.length>4) {
            document.getElementById("otperror").innerHTML='Please enter valid OTP.';
            return false;
        } else {
            document.getElementById("otperror").innerHTML='';
            return true;
        }
    }

$(document).ready(function () {
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

//MODEL OTP FOR PASSENGER.......................
    $('#modalOTP').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var otp = $('#otp').val();
        var hidden_otp = $('#hidden_otp').val();

        //var hidden_user = $('#hidden_user').val();
        $.ajax({
            url: "{{ route('verifyotp') }}",
            method: "POST",
            data: { otp: otp, hidden_otp: hidden_otp},
            success: function(response){
                $("#verificationModal").modal('hide');
                $('#bootboxModal').modal('show');
                $('.bootboxBody').text('OTP verified successfully.');

                var given_name = $('#given_name').val();
                var family_name = $('#family_name').val();
                var mobile_no = $('#mobile_no').val();
                var accessToken = $('#accessToken').val();
                var email = $('#email_id').val();
                var social_unique_id = $('#social_unique_id').val();
                var avatar = $('#avatar').val();
                var login_by = $('#login_by').val();
                var user_type = $('#user_type').val();
                $.ajax({
                    url: "{{ route('social.signin') }}",
                    method: "POST",
                    data: { given_name: given_name, family_name: family_name, email:email, mobile_no: mobile_no, accessToken: accessToken, social_unique_id: social_unique_id, avatar:avatar, login_by: login_by, user_type: user_type, timeZone: timeZone },
                    success: function(response){
                        console.log(response)
                        $('button.bootboxBtn').on('click', function(){
                            $("#bootboxModal").modal('hide');
                            if(user_type=='passenger'){
                                document.cookie = "user_type= ; expires=0; path=/;";
                                location.href="{{ route('passenger.home') }}";
                            } else {
                                document.cookie = "user_type= ; expires=0; path=/;";
                                location.href="{{ route('driver.home') }}";
                            }
                        });
                    },
                    error: function(response){
                        $('button.bootboxBtn').on('click', function(){
                            $("#bootboxModal").modal('hide');
                            if (response.status == 404){
                                var responseMsg = $.parseJSON(response.responseText);
                                $('.offset-lg-1').removeClass('pt-5');
                                $('.offset-lg-1').addClass('pt-2');
                                $('.signup_alert').show();
                                $('#signup-msg').html(responseMsg.message)
                            }
                            if (response.status == 403){
                                var responseMsg = $.parseJSON(response.responseText);
                                $('.offset-lg-1').removeClass('pt-5');
                                $('.offset-lg-1').addClass('pt-2');
                                $('.signup_alert').show();
                                $('#signup-msg').html(responseMsg.message)
                            }
                            if (response.status == 500){
                                var responseMsg = $.parseJSON(response.responseText);
                                $('.offset-lg-1').removeClass('pt-5');
                                $('.offset-lg-1').addClass('pt-2');
                                $('.signup_alert').show();
                                $('#signup-msg').html(responseMsg.message)
                            }
                        });
                    }
                });
            },
            error: function(response){
                $('#otperror').html(response.responseJSON.message);
            }

        });
    });
//END MODEL OTP FOR PASSENGER.......................

//SUBMIT PASSENGER FORM.................................
    $('#submitPassengerForm').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var mobile_no = $('#mobile_no').val();

        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.social.signup.otp') }}",
            method:"POST",
            data:{  mobile_no: mobile_no, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                var otp = response.data;
                $('#hidden_otp').val(otp);
                //$('#hidden_user').val('passenger');
                $("#verificationModal").modal('show');
            },
            error: function(response){
                $(".overlay").hide();
                console.log(response.status)
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);

                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        $('#mobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            $('#mobile_no').addClass('has-error');
                        });
                    }
                    // if (responseMsg.errors.hasOwnProperty('email_id')) {
                    //     $('#email').html(responseMsg.errors.email_id).promise().done(function(){
                    //         $('#email_id').addClass('has-error');
                    //     });
                    // }
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        $('#mobile').html(responseMsg.message).promise().done(function(){
                            $('#mobile_no').addClass('has-error');
                        });
                    }
                }
                if (response.status == 400){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('#mobile').html(responseMsg.message).promise().done(function(){
                        $('#mobile_no').addClass('has-error');
                    });
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.offset-lg-1').removeClass('pt-5');
                    $('.offset-lg-1').addClass('pt-2');
                    $('.signup_alert').show();
                    $('#signup-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT PASSENGER FORM.................................
});
</script>
<!-- footer -->
@include('includes.footer')
