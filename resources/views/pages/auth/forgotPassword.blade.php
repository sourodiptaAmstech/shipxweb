<!-- header -->
@include('includes.header')

<div class="container my-5">
    <form class="bg-white mx-md-5 py-5 px-md-5 rounded-border shadow-lg" id="forgetPasswordForm">
        <h1 class="text-center font-weight-bold">Forgot Password?</h1>
        <div class="col-lg-10 offset-lg-1 px-sm-3 pt-5">

            <div class="alert alert-danger forget_alert" style="display: none;">
                <p class="text-danger" id="forget-msg"></p>
            </div>

            <div class="mb-4">
            <div class="row justify-content-center">
                <div class="d-inline">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio1" name="user" value="passenger" onchange="return validate()">
                        <label class="custom-control-label" for="radio1">Customer</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="radio2" name="user" value="driver" onchange="return validate()">
                        <label class="custom-control-label" for="radio2">Driver</label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <span id="utype" class="text-danger"></span>
            </div>
            </div>

            <div class="form-group mb-4">
                <input type="email" name="email" id="email" class="form-control inset-input pl-4" placeholder="Enter Email Address"
                style="background: #fff url('assets/img/forgot.png') center right 25px no-repeat;padding-right: 73px;" onkeyup="return validate()">
                <span class="text-danger" id="mail"></span>
            </div>
            <div class="d-flex justify-content-center mb-3 mt-5">
                <button type="submit" class="btn btn-success-theme btn-lrg text-white">Submit</button>
            </div>
        </div>
    </form>
</div>

<script>
t = new Date().toUTCString().split(' ');
var timeZone = t[t.length-1] + moment().format('Z');

function validate(){
    var status=null;

    var option=document.getElementsByName('user');
    if (!(option[0].checked || option[1].checked)) {
        document.getElementById("utype").innerHTML='The user field is required.'
        status = false;
    } else {
        $('.offset-lg-1').removeClass('pt-2');
        $('.offset-lg-1').addClass('pt-5');
        $('.forget_alert').hide();
        $('#forget-msg').html('');
        document.getElementById("utype").innerHTML=''
        status=true
    }

    var email = document.getElementById('email').value
    if (email == '') {
        document.getElementById("mail").innerHTML='The email field is required.'
        document.getElementById('email').classList.add('has-error')
        status=false

    } else {
        document.getElementById("mail").innerHTML=''
        document.getElementById('email').classList.remove('has-error')
    }

    return status
}

$(document).ready(function(){
 
    $('#forgetPasswordForm').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var user = $("input[type=radio][name=user]:checked").val();
        
        var email = $('#email').val();
        $(".overlay").show();
        $.ajax({
            url: "{{ route('forget.password') }}",
            method:"POST",
            data:{ email: email, user: user, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                console.log(response);
                var value = { id:response.data.id, otp:response.data.otp, user:response.user, email: response.email };
                var JSONvalue = JSON.stringify(value);
            
                $('#bootboxModal').modal('show');
                $('.bootboxBody').text('OTP sent to your email ID.');
                $('button.bootboxBtn').on('click', function(){
                    $("#bootboxModal").modal('hide');
                    window.location.href = 'resetPassword?'+'data='+btoa(JSONvalue);
                });
            },
            error: function(response){
                $(".overlay").hide();
                var responseMsg = $.parseJSON(response.responseText);
                if (response.status == 422){
                    if (responseMsg.errors.hasOwnProperty('email')) {
                        $('#mail').html(responseMsg.errors.email).promise().done(function(){
                            $('#email').addClass('has-error');
                        });
                    }
                    $('#utype').html(responseMsg.errors.user);
                    
                    if (responseMsg.errors.hasOwnProperty('exception')) {
                        // $('.offset-lg-1').removeClass('pt-5');
                        // $('.offset-lg-1').addClass('pt-2');
                        // $('.forget_alert').show();
                        // $('#forget-msg').html(responseMsg.message);
                        errorMessage(responseMsg)
                    } 
                }
                if (response.status == 403){
                    // var responseMsg = $.parseJSON(response.responseText);
                    // $('.offset-lg-1').removeClass('pt-5');
                    // $('.offset-lg-1').addClass('pt-2');
                    // $('.forget_alert').show();
                    // $('#forget-msg').html(responseMsg.message);
                    errorMessage(responseMsg)
                }
                if (response.status == 400){
                    // var responseMsg = $.parseJSON(response.responseText);
                    // $('.offset-lg-1').removeClass('pt-5');
                    // $('.offset-lg-1').addClass('pt-2');
                    // $('.forget_alert').show();
                    // $('#forget-msg').html(responseMsg.message);
                    errorMessage(responseMsg)
                }
                if (response.status == 500){
                    // var responseMsg = $.parseJSON(response.responseText);
                    // $('.offset-lg-1').removeClass('pt-5');
                    // $('.offset-lg-1').addClass('pt-2');
                    // $('.forget_alert').show();
                    // $('#forget-msg').html(responseMsg.message);
                    errorMessage(responseMsg)
                }
            }
        });
    });
});

function errorMessage(responseMsg){
    $('.offset-lg-1').removeClass('pt-5');
    $('.offset-lg-1').addClass('pt-2');
    $('.forget_alert').show();
    $('#forget-msg').html(responseMsg.message); 
}
</script>

<!-- footer -->
@include('includes.footer')