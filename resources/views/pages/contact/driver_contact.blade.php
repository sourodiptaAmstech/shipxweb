@include('includes.driver_header')

<div class="container-fluid contact_banner pb-5 pt-3">
    <div class="row pt-0 py-5 pl-0">
        <div class="col-md-6 my-5 ml-5 py5 pl-0">
            <h1 class="py-2 font-weight-bold">Contact Us</h1>
            <h5 class="py-2 font-weight-bold">Our team members will be more than happy to assist you shortly</h5>
            <div class="my-5">
                <a class="btn btn-lrg-banner btn-store text-white mr-3 mb-3" href="https://apps.apple.com/us/app/id1528462958">
                    <div class="d-flex">
                        <img src="{{URL::asset('/')}}assets/img/apple.png" alt="">  
                        <div>
                            <div class="d-block">
                                <h6 style="font-size: 15px">Download on the</h6>
                            </div>
                            <div class="d-block">App store</div>
                        </div>
                    </div>
                </a>
                <a class="btn btn-lrg-banner btn-store text-white mr-3 mb-3" href="https://play.google.com/store/apps/details?id=com.shipx.user">
                    <div class="d-flex">
                        <img src="{{URL::asset('/')}}assets/img/google.png" alt="">  
                        <div>
                            <div class="d-block">
                                <h6 style="font-size: 15px">Android app on</h6>
                            </div>
                            <div class="d-block">Google Play</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-9 px-0">
                <form class="px-md-5 mx-md-5" id="contact_us_form">
                    <div class="px-3 upper pt-5">
                        <div class="alert alert-danger dupdate_alert" style="display: none;">
                            <p class="text-danger" id="dupdate-msg"></p>
                        </div>
                        <div class="alert alert-success dupdatesuc_alert" style="display: none;">
                            <p class="text-success" id="dupdatesuc-msg"></p>
                        </div>

                        <div style="text-align: center;">
                            <h2 class="font-weight-bold mb-4">Contact With Us</h2>
                            <p class="my-4">Contact with us by completing the following form</p>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                            <div class="form-group mb-4">
                                <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" class="form-control inset-input pl-4" placeholder="First Name" onkeyup="return validate()" >
                                <span class="text-danger" id="fname"></span>
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group mb-4">
                                <input type="text" class="form-control inset-input pl-4" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="Last Name" onkeyup="return validate()" >
                                <span class="text-danger" id="lname"></span>
                            </div>
                            </div>
                        </div>

                        <div class="form-group mb-4">
                            <input style="background: #fff" type="email" id="email" name="email" value="{{ old('email') }}" class="form-control inset-input pl-4" placeholder="Enter Your Email" onkeyup="return validate()">
                            <span class="text-danger" id="mail"></span>
                        </div>

                        <div class="form-group mb-4">
                            <textarea name="message" id="message" class="form-control pl-4" placeholder="Type Your Message" onkeyup="return validate()"></textarea>
                            <span class="text-danger" id="msg"></span>
                        </div>

                        <div class="d-flex justify-content-center my-5">
                            <button type="submit" class="btn btn-success-theme btn-lrg text-white mb-5">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Subscribe Form -->
@include('common.newsletter')

<script>
    function validate(){
      var status=null;

      var first_name = document.getElementById('first_name').value;
      if (first_name == '') {
          document.getElementById("fname").innerHTML='The first name field is required.'
          document.getElementById('first_name').classList.add('has-error')
          status = false
      } else {
          $('.upper').addClass('pt-5');
          $('.dupdate_alert').hide();
          $('#dupdate-msg').html('');
          $('.dupdatesuc_alert').hide();
          $('#dupdatesuc-msg').html('');
          document.getElementById("fname").innerHTML='';
          document.getElementById('first_name').classList.remove('has-error')
          status=true;
      }

      var last_name = document.getElementById('last_name').value
      if (last_name == '') {
          document.getElementById("lname").innerHTML='The last name field is required.'
          document.getElementById('last_name').classList.add('has-error')
          status=false
      } else {
          document.getElementById("lname").innerHTML=''
          document.getElementById('last_name').classList.remove('has-error')
      }

      var email = document.getElementById('email').value
      if (email == '') {
          document.getElementById("mail").innerHTML='The email field is required.'
          document.getElementById('email').classList.add('has-error')
          status=false
      } else {
          document.getElementById("mail").innerHTML=''
          document.getElementById('email').classList.remove('has-error')
      }

      var message = document.getElementById('message').value
      if (message == '') {
          document.getElementById("msg").innerHTML='The message field is required.'
          document.getElementById('message').classList.add('has-error')
          status=false
      } else {
          document.getElementById("msg").innerHTML=''
          document.getElementById('message').classList.remove('has-error')
      }
      
      return status
  }

  $(document).ready(function(){
      $('#contact_us_form').on('submit', function(event){
          event.preventDefault();
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
          t = new Date().toUTCString().split(' ');
          var timeZone = t[t.length-1] + moment().format('Z');
          var first_name = $('#first_name').val();
          var last_name = $('#last_name').val();
          var email = $('#email').val();
          var message = $('#message').val();
          $(".overlay").show();
          $.ajax({
              url: "{{ route('contact.to.us') }}",
              method:"POST",
              data:{ first_name: first_name, last_name: last_name, email: email, message: message, timeZone: timeZone },
              success: function(response){
                  $(".overlay").hide();
                  $('.upper').removeClass('pt-5');
                  $('.upper').addClass('pt-4');
                  $('.dupdatesuc_alert').show();
                  $('#dupdatesuc-msg').html(response.message);
                  $('#first_name').val('');
                  $('#last_name').val('');
                  $('#email').val('');
                  $('#message').val('');
              },
              error: function(response){
                  $(".overlay").hide();
                  if (response.status == 422){
                      var responseMsg = $.parseJSON(response.responseText);
                      if (responseMsg.errors.hasOwnProperty('first_name')) {
                          $('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                              $('#first_name').addClass('has-error');
                          });
                      }
                      if (responseMsg.errors.hasOwnProperty('email')) {
                          $('#mail').html(responseMsg.errors.email).promise().done(function(){
                              $('#email').addClass('has-error');
                          });
                      }
                      if (responseMsg.errors.hasOwnProperty('last_name')) {
                          $('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                              $('#last_name').addClass('has-error');
                          });
                      }
                      if (responseMsg.errors.hasOwnProperty('message')) {
                          $('#msg').html(responseMsg.errors.message).promise().done(function(){
                              $('#message').addClass('has-error');
                          });
                      }
                      if (responseMsg.errors.hasOwnProperty('exception')) {
                          $('.upper').removeClass('pt-5');
                          $('.upper').addClass('pt-4');
                          $('.dupdate_alert').show();
                          $('#dupdate-msg').html(responseMsg.message);
                      } 
                  }
                  if (response.status == 403){
                      var responseMsg = $.parseJSON(response.responseText);
                      $('.upper').removeClass('pt-5');
                      $('.upper').addClass('pt-4');
                      $('.dupdate_alert').show();
                      $('#dupdate-msg').html(responseMsg.message);
                  }
                  if (response.status == 400){
                      var responseMsg = $.parseJSON(response.responseText);
                      $('.upper').removeClass('pt-5');
                      $('.upper').addClass('pt-4');
                      $('.dupdate_alert').show();
                      $('#dupdate-msg').html(responseMsg.message);
                  }
                  if (response.status == 500){
                      var responseMsg = $.parseJSON(response.responseText);
                      $('.upper').removeClass('pt-5');
                      $('.upper').addClass('pt-4');
                      $('.dupdate_alert').show();
                      $('#dupdate-msg').html(responseMsg.message);
                  }
              }
          });
      });
  });
</script>
@include('includes.driver_footer')
