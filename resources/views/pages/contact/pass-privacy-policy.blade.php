@include('includes.passenger_header')
<div class="bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="upper py-5">
                    <div class="alert alert-danger update_alert" style="display: none;">
                        <p class="text-danger" id="update-msg"></p>
                    </div>
                    <h2 class="text-left font-weight-bold">Privacy Policy</h2>
                    <div class="ptnc privacyPolicyText">
                    
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.terms-conditions') }}",
            method: "GET",
            data: {timeZone: timeZone},
            success: function(response){
                console.log(response)
                $('.privacyPolicyText').html(response.data.privecy)
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                var responseMsg = $.parseJSON(response.responseText);
                $('.upper').removeClass('pt-5');
                $('.upper').addClass('pt-4');
                $('.update_alert').show();
                $('#update-msg').html(responseMsg.message)
            }
        });
</script>
@include('includes.passenger_footer')
