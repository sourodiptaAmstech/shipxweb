<!-- header -->
@include('includes.header')

<!-- Banner section -->
<div class="container-fluid about_banner pb-5 pt-3">
    <div class="row pt-0 py-5 pl-0">
        <div class="col-md-6 my-5 ml-5 py5 pl-0">
            <h1 class="py-2 font-weight-bold">About SHIPX</h1>
            
            <div class="my-5">
                <a class="btn btn-lrg-banner btn-store text-white mr-3 mb-3" href="https://apps.apple.com/us/app/id1528462958">
                    <div class="d-flex">
                        <img src="{{URL::asset('/')}}assets/img/apple.png" alt="">  
                        <div>
                            <div class="d-block">
                                <h6 style="font-size: 15px">Download on the</h6>
                            </div>
                            <div class="d-block">App store</div>
                        </div>
                    </div>
                </a>
                <a class="btn btn-lrg-banner btn-store text-white mr-3 mb-3" href="https://play.google.com/store/apps/details?id=com.shipx.user">
                    <div class="d-flex">
                        <img src="{{URL::asset('/')}}assets/img/google.png" alt="">  
                        <div>
                            <div class="d-block">
                                <h6 style="font-size: 15px">Android app on</h6>
                            </div>
                            <div class="d-block">Google Play</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- About -->
<div class="container about">
        
    <div class="row py-5">
        <div class="col-md-6">
            <img src="{{URL::asset('/')}}assets/img/mob4.jpg" width="100%" height="100%" controls="controls">
        </div>
        <div class="col-md-6">
            <h2 class="text-left font-weight-bold pb-md-3">Why SHIPX</h2>
        
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                we are new and unique app that will redefine the delivery system, customers will experience new technology called SHIPX, by finger tip customer can order what they want and delivered right to there destination and they will enjoy live tracking, free certified signature on delivery, picture of the items delivered and mostly in affordable and competitive pricing from other delivery companies No more extra chargers for signature on delivery No more wasting times on getting quotes No more extra chargers for same delivery No more weighting and measuring All done with us on flat rate – same day delivery just from finger tip.
            </h6>
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                Prohibited items we don’t ship or deliver
            </h6>
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                – No Cash
            </h6>
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                – No drugs all kinds including but not limited weed-marijuana etc
            </h6>
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                – No guns – weapons
            </h6>
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                – No alcohol all kinds
            </h6>
            <h6 style="line-height: 27px;font-size: 1.02rem" class="text-muted">
                – No illegal substances for any reasons
            </h6>
        </div>
    </div>
</div>

<!-- How it Works -->
<div class="container-fluid work">
        <h1 class="text-center pt-md-5 mt-md-5 font-weight-bold">We are always here to help you</h1>
        <h5 class="text-center text-muted pt-4 font-weight-bold">
            US Based Customer Service
        </h5>
        
        <section class="center slider mt-5">
            
            <div>
                <img src="{{url('assets/img/helppic2.png')}}">
                <h5 class="text-center font-weight-bold my-4">SHIPX Help Service</h5>
            </div>
            <div>
                <img src="{{url('assets/img/helppic3.png')}}">
                <h5 class="text-center font-weight-bold my-4">Pay Securely</h5>
            </div>
            <div>
                <img src="{{url('assets/img/helppic1.png')}}">
                <h5 class="text-center font-weight-bold my-4">Register Securley</h5>
            </div>
           
            <div>
                <img src="{{url('assets/img/helppic2.png')}}">
                <h5 class="text-center font-weight-bold my-4">SHIPX Help Service</h5>
            </div>
            <div>
                <img src="{{url('assets/img/helppic3.png')}}">
                <h5 class="text-center font-weight-bold my-4">Pay Securely</h5>
            </div>
            <div>
                <img src="{{url('assets/img/helppic1.png')}}">
                <h5 class="text-center font-weight-bold my-4">Register Securley</h5>
            </div>

        </section>
</div>

<!-- Subscribe Form -->
@include('common.newsletter') 

<!-- footer -->
@include('includes.footer')