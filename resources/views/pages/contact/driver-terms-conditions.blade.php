@include('includes.driver_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-12 px-0">
                <div class="upper py-5">
                    <div class="alert alert-danger update_alert" style="display: none;">
                        <p class="text-danger" id="update-msg"></p>
                    </div>
                    <h2 class="text-left font-weight-bold">Terms & Conditions</h2>
                    <div class="ptnc dtermsConditions">
                        
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    $(".overlay").show();
    $.ajax({
        url: "{{ route('driver.terms-conditions') }}",
        method: "GET",
        data: {timeZone: timeZone},
        success: function(response){
            console.log(response)
            $('.dtermsConditions').html(response.data.terms_conditions)
            $(".overlay").hide();
        },
        error: function(response){
            $(".overlay").hide();
            var responseMsg = $.parseJSON(response.responseText);
            $('.upper').removeClass('pt-5');
            $('.upper').addClass('pt-4');
            $('.update_alert').show();
            $('#update-msg').html(responseMsg.message)
        }
    });
</script>
@include('includes.driver_footer')
