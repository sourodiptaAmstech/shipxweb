<!-- header -->
@include('includes.passenger_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.passenger_sidebar')
            </div>
            <div class="col-lg-9 px-0">

                <form class="" id="promoCode">
                    <div class="px-md-5 mx-md-5">
                      <div class="px-3 upper pt-5">
                        <div class="alert alert-danger update_alert" style="display: none;">
                            <p class="text-danger" id="update-msg"></p>
                        </div>
                        <div class="alert alert-success updatesuc_alert" style="display: none;">
                            <p class="text-success" id="updatesuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-4">{{trans('weblng.ADD_PROMO')}}</h2>
                        <div class="form-group">
                            <input type="text" name="promo_code" id="promo_code" class="form-control inset-input" placeholder="{{trans('weblng.FIELD.PROMOCODE')}}" onblur="return validate()">
                            <span class="text-danger" id="promo"></span>
                        </div>
                        <div class="d-flex justify-content-start mb-5 mt-1">
                            <button type="submit" class="btn btn-lg btn-success-theme text-white">{{trans('weblng.FIELD.SAVE')}}</button>
                        </div>
                      </div>
                    </div>
                </form>
                
                <div class="container my-2" id="promobody">
                    
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    function validate(){
        var status=null;
        var promoCode = document.getElementById('promo_code').value;
        if (promoCode == '') {

            document.getElementById("promo").innerHTML='{{trans('weblng.VALIDATION_MSG.PROMOCODE')}}'
            document.getElementById('promo_code').classList.add('has-error')
            status = false
        } else {
            $('.upper').addClass('pt-5');
            $('.update_alert').hide();
            $('#update-msg').html('');
            $('.updatesuc_alert').hide();
            $('#updatesuc-msg').html('');
            document.getElementById("promo").innerHTML='';
            document.getElementById('promo_code').classList.remove('has-error')
            status=true;
        }
        return status
    }

    $(document).ready(function(){
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $('#promoCode').on('submit', function(event){
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var promo_code = $('#promo_code').val();
            $(".overlay").show();
            $.ajax({
                url: "{{ route('passenger.add.promocode') }}",
                method:"POST",
                data:{ promo_code: promo_code, timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    console.log(response)
                    getPromoCode()
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.updatesuc_alert').show();
                    $('#updatesuc-msg').html(response.message);
                    $('#promo_code').val('');
                    
                },
                error: function(response){
                    $(".overlay").hide();
                    console.log(response)
                    if (response.status == 422){
                        var responseMsg = $.parseJSON(response.responseText);
                        if (responseMsg.errors.hasOwnProperty('promo_code')) {
                            $('#promo').html(responseMsg.errors.promo_code).promise().done(function(){
                                $('#promo_code').addClass('has-error');
                            });
                        } else {
                            $('.upper').removeClass('pt-5');
                            $('.upper').addClass('pt-4');
                            $('.update_alert').show();
                            $('#update-msg').html(responseMsg.message);
                        } 
                    }
                    if (response.status == 400){
                        var responseMsg = $.parseJSON(response.responseText);
                        $('.upper').removeClass('pt-5');
                        $('.upper').addClass('pt-4');
                        $('.update_alert').show();
                        $('#update-msg').html(responseMsg.message);
                    }
                }
            });
        });
        getPromoCode()
    });

function getPromoCode(){
    $(".overlay").show();
    $.ajax({
        url: "{{ route('passenger.get.promocode') }}",
        method:"GET",
        data:{ timeZone: timeZone },
        success: function(response){
            console.log(response.data)
            $(".overlay").hide();
            $('#promobody').html('');
            var options = '';
            $.each(response.data, function(key, value) {
                options += `
                <div class="card text-center1 col-lg-8">
                    <div class="card-body">
                        <div class="row my-2">
                            <div class="col-lg-12 mx-0 px-0">
                                <h2 class="text-muted">
                                    `+value.promocode.discount+`% {{trans('weblng.OFF')}}
                                </h2>
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-lg-12 mx-0 px-0">
                                <h4 class="text-muted">
                                {{trans('weblng.COUPON_APPLIED')}}<br> `+value.promocode.promo_code+`
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted col-lg-12">
                        <h4 class="text-muted float-right">
                            {{trans('weblng.VALID_UNTILL')}} `+moment(value.promocode.expiration).format('MMM Do YYYY')+`
                        </h4>                          
                    </div>
                </div><br>
                `;
            });
            $('#promobody').append(options);
        },
        error: function(response){
            $(".overlay").hide();
            console.log(response);
        }
    });
}
</script>
<!-- footer -->
@include('includes.passenger_footer')