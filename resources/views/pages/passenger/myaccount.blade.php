<!-- header -->
@include('includes.passenger_header')

<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.passenger_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <form class="px-md-5 mx-md-5" id="profileUpdateFrom">
                    <div class="px-3 upper pt-5">
                        <div class="alert alert-danger update_alert" style="display: none;">
                            <p class="text-danger" id="update-msg"></p>
                        </div>
                        <div class="alert alert-success updatesuc_alert" style="display: none;">
                            <p class="text-success" id="updatesuc-msg"></p>
                        </div>

                        <div class="row justify-content-center mb-5 mx-0 below pt-5">
                            <div style="height: 157px;width: 157px;background-color: #ddd;border: 9px solid #f18933;border-radius: 50%; cursor: pointer;"
                            class="m-0 profile-picture" data-toggle="tooltip" title="Edit Your Image">
                                <img style="object-fit: cover;height: 140px; width: 140px; background-color: #ddd; border-radius: 50%"
                                src="{{url('assets/img/profile-user.png')}}" alt="" id="image">
                                <span class="edit-profile bg-white rounded-circle d-inline-flex justify-content-center align-items-center position-absolute" data-placement="bottom">
                                    <i class="fas fa-edit"></i>
                                </span>
                            </div>
                        </div>
                        <div style="display: none;">
                            <input type="file" name="profile_image" id="profile_image">
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" name="first_name" id="first_name" class="form-control inset-input pl-4" placeholder="First Name" onkeyup="return validate()">
                            <span class="text-danger" id="fname"></span>
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" name="last_name" id="last_name" class="form-control inset-input pl-4" placeholder="Last Name" onkeyup="return validate()">
                            <span class="text-danger" id="lname"></span>
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" name="business_name" id="business_name" class="form-control inset-input pl-4" placeholder="Business Name">
                        </div>

                        <div class="form-group mb-4">
                            <input style="background: #fff url('../assets/img/edit.png') center right 25px no-repeat;"
                            type="email" id="email" name="email" class="form-control inset-input pl-4" placeholder="Email Address" onkeyup="return validate()">
                            <span class="text-danger" id="pmail"></span>
                        </div>
                        <div class="form-group mb-4">
                            <input style="background: #fff url('../assets/img/edit.png') center right 25px no-repeat;"
                            type="text" name="mobile_no" id="mobile_no" class="form-control inset-input pl-4" placeholder="Mobile Number" onkeyup="return validate()">
                            <span class="text-danger" id="mobile"></span>
                        </div>
                        <div class="d-flex justify-content-center my-5">
                            <button type="submit" class="btn btn-success-theme btn-lrg grad text-white mb-5">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    $(".overlay").show();
    $.ajax({
        url: "{{ route('passenger.profile.list') }}",
        method:"GET",
        data:{ timeZone: timeZone },
        success: function(response){
            console.log(response);
            $(".overlay").hide();

            $('#first_name').val(response.data.first_name);
            $('#last_name').val(response.data.last_name);
            $('#business_name').val(response.data.businessName);
            $('#mobile_no').val(response.data.mobile);
            $('#email').val(response.data.email);
            var picture = response.data.picture;
            if(picture!=null||picture!=''){
                var isValid = picture.includes("http");
                if (isValid) {
                    $("#image").attr({ "src": picture });
                } else {
                    $("#image").attr({ "src": "https://app.shipxnow.com/storage/"+picture });
                   /// $("#image").attr({ "src": "https://demos.mydevfactory.com/debarati/shipx/storage/"+picture });
                }
            }

        },
        error: function(response){
            console.log(response);
            $(".overlay").hide();
            var responseMsg = $.parseJSON(response.responseText);
            $('.upper').removeClass('pt-5');
            $('.upper').addClass('pt-4');
            $('.below').removeClass('pt-5');
            $('.below').addClass('pt-1');
            $('.update_alert').show();
            $('#update-msg').html(responseMsg.message);
        }
    });


    $('#timeZone').val(timeZone);
    $(".profile-picture").click(function(e) {
        $("#profile_image").click();
    });
    function fasterPreview(uploader) {
        if ( uploader.files && uploader.files[0] ){
        $('#image').attr('src',
        window.URL.createObjectURL(uploader.files[0]) );
        }
    }
    $('input:file').change(function() {
        fasterPreview(this);
        $('.upper').removeClass('pt-4');
        $('.upper').addClass('pt-5');
        $('.below').removeClass('pt-1');
        $('.below').addClass('pt-5');
        $('.update_alert').hide();
        $('#update-msg').html('');
        $('.updatesuc_alert').hide();
        $('#updatesuc-msg').html('');
    });

    function validate(){
        var status=null;

        var fname = document.getElementById('first_name').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='The first name field is required.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length<3) {
            document.getElementById("fname").innerHTML='The first name must be at least 3 characters.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else if (fname.length>255) {
            document.getElementById("fname").innerHTML='The first name may not be greater than 255 characters.'
            document.getElementById('first_name').classList.add('has-error')
            status=false
        } else {
            $('.upper').removeClass('pt-4');
            $('.upper').addClass('pt-5');
            $('.below').removeClass('pt-1');
            $('.below').addClass('pt-5');
            $('.update_alert').hide();
            $('#update-msg').html('');
            $('.updatesuc_alert').hide();
            $('#updatesuc-msg').html('');
            document.getElementById("fname").innerHTML=''
            document.getElementById('first_name').classList.remove('has-error')
            status=true
        }

        var lname = document.getElementById('last_name').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='The last name field is required.'
            document.getElementById('last_name').classList.add('has-error')
            status=false
        } else if (lname.length<3) {
            document.getElementById("lname").innerHTML='The last name must be at least 3 characters.'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else if (lname.length>255) {
            document.getElementById("lname").innerHTML='The last name may not be greater than 255 characters.'
            document.getElementById('last_name').classList.add('has-error')
            status=false

        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('last_name').classList.remove('has-error')
        }

        var mobileno = document.getElementById('mobile_no').value
        if (mobileno == '') {
            document.getElementById("mobile").innerHTML='The mobile no field is required.'
            document.getElementById('mobile_no').classList.add('has-error')
            status=false

        }else {
            document.getElementById("mobile").innerHTML=''
            document.getElementById('mobile_no').classList.remove('has-error')
        }

        var emailid = document.getElementById('email').value
        if (emailid == '') {
            document.getElementById("pmail").innerHTML='The email field is required.'
            document.getElementById('email').classList.add('has-error')
            status=false

        }else {
            document.getElementById("pmail").innerHTML=''
            document.getElementById('email').classList.remove('has-error')
        }

        return status
    }



$(document).ready(function () {

    //SUBMIT PASSENGER FORM.................................
    $('#profileUpdateFrom').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.profile.update') }}",
            method:"POST",

            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                $(".overlay").hide();
                $('.upper').removeClass('pt-5');
                $('.upper').addClass('pt-4');
                $('.below').removeClass('pt-5');
                $('.below').addClass('pt-1');
                $('.updatesuc_alert').show();
                $('#updatesuc-msg').html(response.message);
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('first_name')) {
                        $('#fname').html(responseMsg.errors.first_name).promise().done(function(){
                            $('#first_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('last_name')) {
                        $('#lname').html(responseMsg.errors.last_name).promise().done(function(){
                            $('#last_name').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('email')) {
                        $('#pmail').html(responseMsg.errors.email).promise().done(function(){
                            $('#email').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('mobile_no')) {
                        $('#mobile').html(responseMsg.errors.mobile_no).promise().done(function(){
                            $('#mobile_no').addClass('has-error');
                        });
                    }

                    if (responseMsg.errors.hasOwnProperty('profile_image')) {
                        $('.upper').removeClass('pt-5');
                        $('.upper').addClass('pt-4');
                        $('.below').removeClass('pt-5');
                        $('.below').addClass('pt-1');
                        $('.update_alert').show();
                        $('#update-msg').html(responseMsg.errors.profile_image);
                    }

                    // if (responseMsg.errors.hasOwnProperty('exception')) {
                    //     $('#emailadd').html(responseMsg.message).promise().done(function(){
                    //         $('#email_id').addClass('has-error');
                    //     });
                    // }
                }
                if (response.status == 403){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.below').removeClass('pt-5');
                    $('.below').addClass('pt-1');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.below').removeClass('pt-5');
                    $('.below').addClass('pt-1');

                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                }
            }
        });
    });
//END SUBMIT PASSENGER FORM.................................
});

</script>

<!-- footer -->
@include('includes.passenger_footer')
