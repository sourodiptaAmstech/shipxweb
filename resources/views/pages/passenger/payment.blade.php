<!-- header -->
@include('includes.passenger_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.passenger_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <div class="px-md-5 mx-md-5">
                    <div class="px-3 pt-5">
                        <h2 class="font-weight-bold mb-5">Payment Methods</h2>
                        {{-- <div class="text-center mt-5">
                            <h3 class="font-weight-bold ">View Transaction</h3>
                        </div> --}}
                        <div class="mt-0">
                            <a href="{{ route('passenger.addpayment') }}" class="btn btn-success-theme btn-lg text-white">Add Debit Card or Credit Card</a>
                        </div>
                        <div class="mt-5">
                            <a href="{{ route('passenger.promocode') }}" class="btn btn-success-theme btn-lg text-white">Add Promo Code or Gift Code</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <script>
    $(document).ready(function(){

        $('#DebitCard').on('click', function(event){
            event.preventDefault();
            t = new Date().toUTCString().split(' ');
            var timeZone = t[t.length-1] + moment().format('Z');
            alert(timeZone);
            $(".overlay").show();
            $.ajax({
                url: "{{ route('passenger.ListCard') }}",
                method:"GET",
                data:{ timeZone: timeZone },
                success: function(response){
                    $(".overlay").hide();
                    console.log(response);
                    // console.log(response.data.emailStatus);
                    // if (response.data.emailStatus=='EMAILNOTVERIFIED'||response.data.emailStatus=='EMAILNOTADDED') {
                    //     $(".overlay").hide();
                    //     $('.debit_card').css({'display':'none'});
                    //     $('#card-field').css({'display':'block'});
                    // }
                    // if (response.data.emailStatus=='EMAILVERIFIED') {
                    //     // $('.debit_card').css({'display':'none'});
                    //     // $('.addCard').css({'display':'block'});
                    //     //$('#email_paystack').val(response.data.email);
                    //     //$('#paymentModal').modal('show');
                    //     //location.href='';
                    // }
                },
                error: function(response){
                    $(".overlay").hide();
                }
            });
        });
    });
</script> --}}

<!-- footer -->
@include('includes.passenger_footer')