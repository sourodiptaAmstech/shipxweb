<!-- header -->
@include('includes.passenger_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.passenger_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <div class="px-md-5 mx-md-5">
                    <div class="px-3 upper pt-5">
                        <div class="alert alert-danger update_alert" style="display: none;">
                            <p class="text-danger" id="update-msg"></p>
                        </div>
                        <h2 class="font-weight-bold mb-0">History</h2>
                        <div class="container my-2" id="tripHistory"> 

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.trip.history') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                var options = '';
                if(response.data.length==0){
                    options=`<h6 style="font-size: x-large;" class="mt-5 text-muted">You did not apply for any trip yet.</h6>`;
                    $('#tripHistory').addClass('px-0');
                } else {
                console.log(response)
                $.each(response.data, function(key, value) {
                    console.log(value)
                    // var requestStatus=value.requestDetails.request_status;
                    // if (requestStatus=='CANCELBYDRIVER'||requestStatus=='CANCELBYPASSENGER'||requestStatus=='CANCELBYSYSTEM') {
                    //     var cancel='<p class="text-danger">Cancelled</p>';
                    // }else{
                    //     var cancel='';
                    // }
                    //var cancel='';
                    options += `
    <div class="row shadow-lg bg-white" style="border: 1px solid lightgrey;">

        <div class="col-lg-12 px-0 d-flex">
            <img src="`+value.static_map+`" alt="" style="width: 100%;height: auto;">
        </div>

        <div class="col-lg-12 px-5 py-3">
            <div class="row mb-2">
                <div class="col-sm-6 mx-0 px-0">
                    <h5 class="font-weight-bold text-muted">
                        Booking ID : <span>`+value.booking_id+`</span>
                    </h5>
                </div>
                <div class="col-sm-6 mx-0 px-0">
                    <h5 class="text-muted textalign">
                        <span style="color: orange;">$`+value.payment.total+`</span>
                    </h5>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-6 mx-0 px-0">
                    <h5 class="font-weight-bold text-muted">
                        Car Model: <span>`+value.service_type.name+`</span>
                    </h5>
                </div>
                <div class="col-sm-6 mx-0 px-0">
                    <img src="`+value.service_type.image+`" class="serviceImage imagealign">
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-6 mx-0 px-0">
                    <h6 class="text-muted">
                        `+moment(value.assigned_at).format('MMM Do YYYY, h:mm A')+`
                    </h6>
                </div>
                <div class="col-sm-6 mx-0 px-0">
                    <h6 class="text-muted textalign">
                        <a href="{{ route('passenger.trip-details') }}?id=`+value.id+`" class="btn btn-sm btn-success-theme text-white trip-detail">Details</a>
                    </h6>
                </div>
            </div>
        </div>

    </div><br>
                    `;
                });
            }
                $('#tripHistory').append(options);
                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                } 
            }
        });
    });
    
</script>

<!-- footer -->
@include('includes.passenger_footer')

{{-- <div class="service-image position-relative my-1 d-flex justify-content-center align-items-center img-fluid">
</div> --}}

{{-- <div class="col-sm-2 mx-0 px-0 text-center">
    <h5 class="font-weight-bold text-muted">
        `+cancel+`
    </h5>
</div> --}}

{{-- <div class="col-lg-12 px-5 py-1">

    <div class="row mb-2">
        <div class="col-sm-6 mx-0 px-0">
            <h5 class="font-weight-bold text-muted">
               Booking ID : 
            </h5>
        </div>
        <div class="col-sm-6 mx-0 px-0">
            <h5 class="text-muted">`+value.booking_id+`</h5>
        </div>
    </div>
   
    <div class="row mb-2">
        <div class="col-sm-6 mx-0 px-0">
            <h6 class="font-weight-bold text-muted">
                Date: 
            </h6>
        </div>
        <div class="col-sm-6 mx-0 px-0">
            <h6 class="text-muted">`+moment(value.assigned_at).format('MMM Do YYYY, h:mm A')+`</h6>
        </div>
    </div>
    
    <div class="row mb-2">
        <div class="col-sm-6 mx-0 px-0">
            <h6 class="font-weight-bold text-muted">
                Fair: 
            </h6>
        </div>
        <div class="col-sm-6 mx-0 px-0">
            <h6 class="text-muted1">$`+value.payment.total+`</h6>
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-sm-12 mx-0 px-0 text-center">
            <div class="service-image position-relative my-1 d-flex justify-content-center align-items-center img-fluid">
                <img src="`+value.service_type.image+`" class="serviceImage">
            </div>
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-sm-12 mx-0 px-0 text-center">
            <h6 class="text-muted">
                `+value.service_type.name+`
            </h6>
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-sm-12 mx-0 px-0 text-center">
            <h5 class="font-weight-bold text-muted">
                <a href="{{ route('passenger.trip-details') }}?id=`+value.id+`" class="btn btn-sm btn-info trip-detail">Details</a>
            </h5>
        </div>
        
    </div>
</div> --}}