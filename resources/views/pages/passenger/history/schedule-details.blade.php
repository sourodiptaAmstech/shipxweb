<!-- header -->
@include('includes.passenger_header')

<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.passenger_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <div class="px-sm-3 pt-5">
                    <div class="alert alert-danger update_alert" style="display: none;">
                        <p class="text-danger" id="update-msg"></p>
                    </div>
                    <h2 class="font-weight-bold mb-0">Upcoming Trip Details</h2>
                    <div class="container my-2" id="tripDetails">
                        <input type="hidden" name="request_id" id="request_id" value="{{$request_id}}">
                        <div class="row rounded-border1 shadow-lg bg-white" style="border: 1px solid lightgrey;">
                            <div class="col-lg-12 px-0 d-flex">
                                <img src="" style="width: 100%;height: auto;" id="static_map">
                            </div>
                            <div class="col-lg-12 py-2">
                                <div class="row border-bottom">
                                    <div class="col-sm-2 mx-0 px-0">
                                        <div class="profile-image position-relative my-2 d-flex justify-content-center align-items-center">
                                            <img src="{{URL::asset('/')}}assets/img/profile-user.png" alt="Profile" id="promap_image">
                                        </div>
                                    </div>
                                    <div class="row col-sm-10 mx-0 px-0">
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h5 class="py-1 text-muted" id="m_name"></h5>
                                        </div>
                                        <div class="col-sm-6 mx-0 px-0">
                                            <h5 class="py-1 text-muted" id="m_date"></h5>
                                        </div>
                                        <div class="col-sm-12 mx-0 px-0">
                                            <h5 class="py-1 text-muted">Booking ID: <span id="m_bookingID"></span></h5>
                                        </div>
                                        <div class="col-sm-12 mx-0 px-0">
                                            <div id="rateYo" style="float: left;"></div> 
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-12 pt-2 px-5">
                                <div class="row mb-2" id="CancelD">
                                    
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                           PickUp Point: 
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_picpoint"></h6>
                                    </div>
                                </div>
                               
                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                            Drop Off Point: 
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_dropoint"></h6>
                                    </div>
                                </div>
                            
                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                            Total Fare: 
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted">$<span id="m_fair"></span></h6>
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                            Payment By: 
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_payment"></h6>
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="font-weight-bold text-muted">
                                            Comments given by driver: 
                                        </h6>
                                    </div>
                                    <div class="col-sm-6 mx-0 px-0">
                                        <h6 class="text-muted" id="m_comment"></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        var request_id = $('#request_id').val();
        $.ajax({
            url: "{{ route('passenger.get.upcoming-trip.details') }}",
            method:"POST",
            data:{ request_id: request_id, timeZone: timeZone },
            success: function(response){
                console.log(response);
                $("#static_map").attr({ "src": response.data[0].static_map });
                var picture = response.data[0].provider.avatar;
                if(picture!=''&&picture!=null){
                    $("#promap_image").attr({ "src": "https://app.shipxnow.com/storage/"+picture });
                    $("#promap_image").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
                }
                var full_name = response.data[0].provider.first_name+' '+response.data[0].provider.last_name
                $("#m_name").text(full_name);
                $("#m_date").text(moment(response.data[0].assigned_at).format('MMM Do YYYY, h:mm A'));
                $("#m_bookingID").text(response.data[0].booking_id);
                $("#m_picpoint").text(response.data[0].s_address);
                $("#m_dropoint").text(response.data[0].d_address);
                $("#m_fair").text(response.data[0].payment.total);
                $("#m_payment").text(response.data[0].payment.payment_mode);
                $("#m_comment").text(response.data[0].rating.provider_comment);
                $(function(){
                    $("#rateYo").rateYo({
                        rating: response.data[0].provider.rating, 
                        spacing: "5px", 
                        numStars: 5, 
                        minValue: 0, 
                        maxValue: 5, 
                        normalFill: '#a6a6a6', 
                        ratedFill: 'orange',
                        starWidth: "25px",
                    });
                });

                $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.px-sm-3').removeClass('pt-5');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                } 
            }
        });

    });
    
</script>

<!-- footer -->
@include('includes.passenger_footer')