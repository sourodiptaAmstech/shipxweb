<!-- header -->
@include('includes.passenger_header')

<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.passenger_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <form class="px-md-5 mx-md-5" id="reportIssueFrom">
                    <div class="px-3 upper pt-5">
                        <div class="alert alert-danger update_alert" style="display: none;">
                            <p class="text-danger" id="update-msg"></p>
                        </div>
                        <div class="alert alert-success updatesuc_alert" style="display: none;">
                            <p class="text-success" id="updatesuc-msg"></p>
                        </div>

                        <h2 class="font-weight-bold mb-0">Report an issue</h2>

                        <div class="form-group mb-4 mt-4">
                            <input type="text" name="subject" id="subject" class="form-control inset-input pl-4" placeholder="Subject" onkeyup="return validate()">
                            <span class="text-danger" id="fname"></span>
                        </div>
                        <div class="form-group mb-4">
                            <input type="text" name="report" id="report" class="form-control inset-input pl-4" placeholder="Report" onkeyup="return validate()">
                            <span class="text-danger" id="lname"></span>
                        </div>
    
                        <div class="d-flex justify-content-center my-5">
                            <button type="submit" class="btn btn-success-theme btn-lrg grad text-white mb-5">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');
    
    function validate(){
        var status=null;

        var fname = document.getElementById('subject').value
        if (fname == '') {
            document.getElementById("fname").innerHTML='The subject field is required.'
            document.getElementById('subject').classList.add('has-error')
            status=false
        } else {
            $('.upper').removeClass('pt-4');
            $('.upper').addClass('pt-5');
            $('.update_alert').hide();
            $('#update-msg').html('');
            $('.updatesuc_alert').hide();
            $('#updatesuc-msg').html('');
            document.getElementById("fname").innerHTML=''
            document.getElementById('subject').classList.remove('has-error')
            status=true
        }

        var lname = document.getElementById('report').value
        if (lname == '') {
            document.getElementById("lname").innerHTML='The report field is required.'
            document.getElementById('report').classList.add('has-error')
            status=false
        } else {
            document.getElementById("lname").innerHTML=''
            document.getElementById('report').classList.remove('has-error')
        }

        return status
    }

$(document).ready(function () {
    //REPORT PASSENGER FORM.................................
    $('#reportIssueFrom').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var subject = $('#subject').val()
        var report = $('#report').val()
        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.report') }}",
            method: "POST",
            data: { subject: subject, report: report, timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                $('.upper').removeClass('pt-5');
                $('.upper').addClass('pt-4');
                $('.updatesuc_alert').show();
                $('#updatesuc-msg').html(response.message);
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('subject')) {
                        $('#fname').html(responseMsg.errors.subject).promise().done(function(){
                            $('#subject').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('report')) {
                        $('#lname').html(responseMsg.errors.report).promise().done(function(){
                            $('#report').addClass('has-error');
                        });
                    }
                }
                if (response.status == 403){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                }
            }
        });
    });
//END REPORT PASSENGER FORM.................................
});
</script>
<!-- footer -->
@include('includes.passenger_footer')