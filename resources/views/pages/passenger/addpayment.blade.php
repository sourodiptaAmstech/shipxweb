<!-- header -->
@include('includes.passenger_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.passenger_sidebar')
            </div>
            <div class="col-lg-9 px-0">
            <div class="px-md-5 mx-md-5">
              <div class="px-3 pt-5">
                <h2 class="font-weight-bold mb-4">My Added Cards</h2>
                <div class="d-flex justify-content-start mb-3 mt-5">
                    <button type="button" class="btn btn-lg btn-success-theme text-white" id="addDebitCard">Add New Card</button>
                </div>
                <ul class="list-group listGroup mb-4">
                </ul>



              </div>
            </div>
            </div>
        </div>
    </div>
</div>
<script src="https://js.stripe.com/v3/"></script>
<!-- The Modal -->
<div id="modalCustom" class="modalCustom">

    <!-- Modal content -->
    <div class="modal-contentCustom">
      <div class="modal-headerCustom">
          <h2>Add Card</h2>
      </div>
      <div class="modal-bodyCustom">
          <div class="wrapper">
          <form id="payment-form" action="{{ route('passenger.addCard') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-row">
              <label for="card-element" class="stripelabel">
                Credit or Debit Card
              </label>
              <br>
              <div id="card-element">
                <!-- A Stripe Element will be inserted here. -->
              </div>

              <!-- Used to display form errors. -->
              <div id="card-errors" role="alert"></div>
            </div>

            <button class="Stripebutton">Add Card</button>
            <label class="Stripebutton" id="cancelButton">Cancel</label>
          </form>
        </div>
    </div>

    </div>

  </div>
<style>
/* The Modal (background) */
.modalCustom {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/*{
    height: 100%;
    background-color: #f7f8f9;
    color: #6b7c93;
}*/




/* Modal Content */
.modal-contentCustom {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 50%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-headerCustom {
  padding: 2px 16px;
  background-color: #f18933;
  color: white;
  text-align:center;
}

.modal-bodyCustom {padding: 0;}




/**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
 .wrapper{
    width: 671px;
    margin: 0 auto;
    height: 100%;
    background-color: #f7f8f9;
    color: #6b7c93;
}
#payment-form {
    padding: 30px;
    height: 140px;
}
.form-row {
    width: 70%;
    float: left;
}
.Stripebutton {
    border: none;
    border-radius: 4px;
    outline: none;
    text-decoration: none;
    color: #fff;
    background: #f18933;
    white-space: nowrap;
    display: inline-block;
    height: 40px;
    line-height: 40px;
    padding: 0 8px;
    box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
    border-radius: 4px;
    font-size: 15px;
    font-weight: 600;
    letter-spacing: 0.025em;
    text-decoration: none;
    -webkit-transition: all 150ms ease;
    transition: all 150ms ease;
    float: left;
    margin-left: 12px;
    margin-top: 28px;
}
.stripelabel {
    font-weight: 500;
    font-size: 14px;
    display: block;
    margin-bottom: 8px;
    width: 100%;
}
.StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
  width: 100%;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}
#card-errors {
    height: 20px;
    padding: 4px 0;
    color: #fa755a;
}
.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}
</style>
<script>

    t = new Date().toUTCString().split(' ');
    var timeZone = t[t.length-1] + moment().format('Z');

    $(document).ready(function(){
        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.ListCard') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
                console.log(response);
                var options = '';
                $.each(response.data, function(key, value) {
                    if (value.brand.trim()=='Visa') {
                        var card_img = `<img src="{{URL::asset('/')}}assets/img/credit-card-visa.png" alt="" class="card-logo" style="width: 29px;">`;
                    } else if (value.brand.trim()=='MasterCard') {
                        var card_img = `<img src="{{URL::asset('/')}}assets/img/master-card.png" alt="" class="card-logo" style="width: 29px;">`;
                    } else if (value.brand.trim()=='American Express') {
                        var card_img = `<img src="{{URL::asset('/')}}assets/img/american-express.png" alt="" class="card-logo" style="width: 29px;">`;
                    } else if (value.brand.trim()=='UnionPay') {
                        var card_img = `<img src="{{URL::asset('/')}}assets/img/unionpay.png" alt="" class="card-logo" style="width: 29px;">`;
                    } else if (value.brand.trim()=='JCB') {
                        var card_img = `<img src="{{URL::asset('/')}}assets/img/jcb.png" alt="" class="card-logo" style="width: 29px;">`;
                    } else if (value.brand.trim()=='Diners Club') {
                        var card_img = `<img src="{{URL::asset('/')}}assets/img/diners-club.png" alt="" class="card-logo" style="width: 29px;">`;
                    } else if (value.brand.trim()=='Discover') {
                        var card_img = `<img src="{{URL::asset('/')}}assets/img/discover.png" alt="" class="card-logo" style="width: 29px;">`;
                    } else {
                        var card_img = `<img src="{{URL::asset('/')}}assets/img/card.png" alt="" class="card-logo" style="width: 29px;">`;
                    }
                    if (value.is_default==0) {
                        options += `
                            <li class="list-group-item w-100 shadow-sm p-3 mb-2 bg-white rounded border-default">
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 col-1">
                                        `+card_img+`
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-8">
                                        <h5 class="font-weight-bold">**** **** **** `+value.last_four+`</h5>

                                    </div>
                                    <div class="col-md-1 col-sm-1 col-1 card-cross" style="margin-right: 20px;">
                                        <div class="h-100 d-flex align-items-center delete-item">
                                            <button type="button" class="close deleteClass" aria-label="Close" id="closeID`+value.card_id+`" key="`+value.card_id+`">
                                                <img src="{{URL::asset('/')}}assets/img/close.png" alt="" class="m-close" style="width: 32px;height: auto;">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            `;

                    }else{
                        options += `
                            <li class="list-group-item w-100 shadow-sm p-3 mb-2 bg-white rounded border-default">
                                <div class="row">
                                    <div class="col-md-1 col-sm-1 col-1">
                                       `+card_img+`
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-8">
                                        <h5 class="font-weight-bold">**** **** **** `+value.last_four+`</h5>

                                    </div>
                                    <div class="col-md-1 col-sm-1 col-1 card-cross" style="margin-right: 48px;">
                                        <div class="h-100 d-flex align-items-center delete-item">
                                            <button type="button" class="close deleteClass" aria-label="Close" id="closeID`+value.card_id+`" key="`+value.card_id+`">
                                                <img src="{{URL::asset('/')}}assets/img/close.png" alt="" class="m-close" style="width: 32px;height: auto;margin-left: 28px;">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            `;
                    }
                });
                $('.listGroup').append(options);
                $('.deleteClass').on('click',function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var user_cards_id = $('#'+this.id).attr('key');
                    t = new Date().toUTCString().split(' ');
                    var timeZone = t[t.length-1] + moment().format('Z');
                    bootbox.confirm('Are you sure to delete?', function (res) {
                        if (res){
                            $(".overlay").show();
                            $.ajax({
                                url: "{{ route('passenger.delete.card') }}",
                                method:"DELETE",
                                data:{ user_cards_id: user_cards_id, timeZone: timeZone },
                                success: function(response){
                                    location.reload();
                                    $(".overlay").hide();
                                },
                                error: function(response){
                                    $(".overlay").hide();
                                }
                            });
                        }
                    });
                })
            },
            error: function(response){
                console.log(response);
                $(".overlay").hide();
            }
        });

    });

    $(document).on("click","#addDebitCard",function(){
        //alert();
        $("#modalCustom").show();
    })


// Create a Stripe client.
var stripe = Stripe('pk_test_38qqz5TNQaPzuplQhc7HjgHx00lItEoS1B');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.on('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
    $(".overlay").show();
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
      $(".overlay").hide();
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
    $(".overlay").show();
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

$(document).on("click","#cancelButton", function(){
    location.href = "{{ route('passenger.addpayment') }}";
})
</script>

<!-- footer -->
@include('includes.passenger_footer')
