<!-- header -->
@include('includes.passenger_header')
<div class="bg-white px-md-5">
    <div class="container px-0">
        <div class="row mx-0">
            <div class="col-lg-3 px-0">
                @include('includes.passenger_sidebar')
            </div>
            <div class="col-lg-9 px-0">
                <div class="px-md-5 mx-md-5">
                    <div class="px-3 upper pt-5">
                        <div class="alert alert-danger update_alert" style="display: none;">
                            <p class="text-danger" id="update-msg"></p>
                        </div>
                        <h2 class="font-weight-bold mb-0">Shipx Help</h2>
                        <div class="container my-2" id="tripHistory"> 

                            <div class="row justify-content-center mb-5 mx-0 below pt-5">
                                <div class="m-0">
                                    <img style="width: 228px; height: auto;" 
                                    src="{{url('assets/img/user_headphones.png')}}" alt="">
                                </div>
                            </div>

                            <div class="row w-50 h-100 align-items-center" style="margin-left: 26%">

                                <a href="javascript:void(0)" class="btn btn-success-theme text-white"><i class="fa fa-phone" aria-hidden="true"></i></a>

                                <a href="https://mail.google.com/mail/?view=cm&fs=1&to=support@shipxnow.com&su=Shipx Now-Help&body=Hello team" target="_blank" class="btn btn-success-theme text-white"><i class="fa fa-envelope" aria-hidden="true"></i></a>

                                <a href="https://shipxnow.com" target="_blank" class="btn btn-success-theme text-white"><i class="fas fa-globe"></i></a>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        $(".overlay").show();
        $.ajax({
            url: "{{ route('passenger.get.help') }}",
            method:"GET",
            data:{ timeZone: timeZone },
            success: function(response){
                $(".overlay").hide();
            //     var options = '';
            //     if(response.data.length==0){
            //         options=`<h6 style="font-size: x-large;" class="mt-5 text-muted">You did not apply for any trip yet.</h6>`;
            //         $('#tripHistory').addClass('px-0');
            //     } else {
            //     $.each(response.data.data, function(key, value) {
            //         var requestStatus=value.requestDetails.request_status;
            //         if (requestStatus=='CANCELBYDRIVER'||requestStatus=='CANCELBYPASSENGER'||requestStatus=='CANCELBYSYSTEM') {
            //             var cancel='<p class="text-danger">Cancelled</p>';
            //         }else{
            //             var cancel='';
            //         }
            //         options += `
            //         `;
            //     });
            // }
            //     $('#tripHistory').append(options);
            //     $(".overlay").hide();
            },
            error: function(response){
                $(".overlay").hide();
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.upper').removeClass('pt-5');
                    $('.upper').addClass('pt-4');
                    $('.update_alert').show();
                    $('#update-msg').html(responseMsg.message);
                } 
            }
        });
    });
    
</script>

<!-- footer -->
@include('includes.passenger_footer')