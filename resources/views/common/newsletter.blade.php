<!-- Subscribe Form -->
<div class="container-fluid subscribe">
    <div class="row" style="padding: 19px">
        <div class="col-md-6 d-flex justify-content-center">
            <img height="300px" class="img-fluid mr-md-0" src="{{url('assets/img/subscribe-left.png')}}" alt="">
        </div>
        <div class="col-md-6 d-flex align-items-center">
            <div class="mx-0 w-100">
                <div class="alert alert-danger news_alert" style="display: none;">
                    <p class="text-danger" id="news-msg"></p>
                </div>
                <div class="alert alert-success newsuc_alert" style="display: none;">
                    <p class="text-success" id="newsuc-msg"></p>
                </div>
                <h3 class="text-left font-weight-bold mx-0 my-3">Subscribe to Our Newsletter</h3>
                <form id="newLetterForm" class="pr-md-5">
                    <div class="input-group mb-3 pr-md-5">
                        <input type="email" name="email1" id="email1" style="border-radius: 30px 0 0 30px" class="form-control inset-input pl-4" placeholder="Enter your email id" aria-label="Enter your email id" aria-describedby="basic-addon2" onblur="return validateNews()">
                        <div class="input-group-append">
                            <button type="submit" style="border-radius: 0 30px 30px 0; padding: 0 20px 0 15px;" class="btn btn-success-theme text-white">
                                <img src="{{url('assets/img/send.png')}}" alt="">
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function validateNews(){
        var status=null;
        var email1 = document.getElementById('email1').value;
        if (email1 == '') {
            $('#email1').addClass('has-error');
            status = false
        } else {
            $('.news_alert').hide();
            $('#news-msg').html('');
            $('.newsuc_alert').hide();
            $('#newsuc-msg').html('');
            $('#email1').removeClass('has-error');
            status=true;
        }
        return status
    }

    $('#newLetterForm').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        t = new Date().toUTCString().split(' ');
        var timeZone = t[t.length-1] + moment().format('Z');
        var email1 = $('#email1').val()
        $.ajax({
            url: "{{ route('news.letter') }}",
            method: "POST",
            data: {email: email1, timeZone: timeZone},
            success: function(response){
                $('.newsuc_alert').show();
                $('#newsuc-msg').html(response.message);
                $('#email1').val('')
            },
            error: function(response){
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('email')) {
                        $('#email1').addClass('has-error');
                    }
                }
                if (response.status == 400){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.news_alert').show();
                    $('#news-msg').html(responseMsg.message);
                }
                if (response.status == 500){
                    var responseMsg = $.parseJSON(response.responseText);
                    $('.news_alert').show();
                    $('#news-msg').html(responseMsg.message);
                }
            }
        });
    });
</script>